{
    appDir: '../www',
    baseUrl: 'js/lib',
    paths: {
        pages: '../pages',
        app: '../app'
    },
    dir: '../www-built',
    modules: [
        {
            name: '../main',
            include: ['jquery']
        },
        {
            name: '../indexCfg',
            include: ['pages/index'],
            exclude: ['../main']
        },
        {
            name: '../orderCfg',
            include: ['pages/order'],
            exclude: ['../main']
        },
        {
            name: '../serviceCfg',
            include: ['pages/service'],
            exclude: ['../main']
        }

    ]
}
