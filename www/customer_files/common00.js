$(document).ready(function(){
	$('input,select,textarea').focus(function(){
		$(this).addClass('common-input');
	})
	$('.switcher-wrap a').click(function(){
		$(this).parent().find('a').removeClass('curr');
		$(this).addClass('curr');
	})
	$('.price-table tbody tr:last-child, .quality-level tbody tr:last-child').addClass('pt-lastrow');
})