$(document).ready(function(){
    $('.stripe dt').click(function(){
        if ($(this).parent().hasClass('expanded'))
        {
            $(this).parent().removeClass('expanded');
            $(this).find('.collapse span').html('View all');
            $(this).parent().find('dd').slideUp();
        }
        else
        {
            $(this).parent().addClass('expanded');
            $(this).find('.collapse span').html('Stripe');
            $(this).parent().find('dd').slideDown();
        }
    });
})