define(function (require) {
	var customSelect = require('app/customSelect'),
		mobileElementShuffles = require('app/mobileElementShuffles'),
		mobilePanel = require('app/mobilePanel'),
		order = require('app/orderFormMethods'),
		loginDropdown = require('app/loginDropdown');

	order.init();
	mobilePanel.init();
	mobileElementShuffles.init();
	customSelect.init();
	loginDropdown();
});