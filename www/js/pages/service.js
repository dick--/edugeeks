define(function (require) {
	var mobileElementShuffles = require('app/mobileElementShuffles'),
		mobilePanel = require('app/mobilePanel'),
		loginDropdown = require('app/loginDropdown');

	mobilePanel.init();
	mobileElementShuffles.init();
	loginDropdown();
});