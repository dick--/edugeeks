define(function (require) {
	var customSelect = require('app/customSelect'),
		mobileElementShuffles = require('app/mobileElementShuffles'),
		mobilePanel = require('app/mobilePanel'),
		animation = require('app/animation'),
		loginDropdown = require('app/loginDropdown');

	mobilePanel.init();
	mobileElementShuffles.init();
	customSelect.init();
    animation.restart();
    animation.arrows();
    loginDropdown();
});