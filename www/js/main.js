//todo : r.js build configuration files
requirejs.config({
    "baseUrl": 'js/lib',
    "paths": {
        "app": '../app',
        "pages": '../pages',
        "delegation": 'app/delegation'
    },
    "shim": {
    	"jquery-ui-core": {
    		"exports": '$',
    		"deps": ['jquery']
    	},
    	"jquery-ui-position": {
    		"exports": '$',
    		"deps": ['jquery']
    	},
    	"jquery-ui-widget": {
    		"exports": '$',
    		"deps": ['jquery']
    	},
    	"jquery-ui-selectmenu": {
    		"exports": '$',
    		"deps": ['jquery-ui-core','jquery-ui-position','jquery-ui-widget']
    	},
    	"jquery-cluetip": {
    		"exports": '$',
    		"deps": ['jquery-ui-core']
    	}
    }
});