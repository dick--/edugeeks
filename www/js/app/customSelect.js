define(['app/class'],function(classs){

	var configDropDown = {
		selectsClass: 'customSel',
		dropdownsClass: 'customDropDown',//later will do smth
		dropdownsSel: '.customDropDown',
		menuSel: '.mainMenu',
		currMenuSel: '.currentMenu',
		chosenOptionClass: 'chosenOption',
		hiddenSel: '.hiddenSel',
		activeClass: 'active',
		dataValueAttr: 'data-val'
	}
	var isMobile = ( navigator.userAgent.match(/(iPad|iPhone|iPod|Android)/g) ? true : false );
	
	var selects = document.querySelectorAll('.'+configDropDown.selectsClass);
	var dropdowns = document.querySelectorAll(configDropDown.dropdownsSel);
	var hiddenSels = document.querySelectorAll(configDropDown.hiddenSel);
	var chosens = document.querySelectorAll('.'+configDropDown.chosenOptionClass);
	

	function renderCustomDropdowns(config) {
		var dropdownHtml = [];
		for (var i = selects.length - 1; i >= 0; i--) {
			var options = selects[i].querySelectorAll('option');
			dropdownHtml[i] = '<div class="'+config.dropdownsClass+'">';
			for (var j = options.length - 1; j >= 0; j--) {
				dropdownHtml[i] += '<a href="#" data-val="'+options[j].value+'">'+options[j].innerHTML+'</a>';
			};
			dropdownHtml[i] +='</div>';
		};
		for (var i = selects.length - 1; i >= 0; i--) {
			selects[i].innerHTML += dropdownHtml[i];
		}
	}
	function renderSelectedOptions(config) {
		var selectsHtml = [];
		for (var i = selects.length - 1; i >= 0; i--) {
			var firstOption = selects[i].querySelector('option');
			selectsHtml[i] = '<span class="'+config.chosenOptionClass+'">'+firstOption.innerHTML+'</span>';
		}
		for (var i = selects.length - 1; i >= 0; i--) {
			selects[i].innerHTML += selectsHtml[i];
		};
	}
	
	function init() {
		renderSelectedOptions(configDropDown);
		renderCustomDropdowns(configDropDown);
		//default dropdowns in custom selects in IOS&Android
		function mobileSelectorsFallback() {
			if (isMobile) {
				for (var i = hiddenSels.length - 1; i >= 0; i--) {
					hiddenSels[i].style.display="block";
					classs.add(hiddenSels[i].parentNode,'mobile');
				};
				for (var i = dropdowns.length - 1; i >= 0; i--) {
					chosens[i].style.display="none";
				};
			}
		}
		mobileSelectorsFallback();

		// delegation
		document.body.addEventListener('click', function (e) {
			// selects fire
			if (isMobile) {

			} else {
				if (e.target.parentNode.className.indexOf(configDropDown.selectsClass) != -1) {
					var title = e.target.parentNode.querySelector('.'+configDropDown.chosenOptionClass);
					var hiddenSel = e.target.parentNode.querySelector(configDropDown.hiddenSel);
					for (var j = selects.length - 1; j >= 0; j--) {
						if (e.target.parentNode.className.indexOf(configDropDown.activeClass) != -1) {
							classs.remove(selects[j],configDropDown.activeClass);
							classs.remove(e.target.parentNode,configDropDown.activeClass);
						} else {
							classs.remove(selects[j],configDropDown.activeClass);
							classs.toggle(e.target.parentNode,configDropDown.activeClass);
						};
					};
					e.target.parentNode.querySelector(configDropDown.dropdownsSel).addEventListener('click', function(e) {
						e.target.removeEventListener(e.type, arguments.callee);
						if (e.target && e.target.nodeName == "A" ) {
							title.innerHTML = e.target.innerHTML;
							hiddenSel.value = e.target.getAttribute(configDropDown.dataValueAttr);
						}
						e.preventDefault();
					}, false);
				} else {
					if (e.target.className.indexOf(configDropDown.chosenOptionClass) != -1) {
						return false;
					};
					for (var i = selects.length - 1; i >= 0; i--) {
						classs.remove(selects[i],configDropDown.activeClass);
					}
				}
				if (e.target.parentNode.className.indexOf(configDropDown.selectsClass) == -1 ) {
					if (e.target.className.indexOf(configDropDown.chosenOptionClass) != -1) {
						return false;
					};
					for (var i = selects.length - 1; i >= 0; i--) {
						classs.remove(selects[i],configDropDown.activeClass);
					}
				}
			}
		}, false);
	}
	
	return {
		init: init
	}
})