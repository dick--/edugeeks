define(['jquery-ui-selectmenu','jquery-cluetip'], function($) {

    Date.prototype.format = function(format) {
        var returnStr = '';
        var replace = Date.replaceChars;
        for (var i = 0; i < format.length; i++) {
            var curChar = format.charAt(i);
            if (replace[curChar]) {
                returnStr += replace[curChar].call(this);
            } else {
                returnStr += curChar;
            }
        }
        return returnStr;
    };
    Date.replaceChars = {
        shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        longMonths: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        longDays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        d: function() {
            return (this.getDate() < 10 ? '0' : '') + this.getDate();
        },
        D: function() {
            return Date.replaceChars.shortDays[this.getDay()];
        },
        j: function() {
            return this.getDate();
        },
        l: function() {
            return Date.replaceChars.longDays[this.getDay()];
        },
        N: function() {
            return this.getDay() + 1;
        },
        S: function() {
            return (this.getDate() % 10 == 1 && this.getDate() != 11 ? 'st' : (this.getDate() % 10 == 2 && this.getDate() != 12 ? 'nd' : (this.getDate() % 10 == 3 && this.getDate() != 13 ? 'rd' : 'th')));
        },
        w: function() {
            return this.getDay();
        },
        z: function() {
            return "Not Yet Supported";
        },
        W: function() {
            return "Not Yet Supported";
        },
        F: function() {
            return Date.replaceChars.longMonths[this.getMonth()];
        },
        m: function() {
            return (this.getMonth() < 9 ? '0' : '') + (this.getMonth() + 1);
        },
        M: function() {
            return Date.replaceChars.shortMonths[this.getMonth()];
        },
        n: function() {
            return this.getMonth() + 1;
        },
        t: function() {
            return "Not Yet Supported";
        },
        L: function() {
            return (((this.getFullYear() % 4 == 0) && (this.getFullYear() % 100 != 0)) || (this.getFullYear() % 400 == 0)) ? '1' : '0';
        },
        o: function() {
            return "Not Supported";
        },
        Y: function() {
            return this.getFullYear();
        },
        y: function() {
            return ('' + this.getFullYear()).substr(2);
        },
        a: function() {
            return this.getHours() < 12 ? 'am' : 'pm';
        },
        A: function() {
            return this.getHours() < 12 ? 'AM' : 'PM';
        },
        B: function() {
            return "Not Yet Supported";
        },
        g: function() {
            return this.getHours() % 12 || 12;
        },
        G: function() {
            return this.getHours();
        },
        h: function() {
            return ((this.getHours() % 12 || 12) < 10 ? '0' : '') + (this.getHours() % 12 || 12);
        },
        H: function() {
            return (this.getHours() < 10 ? '0' : '') + this.getHours();
        },
        i: function() {
            return (this.getMinutes() < 10 ? '0' : '') + this.getMinutes();
        },
        s: function() {
            return (this.getSeconds() < 10 ? '0' : '') + this.getSeconds();
        },
        e: function() {
            return "Not Yet Supported";
        },
        I: function() {
            return "Not Supported";
        },
        O: function() {
            return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + '00';
        },
        P: function() {
            return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + ':' + (Math.abs(this.getTimezoneOffset() % 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() % 60));
        },
        T: function() {
            var m = this.getMonth();
            this.setMonth(0);
            var result = this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, '$1');
            this.setMonth(m);
            return result;
        },
        Z: function() {
            return -this.getTimezoneOffset() * 60;
        },
        c: function() {
            return this.format("Y-m-d") + "T" + this.format("H:i:sP");
        },
        r: function() {
            return this.toString();
        },
        U: function() {
            return this.getTime() / 1000;
        }
    };

    var OrderForm = {
        beforeSwitchForms: [],
        afterSwitchForms: [],
        afterShowFeaturePrice: [],

        beforeOnInputChange: [],
        afterOnInputChange: [],

        beforeFormInit: [],
        afterFormInit: [],

        beforeOnCalculatePrice: [],
        afterOnCalculatePrice: [],

        beforeValidate: [],

        afterOnValidateRespond: [],

        fn: {},

        order_features: {},

        step: 0,
        values: {
            preff_wr_id: 0
        },
        prices: {},
        limits: {},
        price_groups: {},
        loaded: {},
        form_valid: 0,
        tzOffset: 0,
        max_preferred_writers: 10,
        validateFields: ['firstname', 'lastname', 'name', 'retype_email', 'email', 'country', 'phone1', 'phone1_type', 'phone1_country', 'phone1_area', 'phone1_number', 'phone2', 'phone2_type', 'phone2_country', 'phone2_area', 'phone2_number', 'topic', 'numpages', 'order_category', 'order_category_sphere', 'details', /* 'accept',*/ 'deadline' /* with hack in validate function */ , 'password', 'urgency', 'wrlevel'],
        validateEachField: [],
        validateArrayFields: ['preff_wr_id'],
        coverLetterId: 0,
        withCoverLetterIds: {},
        isPreview: false,
        isResubmit: false,
        isProofreadGrammarbase: false,
        isQuote: false,
        isCalculator: false,
        isEdit: false,
        isInit: false,
        isResumes: false,
        isPaypal: false,
        isTranslation: false,
        isMobileResumes: false,
        cppDiscountRules: {},
        generalDiscount: 0,
        currenciesFormat: {},
        $order_category_options: null,
        $pages_options: [],
        orderDate: false,
        orderCode: '',
        adminAuthorized: false,
        previewName: '',
        validateAction: '',
        showAcceptError: false,
        sp_validation: 0,
        was_proceed: 0,
        switch_to_phone: true,
        switch_to_row_after_error: false,
        linearSelectDelimiter: false,
        linear_options: {
            /** @param jQuery|bool jQuery object */
            entry: $('<a href="#"/>'),
            /** @param jQuery|bool jQuery object */
            container: $('<span class="linear-select"/>'),
            /** @param jQuery|bool jQuery object */
            wrap_entry: false,
            /** @param jQuery|bool jQuery object */
            wrap_title: false
        },
        tempDescountData: null,
        secondStepImage: '',
        numpages_append_words: true,
        callSubmitFromSubmitValidatedForm: false,

        onAfterAddPreferredWriterInput: function() {},
        onAfterRemovePrefWriter: function() {},

        initialize: function() {
            for (var ind in OrderForm.beforeFormInit) {
                OrderForm.beforeFormInit[ind].call(OrderForm, this);
            }

            $.fn.cluetip.defaults = {
                insertionType: 'insertBefore',
                insertionElement: 'body div:first',
                showTitle: false
            };
            OrderForm.calculateTZOffset();
            OrderForm.afterSwitchForms.push(OrderForm.updateLinearSelects);
            $('#order_form').find('select,input,textarea').each(function() {
                OrderForm.saveValue(this);
            });
            if ($('#doctype').length > 0) {
                cur_dt = OrderForm.getDoctypeValue();

                if (cur_dt != OrderForm.doctype) {
                    OrderForm.doctype = cur_dt;
                    OrderForm.onDoctypeChange();
                } else {
                    OrderForm.calculatePrice();
                }
            }

            OrderForm.loaded[OrderForm.doctype] = $('#order_details').html();
            OrderForm.max_preferred_writers = $('#preff_wr_id_max').val();
            OrderForm.fillNumpages();
            OrderForm.showHidePages();
            OrderForm.updateLinearSelects();
            OrderForm.setInputEvents();
            $('#email').change(OrderForm.onInputChange);

            //      $('#preff_wr_id .add').click(OrderForm.addPreferredWriterInput);
            //      $('#preff_wr_id .delete').click(OrderForm.removePrefWriter);

            if ($('#email').val() == '') {
                OrderForm.hidePassword();
            } else {
                if (!OrderForm.isResubmit) {
                    OrderForm.checkPassword();
                }
            }
            OrderForm.attachBubblePopup();

            this.enableHints();
            OrderForm.showHidePreferredInputs();

            if ($('#doctype').length > 0 && !OrderForm.isResumes) OrderForm.preload.call(OrderForm);

            for (field in OrderForm.validateFields) {
                $('#' + OrderForm.validateFields[field]).change(OrderForm.onInputChangeClearValidationError);
            }

            if (OrderForm.authorized) {
                OrderForm.ping();
            }

            OrderForm.enableSubmit();

            OrderForm.initOrderCategories();

            OrderForm.repaintTable();
            if ($('#doctype').length > 0) {
                OrderForm.pickOutFreeFeatures();
            }
            OrderForm.onInputChangeClearValidationError = function(input) {
                if (input) {
                    $row = $('#row_' + input.id)
                    if ($row.length == 0) $row = $(input).parents('tr:first');
                    $row.removeClass('validation-error');
                }
                $(input).removeClass('validation_error');
                $(input).parent().find('div.validation_error').hide();

                OrderForm.insertValInOtherObject(input);

                if ($('#input_phone_country_code1').length > 0 && $(input).attr("id") == 'country') {
                    for (ind in OrderForm.country) {
                        if (OrderForm.country[ind]['id_country'] == $(input).val()) {
                            var code = OrderForm.country[ind]['c_id'].replace("*", " ");
                            $('#input_phone_country_code1').val(code);
                            $('#input_phone_country_code2').val(code);
                            break;
                        }
                    }
                }

                $div = $('div.eot');
                if ($div.length) {
                    $(input).css("background-color", "#FFFFFF");

                    if (($(input).attr("id") == 'email') || ($(input).attr("id") == 'retype_email')) {
                        if ($('#email').val() == $('#retype_email').val()) {
                            $('#email').css("background-color", "#ccff99");
                            $('#retype_email').css("background-color", "#ccff99");
                        }
                    }
                }
            }

            OrderForm.checkPromoCode();

            if (window.Features != undefined) {
                this.order_features = Features.initialize();
            }

            if (window.navigator.userAgent.indexOf("MSIE") != -1) {
                OrderForm.validationName = 'Validating...';
            }

            OrderForm.notEnterSubmit();
            OrderForm.isInit = true;
            for (var ind in OrderForm.afterFormInit) {
                OrderForm.afterFormInit[ind].call(OrderForm, this);
            }
        },
        validateTextareaMaxlength: function() {
            var el = $(this);
            if (el.val().length > parseInt(el.attr('maxlength'))) {
                el.val(el.val().substring(0, el.attr('maxlength')));
            }
        },
        loadEachFieldValidate: function() {
            $.each(OrderForm.validateEachField, function(number, field) {
                OrderForm.validate(OrderForm.onValidateField($('#' + field)));
            });
        },
        notEnterSubmit: function() {
            $('#order_form input[type!=submit]').keypress(function(e) {
                if (e.which == 13) {
                    return false;
                }
            });
        },

        insertValInOtherObject: function(inputObj) {
            if ($(inputObj).attr("insertto")) {
                insertVal = $(inputObj).val();
                if ($(inputObj).attr("id") == 'country') {
                    for (ind in OrderForm.country) {
                        if (OrderForm.country[ind]['id_country'] == insertVal) {
                            insertVal = OrderForm.country[ind]['c_id'].replace("*", " ");
                            break;
                        }
                    }
                }
                insertObjId = $(inputObj).attr("insertto").split(" ");
                for (ind in insertObjId) {
                    if ($('#' + insertObjId[ind]).length > 0 && $('#' + insertObjId[ind]).is(':visible')) {
                        var elm = $('#' + insertObjId[ind]);
                        elm.val(insertVal);
                        OrderForm.saveValue(elm.get(0));
                    }
                }
            }
        },
        ping: function() {
            window.setTimeout(function() {
                $.get('/order.ping?' + Math.random(), {}, OrderForm.ping)
            }, 60000);
        },

        initOrderCategories: function() {
            if ($('#order_category_sphere').length) {
                selected_category = $('#order_category').val();
                $options = $('#order_category option');
                var categories = {};
                category = 0;
                for (i = 1; i < $options.length; i++) {
                    if ($options[i].text.charCodeAt(0) == 160) {
                        $options[i].text = $options[i].text.substr(2);
                    } else {
                        $options[i].text = '#' + $options[i].text;
                        category = i;
                    }
                    if (category) {
                        categories[i] = category;
                    }
                }

                OrderForm.$order_category_options = new $('#order_category option');

                OrderForm.categorySphereChange();

                $('#order_category option').each(
                    function() {
                        if (this.value == selected_category) {
                            $(this).parent()[0].selectedIndex = this.index;
                        }
                    }
                );

                $('#order_category_sphere').change(OrderForm.categorySphereChange);
            }
        },

        categorySphereChange: function() {
            var category_sphere = $('#order_category_sphere').val();
            var $row_order_category = $('#row_order_category');
            var $order_category = $('#order_category');
            $order_category.empty();
            $order_category.append($options[0]);
            if (category_sphere == 0) {
                $('#order_category option[value=' + category_sphere + ']').attr('selected', 'selected');
                $row_order_category.hide();
            } else {
                $options = OrderForm.$order_category_options;
                if ($options.length) {
                    var spheres = false;

                    for (i = 1; i < $options.length && $options[i - 1].value != category_sphere; i++) {}
                    for (; i < $options.length && $options[i].text.indexOf('#') == -1; i++) {
                        $order_category.append($options[i]);
                        spheres = true;
                    }

                    if (!spheres) {
                        for (i = 1; i < $options.length && $options[i].value != category_sphere; i++) {}
                        $order_category.append($options[i]);
                        $('#order_category option[value=' + category_sphere + ']').attr('selected', 'selected');
                        $row_order_category.hide();
                    } else {
                        $('#order_category option[value=0]').attr('selected', 'selected');
                        $row_order_category.show();
                    }
                }
            }
            OrderForm.repaintTable();
        },

        clueTipOpts: {
            topOffset: -12,
            leftOffset: -15,
            arrows: true,
            dropShadow: (window.navigator.userAgent.indexOf("MSIE") != -1) ? true : false,
            fx: {
                open: 'fadeIn', // can be 'show' or 'slideDown' or 'fadeIn'
                openSpeed: 100
            },
            hoverIntent: {
                sensitivity: 5,
                interval: 400,
                timeout: 0
            },

            sticky: true,
            mouseOutClose: true,
            closePosition: 'title',
            closeText: '',
            splitTitle: '|',
            showTitle: false,
            height: 'auto',
            width: 'auto'
        },

        clueTipOptsSsl: {
            topOffset: -50,
            leftOffset: -63,
            arrows: true,
            dropShadow: (window.navigator.userAgent.indexOf("MSIE") != -1) ? true : false,
            fx: {
                open: 'fadeIn', // can be 'show' or 'slideDown' or 'fadeIn'
                openSpeed: 100
            },
            hoverIntent: {
                sensitivity: 5,
                interval: 400,
                timeout: 0
            },

            sticky: true,
            mouseOutClose: true,
            closePosition: 'title',
            closeText: '',
            splitTitle: '|',
            showTitle: false,
            height: 'auto',
            width: 'auto'
        },

        enableHints: function($els) {
            if ($els == undefined) {
                $els = $('a.field_hint');
            }
            $els.each(function() {
                if ($(this).hasClass('ssl')) {
                    $(this).cluetip(OrderForm.clueTipOptsSsl);
                } else {
                    $(this).cluetip(OrderForm.clueTipOpts);
                }
            });
        },

        priceError: function(d, w, u) {
            return;
            $.post('/order.error/', {
                'd': d,
                'u': u,
                'w': w,
                'url': window.location
            }, function(data) {
                if (data != '') {
                    alert('oops!');
                }
            })
        },
        setFormValues: function() {
            for (item_id in this.values) {
                if (item_id == 'doctype') {
                    this.setDoctypeValue(this.doctype);
                    this.saveValue($('#doctype')[0]);
                } else
                if (item_id == 'preff_wr_id') {
                    values = this.values.preff_wr_id;
                    pref_cnt = 0;
                    for (i = 0; i < values.length; i++) {
                        if (values[i].value != '') pref_cnt++;
                    }
                    pref_cnt = pref_cnt > 0 ? pref_cnt : 1;
                    $preff_wr_id_inputs = $('#preff_wr_id input');
                    for (i = $preff_wr_id_inputs.length; i < pref_cnt; i++) {
                        this.addPreferredWriterInput();
                    }

                    $preff_wr_id_inputs = $('#preff_wr_id input');

                    for (i = 0; i < values.length; i++) {
                        if ($preff_wr_id_inputs[i]) {
                            $preff_wr_id_inputs[i].value = values[i];
                        }
                    }

                } else
                if (item_id != '') {
                    $element = $('#' + item_id);
                    if ($element.length > 0) {
                        tagName = $element[0].tagName;
                        if (tagName == 'INPUT' || tagName == 'TEXTAREA') {
                            if ($element[0].type == 'checkbox') {
                                $element[0].checked = this.values[item_id].checked;
                            } else {
                                $element.val(this.values[item_id].value);
                            }
                        } else
                        if (tagName == 'SELECT') {
                            if (item_id != 'urgency' || OrderForm.isResubmit) {
                                for (i = 0; i < $element[0].length; i++) {
                                    if ($element[0].options[i].text == this.values[item_id].text ||
                                        item_id == 'order_category' && $element[0].options[i].text.charCodeAt(0) == 160 && $element[0].options[i].text.substr(2) == this.values[item_id].text) {
                                        $($element[0].options[i]).attr('selected', 'selected');
                                        break;
                                    }
                                }
                            }
                            this.saveValue($element[0]);
                        }
                    } else
                    if (item_id == 'o_interval') {
                        this.values[item_id].value = 0;
                    } else
                    if (item_id != 'email' && item_id != 'order_category') {
                        delete this.values[item_id];
                    }
                }
            }
            if ($('#numpapers').length == 0 && this.values.numpapers) {
                this.values.numpapers.value = 1;
            }
        },
        calculatePrice: function() {
            if ($('#doctype').length == 0) return;

            for (ind in OrderForm.beforeOnCalculatePrice) {
                OrderForm.beforeOnCalculatePrice[ind].call(OrderForm, this);
            }

            params = OrderForm.getSelectedParams();
            value = OrderForm.calcPriceForDoctype(params);

            if (!value) {
                window.setTimeout(OrderForm.calculatePrice, 2000);
            }

            if (OrderForm.primeSupport) {
                if (!OrderForm.primeSupport.instance) {
                    OrderForm.primeSupport.init();
                }
                if (result.total_without_feature > params.currencyRate * 300) {
                    OrderForm.primeSupport.show();
                } else {
                    OrderForm.primeSupport.hide();
                }
            }

            if (this.isPreview) {
                $('#value_cost_per_page').html(OrderForm.formatCurrency(value.cost_per_page, OrderForm.values.curr.value));
                $('#value_total_without_discount').html(OrderForm.formatCurrency(value.total_without_discount, OrderForm.values.curr.value));
                $('#value_discount').html(OrderForm.formatCurrency(value.discount, OrderForm.values.curr.value));
                this.setDiscountValue(value);
                $('#value_total').html(OrderForm.formatCurrency(value.total_with_discount, OrderForm.values.curr.value));

                var prev_value = $('#order_total_block .previous_value').html();
                if (prev_value) {
                    prev_value = prev_value.replace('(', '');
                    prev_value = prev_value.replace(')', '');
                    prev_value = parseFloat(prev_value);
                    if (prev_value == value.total_with_discount) $('#order_total_block .previous_value').hide();
                }
            } else {
                if ($('#cost_per_page').length && $('#cost_per_page')[0].tagName != 'INPUT') {
                    $('#cost_per_page').html(OrderForm.formatCurrency(value.cost_per_page, OrderForm.values.curr.value));
                }
                $('#total_without_discount').html(OrderForm.formatCurrency(value.total_without_discount, OrderForm.values.curr.value));
                OrderForm.setDiscountValue(value);
                if ($('#total').length > 0) {
                    if ($('#total')[0].tagName != 'INPUT') {
                        $('#total').html(OrderForm.formatCurrency(value.total_with_discount, OrderForm.values.curr.value));
                    }
                }

                if ($('#doctype')[0].tagName != 'SELECT' && !OrderForm.isPreview || OrderForm.isMobileResumes) {
                    $('.doctype_radiolist .name').css('width', '80%');
                    for (d_id in this.prices) {
                        if (OrderForm.tempDescountData != null) {
                            OrderForm.discountCodeType = OrderForm.tempDescountData.coefficient_type;
                            OrderForm.discountCodeCoefficient = OrderForm.tempDescountData.coefficient[d_id];
                            if (OrderForm.discountCodeCoefficient == undefined) {
                                OrderForm.discountCodeCoefficient = 0;
                            }
                        } else {
                            OrderForm.discountCodeType = 0;
                            OrderForm.discountCodeCoefficient = 0;
                        }

                        params.doctype_id = d_id;
                        res = this.calcCostPerPageForDoctype(params);

                        if (d_id == this.getDoctypeValue()) {
                            value = this.calcPriceForDoctype(params);
                            var current_doctype_id = params.doctype_id;

                            if (this.withCoverLetterIds[params.doctype_id]) {
                                var current_total_without_feature = value.total_without_feature;
                                cl_price = parseFloat(params.currencyRate) * 23 * Math.max(this.values.cover_letters ? this.values.cover_letters.value - 1 : 0, 0);
                                value.total = parseFloat(value.total) + cl_price;
                                value.total_with_discount = (parseFloat(value.total_with_discount) + cl_price).toFixed(2);
                                value.total_without_discount = (parseFloat(this.total_without_discount) + cl_price).toFixed(2);
                                current_total_without_feature = (parseFloat(current_total_without_feature) + cl_price + parseFloat(value.discount)).toFixed(2);
                                var current_row = $('#label_doctype_' + params.doctype_id);
                                res.cost_per_page_without_discount = (res.cost_per_page_without_discount + cl_price).toFixed(2);
                            }
                            this.setDiscountValue(value);
                            if ($('#total')[0].tagName != 'INPUT') {
                                $('#total').html(OrderForm.formatCurrency(value.total_with_discount, OrderForm.values.curr.value));
                            }
                        }

                        var originalPrice = res.cost_per_page_without_discount;

                        if (OrderForm.discountCodeCoefficient != 0 && originalPrice >= params.currencyRate * 30) {
                            if (OrderForm.discountCodeType == 0) {
                                var discountedPrice = Math.round((1 - OrderForm.discountCodeCoefficient) * res.cost_per_page_without_discount * 100) / 100;
                            } else if (OrderForm.discountCodeType == 1) {
                                var discountedPrice = (res.cost_per_page_without_discount - OrderForm.discountCodeCoefficient).toFixed(2);
                            }

                            $('#label_doctype_' + d_id).html('<div class="discountedPrice">' + OrderForm.formatCurrency(discountedPrice, OrderForm.values.curr.value) + " </div><div class='originalPrice'>" + OrderForm.formatCurrency(originalPrice, OrderForm.values.curr.value) + '</div>');
                            $('.doctype_radiolist .name').css('width', '75%');
                        } else {
                            $('#label_doctype_' + d_id).html(OrderForm.formatCurrency(res.cost_per_page_without_discount, OrderForm.values.curr.value));
                        }

                    }
                    OrderForm.discountCodeType = 0;
                    OrderForm.discountCodeCoefficient = 0;
                }
            }

            if (OrderForm.withCoverLetterIds[current_doctype_id] && OrderForm.isResumes) {
                if (OrderForm.tempDescountData == null || OrderForm.tempDescountData.coefficient[current_doctype_id] == undefined) {
                    current_row.html(OrderForm.formatCurrency(current_total_without_feature, OrderForm.values.curr.value));
                }
            }

            if (value.discount > 0) {
                this.showDiscount();
            } else {
                OrderForm.hideDiscount();
            }

            for (ind in OrderForm.afterOnCalculatePrice) {
                OrderForm.afterOnCalculatePrice[ind].call(OrderForm, this);
            }
        },
        showHidePreferredInputs: function() {

            $preff_wr_id = $('#preff_wr_id');

            if ($preff_wr_id.length > 0) {
                $('#preff_wr_id .add').click(OrderForm.addPreferredWriterInput);
                $('#preff_wr_id .delete').click(OrderForm.removePrefWriter);
                //$preff_wr_id.find('.add').click(OrderForm.addPreferredWriterInput);
                //$preff_wr_id.find('.delete').click(OrderForm.removePrefWriter);
            }

            if (OrderForm.version1) {
                //return;
            }

            if ($preff_wr_id.length > 0 && $('#prefwriter_urgency_attention').length > 0) {
                if (OrderForm.getSelectedHours() > 48) {
                    $('#prefwriter_urgency_attention').hide();
                    //$preff_wr_id.parent().parent().show();
                } else {
                    $('#prefwriter_urgency_attention').show();
                    //$preff_wr_id.parent().parent().hide();
                }
            }
        },

        setInputEvents: function() {
            $('#order_form').find('select,input,textarea').unbind('change').change(this.onInputChange);
            if (-[1, ]);
            else { //IE            
                $('#order_form').find('input[type=checkbox],input[type=radio]').unbind('click').click(this.onInputChange);
            }

            if (!(window.navigator.userAgent.indexOf("mozilla") != -1) && !OrderForm.isResumes) {
                $('#order_form').find('input,textarea').bind('focus', function() {
                    this.originalvalue = this.value;
                });
                $('#order_form').find('input,textarea').bind('blur', function() {
                    if (this.value != this.originalvalue) {
                        $(this).change();
                    }
                });
            }

            $('#extend_days,#extend_hours').change(OrderForm.deadlineExtendChange);

            if (this.isResubmit && !this.isPreview) {
                $('#urgency').change(OrderForm.deadlineExtendByUrgencyChange);
            }
            $('textarea[maxlength]').bind('keyup', OrderForm.validateTextareaMaxlength);
        },

        skipSwitchForms: function() {
            var cover_letters = $('#cover_letters_holder');
            var doc = $('#doctype_' + OrderForm.new_form_id).parent().parent().find('td.cover_letters label');
            $('#cover_letters').val(1);

            if (doc.length) {
                cover_letters.show();
                doc.after(cover_letters);
            } else {
                cover_letters.hide();
            }
            OrderForm.updateWrLevels(OrderForm.new_form_id);
            OrderForm.new_form_id = undefined;
            this.setFormValues();
            this.pickOutFreeFeatures();
            this.calculatePrice();
            $('#cover_letters').change(OrderForm.onInputChange);
        },

        updateWrLevels: function(doctype_id) {
            if (OrderForm.isResumes && OrderForm.wrLevels && doctype_id && OrderForm.wrLevels[doctype_id]) {
                OrderForm.updateInputptions($("#wrlevel"), OrderForm.wrLevels[doctype_id]);
            }
        },

        updateInputptions: function(elm, options) {
            var html = "";
            if (elm && elm.length && options) {
                $.each(options, function(k, v) {
                    html += "<option value=\"" + k + "\">" + v + "</option>";
                });
                if (html) {
                    if (elm.is("select")) {
                        elm.html(html);
                    }
                }
            }
        },

        switchForms: function(new_form_id) {
            OrderForm.new_form_id = new_form_id;
            for (ind in OrderForm.beforeSwitchForms) {
                OrderForm.beforeSwitchForms[ind].call(OrderForm, OrderForm.new_form_id);
            }

            if (OrderForm.new_form_id != undefined) {
                $order_details = $('#order_details');
                $order_details.find('[id]').each(function() {
                    this.id += '_old';
                });
                $order_details[0].id += '_old';

                $order_details.after('<tbody id="order_details" style="display: none"/>');
                $('#order_details').html(this.loaded[OrderForm.new_form_id]);


                this.setFormValues();
                this.showHidePreferredInputs();
                this.pickOutFreeFeatures();
                this.calculatePrice();
                this.setInputEvents();
                this.fillNumpages();
                this.showHidePages();
                this.enableHints();
                this.initOrderCategories();

                $order_details.remove();
                document.getElementById('order_details').style.display = '';
                if ($('[name=accept]') && $('[name=accept]').length > 0 && $('[name=accept]')[0].checked == true) {
                    $('[name=accept]')[0].checked = false;
                    $('[name=accept]')[0].checked = true;
                }

                this.repaintTable();
                this.enableSubmit();

                if (this.isResubmit && !this.isPreview) {
                    OrderForm.deadlineExtendByUrgencyChange();
                    OrderForm.deadlineExtendChange();
                    OrderForm.checkPromoCode();
                }
            }

            for (ind in OrderForm.afterSwitchForms) {
                OrderForm.afterSwitchForms[ind].call(OrderForm);
            }
            OrderForm.notEnterSubmit();
        },

        checkPromoCode: function() {
            var promoElm = $("#promo");
            promoCode = promoElm.val().replace(/^\s\s*/, '').replace(/\s\s*$/, '').toLowerCase();
            email_value = OrderForm.values.email ? OrderForm.values.email.value : '';
            if (OrderForm.isResumes || !email_value) {
                email_value = null;
            }
            if (promoCode != '' && !promoElm.hasClass("default-hint")) {
                numpages = OrderForm.values.numpages ? numpages = OrderForm.values.numpages.value : 1;
                var doctype = this.getDoctypeValue();
                wrlevel = this.getWrlevelValue();
                urgency = this.getUrgencyValue();

                if (this.isResubmit && !this.isPreview && OrderForm.adminAuthorized) {
                    loc = location.href;
                    loc = loc.substring(0, loc.indexOf('resubmit') - 1);
                } else {
                    loc = '/order';
                }

                loc = loc + '.check-promo-code/' + encodeURIComponent(email_value) + '/' + encodeURIComponent(promoCode) + '/' + encodeURIComponent(numpages) + '/' + encodeURIComponent(doctype) + '/' + encodeURIComponent(wrlevel) + '/' + encodeURIComponent(urgency);
                if (this.isResubmit && OrderForm.orderCode != '') {
                    loc = loc + '/' + OrderForm.orderCode;
                } else {
                    loc = loc + '/null';
                }

                var get_doctypes = OrderForm.showOriginalPrice ? 1 : 0;
                if (get_doctypes) {
                    loc = loc + '/' + encodeURIComponent(get_doctypes)
                }

                if (!this.isResubmit) {
                    $.getJSON(
                        loc, {},
                        OrderForm.onCheckPromoCode
                    );
                }

            } else {
                if (this.isResumes) {
                    OrderForm.onCheckPromoCode();
                }
                OrderForm.discountCodeCoefficient = 0;
                OrderForm.discountCodeType = 0;
                OrderForm.calculatePrice();
            }
        },

        onCheckPromoCode: function(data) {
            if (!OrderForm.isResumes && !OrderForm.isMobileResumes) {
                OrderForm.discountCodeCoefficient = data.coefficient;
                OrderForm.discountCodeType = data.coefficient_type;
            } else {
                OrderForm.tempDescountData = data
            }
            OrderForm.calculatePrice();
        },

        getDoctypeValue: function() {
            result = undefined;
            $doctype = $('#doctype');
            if ($doctype.length > 0) {
                if ($doctype[0].tagName == 'INPUT' || $doctype[0].tagName == 'SELECT') {
                    result = $doctype.val();
                } else {
                    result = $doctype.find('input:checked').val();
                }
            }
            return result;
        },

        getWrlevelValue: function() {
            result = undefined;
            $wrlevel = $('#wrlevel');
            if ($wrlevel.length > 0) {
                if ($wrlevel[0].tagName == 'INPUT' || $wrlevel[0].tagName == 'SELECT') {
                    result = $wrlevel.val();
                } else {
                    result = $wrlevel.find('input:checked').val();
                }
            }
            return result;
        },

        getUrgencyValue: function() {
            result = undefined;
            $urgency = $('#urgency');
            if ($urgency.length > 0) {
                if ($urgency[0].tagName == 'INPUT' || $urgency[0].tagName == 'SELECT') {
                    result = $urgency.val();
                } else {
                    result = $urgency.find('input:checked').val();
                }
            }
            return result;
        },

        setDoctypeValue: function(value) {
            $doctype = $('#doctype');
            if ($doctype[0].tagName == 'INPUT' || $doctype[0].tagName == 'SELECT') {
                $doctype.val(value);
            } else {
                $('#doctype_' + value).attr('checked', 'checked');
            }
        },

        saveValue: function(element) {

            if (element.name == 'preff_wr_id[]') {
                this.values.preff_wr_id = new Array();
                $('#preff_wr_id input').each(function() {
                    OrderForm.values.preff_wr_id[OrderForm.values.preff_wr_id.length] = this.value;
                });
            } else
            if (element.tagName == 'SELECT') {
                var elementId = element.id;
                var elementObj = $('#' + elementId);
                if (element.selectedIndex == -1) {
                    element.selectedIndex = 0;
                }

                if (elementObj.attr('multiple')) {
                    var values = elementObj.val();
                    OrderForm.values[element.id] = {};
                    for (var key in values) {
                        OrderForm.values[element.id][key] = {
                            value: values[key],
                            text: element.options[key].text,
                            checked: ''
                        };
                    }
                } else {
                    OrderForm.values[element.id] = {
                        value: element.value,
                        text: element.options[element.selectedIndex].text,
                        checked: ''
                    };
                }
            } else
            if (element.type == 'checkbox') {
                OrderForm.values[element.id] = {
                    value: element.value,
                    text: '',
                    checked: element.checked
                };
            } else {
                OrderForm.values[element.id] = {
                    value: element.value,
                    text: '',
                    checked: ''
                };
            }
        },

        calcTechPrices: function(d, w, u, c, price) {
            var tech_doctype = true,
                tech_category = false,
                intricate_category = false;
            for (i in this.nonTechDoctypes) {
                if (this.nonTechDoctypes[i] == d) {
                    tech_doctype = false;
                    break;
                }
            }
            for (i in this.techCategories) {
                if (this.techCategories[i] == c) {
                    tech_category = true;
                    break;
                }
            }

            for (i in this.intricateCategories) {
                if (this.intricateCategories[i] == c) {
                    intricate_category = true;
                    break;
                }
            }

            if (tech_doctype && tech_category) {
                price += 10;
            }

            if (tech_doctype && intricate_category) {
                price += 3;
            }

            if (c && OrderForm.categoriesPriceChange && tech_doctype) {
                c = parseInt(c);

                if (OrderForm.categoriesPriceChange[c]) {
                    price += OrderForm.categoriesPriceChange[c];
                }
            }

            return price;
        },

        calcCostPerPageForDoctype: function(params) {
            this.d = params.doctype_id;
            this.u = params.urgency_id;
            this.w = params.wrlevel_id;
            this.c = params.category_id;

            result = {};

            result.cost_per_page_without_discount = 0.0;

            try {
                if (!OrderForm.prices[this.d][this.w][this.u]) a = a.a;
                for (i in OrderForm.prices[this.d][this.w][this.u]) {
                    if (!isNaN(parseFloat(OrderForm.prices[this.d][this.w][this.u][i]))) {
                        result.cost_per_page_without_discount = Math.max(
                            parseFloat(OrderForm.prices[this.d][this.w][this.u][i]),
                            result.cost_per_page_without_discount);
                    }
                }
            } catch (e) {
                OrderForm.priceError(this.d, this.w, this.u);
                //console.log('No prices: d: ' + this.d + ', w: ' + this.w + ', u: ' + this.u);
            }

            result.cost_per_page_without_discount = this.calcTechPrices(this.d, this.w, this.u, this.c, result.cost_per_page_without_discount);

            result.cost_per_page_without_discount *= (params.currencyRate * (params.interval > 0 ? 2 : 1));

            result.cost_per_page_without_discount = Math.round(result.cost_per_page_without_discount * 100) / 100;

            return result;
        },

        repaintTable: function() {
            $trs = $('#order_form table tbody tr');
            j = 0;
            for (i = 0; i < $trs.length; i++) {
                if ($trs[i].cells[0].nodeName == 'TH') j = 0;
                if ($trs[i].style.display != 'none') j++;
                if (j % 2) {
                    $($trs[i]).addClass('even');
                } else {
                    $($trs[i]).removeClass('even');
                }
            }

            if (window.customizeStylePremiumWriter) {
                customizeStylePremiumWriter();
            }
            if (OrderForm.hideZeroPriceDoctypes) {
                OrderForm.hideZeroPriceDoctypes();
            }
        },

        calculateTZOffset: function() {
            $deadline = $('#deadline');
            deadline_value = $deadline.val();
            timestamp = parseInt($('#original_deadline').val());
            if (!$deadline.length || typeof(deadline_value) == 'undefined' || !timestamp) {
                return;
            }

            i = 0;
            do {
                i++;
                deadline_converted = new Date(
                    timestamp +
                    Math.floor(i / 2) * 60 * 60 * 1000 * (i % 2 == 1 ? -1 : 1)
                ).format('Y-m-d H:i:s');
            } while (deadline_value != deadline_converted);

            this.tzOffset = Math.floor(i / 2) * (i % 2 == 1 ? -1 : 1);
        },

        calculateFeaturesPrices: function(params) {
            for (var ind in OrderForm.featurePrices) {
                if (document.getElementById('additional_' + ind) != null) {
                    if (!OrderForm.values['additional_' + ind]) {
                        OrderForm.onInputChange.call(document.getElementById('additional_' + ind));
                    }
                    f_price = Math.round(100 * parseFloat(OrderForm.featurePrices[ind](ind, OrderForm.getVasCount(ind))) * params.currencyRate) / 100;
                    var no_price = OrderForm.formatCurrency('0.00', OrderForm.values.curr.value);

                    if (!OrderForm.adminAuthorized && OrderForm.fieldDoctypes[ind] && OrderForm.isFreeFeatureByAdmin(OrderForm.fieldDoctypes[ind])) {
                        f_price = 0;
                    }
                    if (!params.popup) {
                        OrderForm.showFeaturePrice(ind,
                            f_price > 0 ?
                            OrderForm.formatCurrency(f_price.toFixed(2), OrderForm.values.curr.value) :
                            no_price
                        );
                    }
                }
            }
        },
        isFreeFeatureByAdmin: function(feature_doctype_id) {
            if (OrderForm.admin_free_vas && OrderForm.admin_free_vas[feature_doctype_id]) {
                return true;
            }
            return false;
        },
        isFreeFeature: function(feature_doctype_id) {
            params = OrderForm.getSelectedParams();

            if (OrderForm.isFreeFeatureByAdmin(feature_doctype_id)) {
                return true;
            }

            if (OrderForm.free_vas[params.doctype_id]) {
                if (OrderForm.free_vas[params.doctype_id][params.urgency_id]) {
                    if (OrderForm.free_vas[params.doctype_id][params.urgency_id][params.wrlevel_id]) {
                        if (OrderForm.free_vas[params.doctype_id][params.urgency_id][params.wrlevel_id][feature_doctype_id]) {
                            return true;
                        }
                    }
                }
            }

            return false;
        },
        pickOutFreeFeatures: function() {
            params = OrderForm.getSelectedParams();
            for (ind in OrderForm.featurePrices) {
                if (document.getElementById('additional_' + ind) != null && $('#additional_' + ind).is(':visible') && OrderForm.values['additional_' + ind]) {
                    field_doctype = OrderForm.fieldDoctypes[ind];
                    $el = $('#additional_' + ind);

                    $('#row_additional_' + ind).find('.label').removeClass("free_feature_active");
                    if (OrderForm.isFreeFeature(field_doctype)) {
                        $('#row_additional_' + ind).find('.label').addClass('free_feature_active');

                        OrderForm.values['additional_' + ind].checked = "checked";
                        $el.attr('checked', 'checked');
                        if ($el[0]) {
                            $el[0].checked = 'checked';
                        }
                        $el.change();
                    }
                }
            }
        },

        calcPriceForDoctype: function(params) {
            this.d = params.doctype_id;
            this.u = params.urgency_id;
            this.w = params.wrlevel_id;
            this.cl = params.cover_letters;
            this.interval = params.interval;

            this.p = params.numpages;
            this.pp = params.numpapers;

            this.per_page = this.cost_per_page = this.calcCostPerPageForDoctype(params).cost_per_page_without_discount;

            this.group = 0;
            if (this.price_groups[this.d]) {
                for (i in this.price_groups[this.d]) {
                    if ((parseInt(this.price_groups[this.d][i].to) == 0 || parseInt(this.price_groups[this.d][i].to) >= parseInt(this.p)) &&
                        parseInt(this.price_groups[this.d][i].from) <= parseInt(this.p)) {
                        this.group = i;
                        break;
                    }
                }
            }

            if (!this.prices[this.d]) {
                return false;
            }

            if (this.prices[this.d][this.w] == undefined) {
                var defaultDoctype = $('#doctype td input[type=radio]')[0];
                $(defaultDoctype).click();
                $(defaultDoctype).change();
            } else {
                this.per_page = Math.round(100 * (this.prices[this.d][this.w][this.u][this.group] * (parseInt(this.interval) + 1)) * params.currencyRate) / 100;
            }
            this.total_without_discount = this.cost_per_page * this.p * this.pp;

            if (this.group == 0) {
                this.discount = 0;
                for (from in OrderForm.cppDiscountRules) {
                    if (this.p >= from) {
                        this.discount = this.total_without_discount * OrderForm.cppDiscountRules[from] / 100;
                    }
                }
            } else {
                this.discount = (this.cost_per_page - this.per_page) * this.p * this.pp;
            }
            this.discount = Math.round(this.discount * 100) / 100;

            discount_by_papers = 0.0;
            if (params.numpapers >= 2 && params.numpapers <= 3) {
                discount_by_papers = this.total_without_discount * 0.05;
            } else
            if (params.numpapers >= 4 && params.numpapers <= 5) {
                discount_by_papers = this.total_without_discount * 0.10;
            } else
            if (params.numpapers >= 6) {
                discount_by_papers = this.total_without_discount * 0.15;
            }

            if (discount_by_papers > this.discount) {
                this.discount = discount_by_papers;
            }

            //set general discount
            this.generalDiscountCalculated = this.total_without_discount * this.generalDiscount / 100;
            if (this.generalDiscountCalculated > this.discount) {
                this.discount = this.generalDiscountCalculated;
            }

            flag = false;
            hours = OrderForm.hours[params.doctype_id][params.urgency_id];

            switch (parseInt(OrderForm.discountCodeType)) {
                case 0:
                    if (this.total_without_discount >= params.currencyRate * 30 && OrderForm.discountCodeCoefficient * this.total_without_discount > this.discount) {
                        cl_price_dis = 0;
                        if (this.cl > 1) {
                            cl_price_dis = parseFloat(params.currencyRate) * 23 * (this.cl - 1);
                        }
                        this.discount = OrderForm.discountCodeCoefficient * (this.total_without_discount + cl_price_dis);
                    }
                    break;
                case 1:
                    if (this.total_without_discount >= params.currencyRate * 30 && OrderForm.discountCodeCoefficient > this.discount) {
                        this.discount = OrderForm.discountCodeCoefficient;
                    }
                    break;
            }

            this.total_without_feature = this.total_without_discount - this.discount;

            this.calculateFeaturesPrices(params);

            non_discountable = 0;
            for (ind in this.featurePrices) {
                if ((OrderForm.version1 && OrderForm.version1_1) && !OrderForm.isPreview) {
                    continue;
                }
                if (
                    (feature = document.getElementById('additional_' + ind)) != null &&
                    OrderForm.values['additional_' + ind] &&
                    (
                        feature.type != 'checkbox' ||
                        feature.checked == true
                    )
                ) {
                    price = parseFloat(OrderForm.featurePrices[ind](ind, OrderForm.getVasCount(ind))) * params.currencyRate;
                    if (OrderForm.fieldDoctypes[ind] && OrderForm.isFreeFeatureByAdmin(OrderForm.fieldDoctypes[ind])) {
                        price = 0;
                    }
                    if (OrderForm.featureDiscountable[ind]) {
                        this.total_without_discount += price;
                    } else {
                        non_discountable += price;
                    }
                }
            }

            if (document.getElementById('preff_wr_id') && ($preferred = $('#preff_wr_id input')).length > 0) {
                if (hours > 48) {
                    for (i = 0; i < $preferred.length; i++) {
                        if ($preferred[i].value.match(/[0-9]+/)) {
                            flag = true;
                            break;
                        }
                    }
                }
                if (flag) {
                    var preff_wr_margin = typeof JsReg == 'object' && JsReg.get('preff_wr_margin') ? JsReg.get('preff_wr_margin') : 1.2;
                    this.total_without_discount *= preff_wr_margin;
                }
            }

            for (ind in OrderForm.featurePrices) {
                if (document.getElementById('additional_' + ind) != null && OrderForm.values['additional_' + ind]) {
                    if ((OrderForm.version1 && OrderForm.version1_1) && !OrderForm.isPreview) {
                        continue;
                    }
                    f_price = Math.round(100 * parseFloat(OrderForm.featurePrices[ind](ind, OrderForm.getVasCount(ind))) * params.currencyRate) / 100;
                    if (!OrderForm.featureDiscountable[ind] &&
                        (
                            (feature = document.getElementById('additional_' + ind)) != null &&
                            (
                                feature.type != 'checkbox' ||
                                feature.checked == true
                            )
                        )
                    ) {
                        if (OrderForm.fieldDoctypes[ind] && OrderForm.isFreeFeatureByAdmin(OrderForm.fieldDoctypes[ind])) {
                            f_price = 0;
                        }
                        this.total_without_discount += f_price;
                    }
                }
            }

            this.total_with_discount = this.total_without_discount - this.discount;

            result = {
                cost_per_page: parseFloat(this.cost_per_page).toFixed(2),
                total_without_discount: parseFloat(this.total_without_discount).toFixed(2),
                discount: parseFloat(this.discount).toFixed(2),
                total: parseFloat(this.total_with_discount).toFixed(2),
                total_with_discount: parseFloat(this.total_with_discount).toFixed(2),
                total_without_feature: parseFloat(this.total_without_feature).toFixed(2)
            };

            return result;
        },

        savePreloadedJson: function(data) {
            for (id in data.html) {
                OrderForm.loaded[id] = data.html[id];
            }
            for (id in data.price_groups) {
                OrderForm.price_groups[id] = data.price_groups[id];
            }
            for (id in data.prices) {
                OrderForm.prices[id] = data.prices[id];
            }
            for (id in data.hours) {
                OrderForm.hours[id] = data.hours[id];
            }

            for (id in data.limits) {
                OrderForm.limits[id] = data.limits[id];
            }

        },

        preload: function() {
            //  OMG
            var loc = location.href;

            if (this.isResubmit && loc.indexOf('resubmit') >= 0 && !this.isPreview) {
                loc = loc.substring(0, loc.indexOf('resubmit') + 8) + '.popular/' + loc.substring(loc.indexOf('resubmit') + 9, loc.indexOf('resubmit') + 17);
            } else if (this.isQuote && !this.isPreview) {
                loc = loc.substring(0, loc.indexOf('quote') + 5) + '.popular/';

                if (OrderForm.step != 0) {
                    loc += '?step=' + OrderForm.step;
                }
            } else if (OrderForm.step != 0) {
                loc = '/order.popular?step=' + OrderForm.step;
            } else if (OrderForm.isCalculator) {
                loc = '/order/calculator.popular/';
            } else {
                loc = '/order.popular/';
            }

            $.getJSON(loc, {}, OrderForm.savePreloadedJson);
        },

        checkPassword: function() {
            if (OrderForm.step == 3) return;
            email_value = OrderForm.values.email ? OrderForm.values.email.value : '';
            if (email_value != '') {
                numpages = OrderForm.values.numpages ? numpages = OrderForm.values.numpages.value : 0;
                var doctype = this.getDoctypeValue();
                if (this.isResubmit && !this.isPreview && OrderForm.adminAuthorized) {
                    loc = location.href;
                    loc = loc.substring(0, loc.indexOf('resubmit') - 1);
                } else {
                    loc = '/order';
                }

                loc = loc + '.check-email/' + encodeURIComponent(email_value);

                $.getJSON(
                    loc, {},
                    OrderForm.onCheckPassword
                );
            } else {
                OrderForm.hidePassword();
            }

        },

        onCheckPassword: function(data) {
            if (data) {
                //set general discount
                if (data.generalDiscount != undefined && !isNaN(parseFloat(data.generalDiscount))) {
                    OrderForm.generalDiscount = data.generalDiscount;
                    OrderForm.calculatePrice();
                }

                OrderForm.showPassword();
            } else {
                OrderForm.hidePassword();
            }
        },

        onInputChange: function() {
            // Before onInputChange
            if (typeof FormSaver != 'undefined') {
                FormSaver.sendAjax(this);
            }

            for (ind in OrderForm.beforeOnInputChange) {
                OrderForm.beforeOnInputChange[ind].call(OrderForm, this);
            }
            if (this.name == 'doctype') {
                OrderForm.onDoctypeChange();
                return;
            }
            OrderForm.saveValue(this);

            if (this.id == 'o_interval' || this.id == 'wrlevel' || this.id == 'urgency') {
                OrderForm.showHidePages();
                if (OrderForm.BD_validation) {
                    checkMaxNumpages();
                }
                OrderForm.fillNumpagesWithLimit();
            }
            if (this.id == 'urgency') OrderForm.showHidePreferredInputs();
            OrderForm.enableSubmit();

            if (this.id == 'email' || this.id == 'promo' || this.id == 'numpages') {
                OrderForm.checkPromoCode();
            }
            if (this.id == 'email' && !OrderForm.isResubmit) {
                OrderForm.checkPassword();
            }
            if (this.id != 'doctype' && this.name != 'doctype') {
                OrderForm.calculatePrice()
            }
            if (OrderForm.hideZeroPriceDoctypes) {
                OrderForm.hideZeroPriceDoctypes();
            }
            if (window.customizeStylePremiumWriter) {
                customizeStylePremiumWriter();
            }
            OrderForm.onInputChangeClearValidationError(this);

            // PickUp Free Feature
            if ((this.name == 'doctype') || (this.name == 'wrlevel') || (this.name == 'urgency')) {
                OrderForm.pickOutFreeFeatures();
            }
            if (OrderForm.order_features.onInputChange) {
                OrderForm.order_features.onInputChange();
            }

            if (OrderForm.showAcceptError && this.name == 'accept' && $(this)[0] && $(this)[0].checked == true) {
                if ($(this).val() == '1') {
                    $('#error_accept').hide();
                    $('#submit_order_form').removeAttr('disabled');
                } else {
                    $('#error_accept').show();
                    $('#submit_order_form').attr('disabled', 'disabled');
                }
            }

            // After onInputChange
            for (ind in OrderForm.afterOnInputChange) {
                OrderForm.afterOnInputChange[ind].call(OrderForm, this);
            }
        },

        onInputChangeClearValidationError: function(input) {},

        onAjaxRespond: function(data) {
            OrderForm.loaded[data.id] = data.html;

            for (id in data.price_groups) {
                OrderForm.price_groups[id] = data.price_groups[id];
            }
            for (id in data.prices) {
                OrderForm.prices[id] = data.prices[id];
            }
            for (id in data.hours) {
                OrderForm.hours[id] = data.hours[id];
            }
            for (id in data.limits) {
                OrderForm.limits[id] = data.limits[id];
            }

            $('#doctype_loading').remove();
            $('#order_details').find('input:not(.disabled_email_input),select,textarea').removeAttr('disabled');

            OrderForm.doctype = data.id;

            OrderForm.switchForms(data.id);
            if (OrderForm.sp_validation) {
                ckeckOnSwitchDoctype(OrderForm.was_proceed);
            }

            if (OrderForm.doctype == 182) {
                $('#operational_system option:first').attr("selected", true);
            }

        },

        focusOnDoctype: function() {
            if (OrderForm.sp_validation) {
                ckeckOnSwitchDoctype(OrderForm.was_proceed);
            }
            try {
                $doctype = $('#doctype');
                if ($doctype[0].tagName == 'INPUT' || $doctype[0].tagName == 'SELECT') {
                    $doctype[0].focus();
                } else {
                    $dt = $('#doctype_' + this.doctype);
                    $dt[0].checked = true;
                    $dt[0].focus();
                }
            } catch (e) {}
        },

        onDoctypeChange: function() {
            $order_details = $('#order_details');
            OrderForm.values.cover_letters = 1;
            this.doctype = this.getDoctypeValue();

            if (!this.loaded[this.doctype] && !OrderForm.isResumes) {
                $('#doctype').after('<span id="doctype_loading"><img src="/res/img/template/doctype-loading.gif" />Loading...</span>');
                $('#order_details').find('input:not(.disabled_email_input),select,textarea').attr('disabled', 'disabled');
                if (OrderForm.BD_validation) {
                    checkMaxNumpages();
                    OrderForm.values.numpages = 1;
                }
                //      OMG
                var loc = location.href;
                if (OrderForm.isResubmit) {
                    loc = loc.substring(0, loc.indexOf('resubmit') + 8) + '.ajax/' + loc.substring(loc.indexOf('resubmit') + 9, loc.indexOf('resubmit') + 17);
                    if (loc[loc.length - 1] != '/') {
                        loc += '/';
                    }
                } else
                if (OrderForm.isQuote) {
                    loc = loc.substring(0, loc.indexOf('quote') + 5) + '.ajax/';
                    if (loc[loc.length - 1] != '/') {
                        loc += '/';
                    }
                } else
                if (OrderForm.isCalculator) {
                    loc = '/order/calculator.ajax/';
                } else {
                    loc = '/order.ajax/';
                }
                urll = loc + this.doctype;
                if (OrderForm.step != 0) {
                    urll = loc + this.doctype + '?step=' + OrderForm.step;
                }

                $.getJSON(urll, {}, OrderForm.onAjaxRespond);
            } else {
                OrderForm.switchForms(this.doctype);
                OrderForm.focusOnDoctype();
                if (OrderForm.doctype == 182 && $('#operational_system option').text() != 'select') {
                    $('#operational_system option:first').attr("selected", true);
                }
            }
            if (OrderForm.sp_validation) {
                ckeckOnSwitchDoctype(OrderForm.was_proceed);
            }
            if (OrderForm.BD_validation) {
                ckeckOnSwitchDoctype(this.doctype);
            }
            var cover_letters = $('#cover_letters');
            if (cover_letters.length) {
                OrderForm.values.cover_letters.value = 1;
                cover_letters.val(1);
            }
            this.attachBubblePopup();
        },
        attachBubblePopup: function() {
            var popup = $('#bubble-poppup');
            var popupOrigin = $('#bubble-popup-origin');
            var accept = $('.accept');
            popupOrigin.remove().appendTo("body");

            accept.click(function(e) {
                return false;
            });


            if (popup.length && popupOrigin.length) {
                var offset = popup.offset();
                var hght = popupOrigin.height();
                offset.left = offset.left - popupOrigin.width() / 2 + popup.width() / 2;
                offset.top = offset.top - hght;
                popupOrigin.css({
                    'top': offset.top,
                    'left': offset.left
                });

                accept.hover(function() {
                    popupOrigin.fadeIn();
                    return false;
                }, function() {
                    popupOrigin.fadeOut();
                    return false;
                });
            }
        },
        addPreferredWriterInput: function(value) {
            $pref_input = $('#preff_wr_id');
            $parent = $pref_input.find('li:last');
            $element = $('<li>' + $parent.html() + '</li>');
            $element.find('input')
                .val('')
                .change(OrderForm.onInputChange)
                .removeClass('validation_error');
            $element.find('div.validation_error').hide();
            $parent.parent().append($element);

            $pref_input.find('.add:first').remove();

            $inputs = $pref_input.find('input');
            if ($inputs.length == 2) {
                $inputs.after(OrderForm.removePrefWriterImg);
            }
            if ($inputs.length > OrderForm.max_preferred_writers) {
                if ($('#preff_wr_id .add').length == 0) {
                    $pref_input.find('.add').remove();
                } else {
                    $('#add').css('display', 'none');
                }
            } else {
                $pref_input.find('.add')
                    .show()
                    .click(OrderForm.addPreferredWriterInput);
            }
            OrderForm.onInputChangeClearValidationError();
            $pref_input.find('.delete').click(OrderForm.removePrefWriter);

            OrderForm.onAfterAddPreferredWriterInput();
        },

        removePrefWriter: function() {
            if ($(this).parent().find('input').val() == '' || window.confirm('Yes, I confirm I want to delete this preferred writer')) {
                $(this).parent().remove();
                $inputs = $('#preff_wr_id input');
                if ($('#preff_wr_id .add').length == 0 && $('#add').length == 0) {
                    if (!OrderForm.BE) {
                        $('#preff_wr_id .delete:last').after('<img src="/res/img/template/addgreen16x16.gif" alt="+" title="Add writer" class="add" />');
                    } else {
                        $('#preff_wr_id .delete:last').after('<img src="/res/img/order/addgreen16x16.gif" alt="+" title="Add writer" class="add" />');
                    }
                    $('#preff_wr_id .add').click(OrderForm.addPreferredWriterInput);
                } else {
                    $('#add').css('display', 'block');
                }
                if ($inputs.length == 1) {
                    $('#preff_wr_id .delete').remove();
                }
                OrderForm.saveValue($inputs[0]);
                OrderForm.calculatePrice();
                OrderForm.onInputChangeClearValidationError();
            }

            OrderForm.onAfterRemovePrefWriter();
        },

        fillNumpages: function() {
            $options = $('#numpages option');
            if (OrderForm.values.o_interval) {
                spacing = OrderForm.values.o_interval.value;
                words = OrderForm.numpages_append_words && !this.nonWordsProducts[this.doctype];
                words_per_page = 275 * (spacing == '1' ? 2 : 1);

                $num_pg_ord = $('#num_pg_ord');
                if ($num_pg_ord.length) {
                    $num_pg_ord.html($num_pg_ord.html().replace(/(\d+)/, words_per_page));
                }
                if (words) {
                    for (i = 0; i < $options.length; i++) {
                        if ($options[i].value == 0) {
                            $options[i].text = 'select';
                        } else {
                            $options[i].text = $options[i].value + ' page(s) / ' + $options[i].value * words_per_page + ' words'
                        }
                    }
                } else {
                    for (i = 0; i < $options.length; i++) {
                        if ($options[i].value == 0) {
                            $options[i].text = 'select';
                        } else {
                            $options[i].text = $options[i].value;
                        }
                    }
                }
            } else {
                for (i = 0; i < $options.length; i++) {
                    if ($options[i].value == 0) {
                        $options[i].text = 'select';
                    } else {
                        $options[i].text = $options[i].value;
                    }
                }
            }

            OrderForm.$pages_options = [];
            for (i = 0; i < $options.length; i++) {
                OrderForm.$pages_options.push({
                    v: parseInt($options[i].value),
                    t: $options[i].text
                });
            }

            //set data-value for select numpages (fix for ESSAY-748)
            var dataValue = $('#numpages_old').attr('data-value');
            dataValue && $('#numpages').attr('data-value', dataValue).val(dataValue);
        },
        fillNumpagesWithLimit: function() {
            $options = $('#numpages option');
            if (OrderForm.values.o_interval) {
                spacing = OrderForm.values.o_interval.value;
                words = OrderForm.numpages_append_words && !this.nonWordsProducts[this.doctype];
                words_per_page = 275 * (spacing == '1' ? 2 : 1);

                $num_pg_ord = $('#num_pg_ord');
                if ($num_pg_ord.length) {
                    $num_pg_ord.html($num_pg_ord.html().replace(/(\d+)/, words_per_page));
                }
                if (words) {
                    for (i = 0; i < $options.length; i++) {
                        if ($options[i].value == 0) {
                            $options[i].text = 'select';
                        } else {
                            $options[i].text = $options[i].value + ' page(s) / ' + $options[i].value * words_per_page + ' words'
                        }
                    }
                } else {
                    for (i = 0; i < $options.length; i++) {
                        if ($options[i].value == 0) {
                            $options[i].text = 'select';
                        } else {
                            $options[i].text = $options[i].value;
                        }
                    }
                }
            }
        },

        showHidePages: function() {
            try {
                limit = OrderForm.limits[this.doctype][this.values.wrlevel.value][this.values.urgency.value];

                if (limit != undefined && OrderForm.values.o_interval != undefined) {
                    spacing = parseInt(OrderForm.values.o_interval.value);
                    limit = parseInt(limit / (spacing + 1));
                }
            } catch (e) {
                if (OrderForm.$pages_options[0] != undefined && OrderForm.$pages_options[0].v > 1) {
                    limit = OrderForm.$pages_options.length + OrderForm.$pages_options[0].v - 1;
                } else {
                    limit = OrderForm.$pages_options.length;
                }
            }
            if (limit == undefined) {
                if (OrderForm.$pages_options[0] != undefined && OrderForm.$pages_options[0].v > 1) {
                    limit = OrderForm.$pages_options.length + OrderForm.$pages_options[0].v - 1;
                } else {
                    limit = OrderForm.$pages_options.length;
                }

            }

            $numpages = $('select#numpages');
            if ($numpages.length > 0) {
                val = $numpages.val();

                max = 0;
                opts = $numpages[0].options;
                for (i = 0; i < opts.length; i++) {
                    option = opts[i];
                    opt_value = parseInt(option.value);
                    if (opt_value > limit || OrderForm.$pages_options[i].v != option.value) {
                        $numpages[0].removeChild(option);
                        i--;
                    } else
                    if (opt_value > max) {
                        max = opt_value;
                    }
                }
                if (max < limit) {
                    for (i = 0; i < OrderForm.$pages_options.length; i++) {
                        opt = OrderForm.$pages_options[i];
                        if (opt.v <= limit && opt.v > max) {
                            $numpages.append('<option value="' + opt.v + '">' + opt.t + '</option>');
                        }
                    }
                }

                $options = $('#numpages option');
                spacing = OrderForm.values.o_interval.value;
                if (spacing == '1' && $options.length == 1 && $options[0].value == '0') {
                    $options.parent().append('<option value="1"></option>');
                    OrderForm.fillNumpagesWithLimit();
                    val = 1;
                }
                $numpages.val(val);
                $numpages.change();
            }

        },

        deadlineExtendByUrgencyChange: function() {
            if (OrderForm.orderDate && $('#deadline').length) {
                var hours = 0;
                urgency_id = $('#urgency').val();
                current_doctype = $('#doctype').val();
                if (!current_doctype) {
                    current_doctype = $('#doctype input:radio:checked').val();
                }
                if (OrderForm.hours[current_doctype] != undefined && OrderForm.hours[current_doctype][urgency_id] != undefined) {
                    hours = OrderForm.hours[current_doctype][urgency_id];
                    if (hours > 0) {
                        originalOrderDate = parseInt(OrderForm.orderDate);
                        real = new Date(originalOrderDate * 1000 + Math.abs(hours * 60 * 60 * 1000) + OrderForm.tzOffset * 60 * 60 * 1000);
                        $('#deadline').val(real.format('Y-m-d H:i:s'));
                        $('#original_deadline').val(real.format('U') * 1000 - OrderForm.tzOffset * 60 * 60 * 1000);
                        OrderForm.deadlineExtendChange();
                    }
                }
            }
        },

        deadlineExtendChange: function() {
            add_hours = parseInt($('#extend_hours').val());
            add_days = parseInt($('#extend_days').val());
            original = parseInt($('#original_deadline').val());
            real = new Date(Math.abs(add_hours * 60 * 60 * 1000) + Math.abs(add_days * 24 * 60 * 60 * 1000) + original + OrderForm.tzOffset * 60 * 60 * 1000);

            $('#deadline').val(real.format('Y-m-d H:i:s'));
        },

        validate: function(onValidate) {
            for (var i in OrderForm.beforeValidate) {
                OrderForm.beforeValidate[i].call(OrderForm, this);
            }
            if (OrderForm.validationImgBtn) {
                $('#submit_order_form').css('background', OrderForm.validationImgBtn);
            } else {
                OrderForm.previewName = $('#submit_order_form').val();
                if (onValidate == undefined) {
                    $('#submit_order_form').val(OrderForm.validationName);
                    $('#submit_order_form').addClass("button_wait_validation");
                }
            }

            if (!onValidate) {
                $('#submit_order_form').attr('disabled', 'disabled');
            }


            OrderForm.validateObject = {
                doctype: OrderForm.getDoctypeValue()
            };
            for (field in OrderForm.validateFields) {
                var fieldElm = $('#' + OrderForm.validateFields[field]);
                if (fieldElm.length) {

                    if (fieldElm.is(":checkbox")) {
                        if (fieldElm.attr("checked")) {
                            OrderForm.validateObject[OrderForm.validateFields[field]] = fieldElm.val();
                        }
                    } else {
                        OrderForm.validateObject[OrderForm.validateFields[field]] = fieldElm.val();
                    }
                }
            }

            for (field in OrderForm.validateArrayFields) {
                if (OrderForm.validateArrayFields[field] == 'preff_wr_id' && OrderForm.getSelectedHours() <= 48) continue;
                if ($('#' + OrderForm.validateArrayFields[field] + ' input').length) {
                    OrderForm.temp = new Array();
                    $('#' + OrderForm.validateArrayFields[field] + ' input').each(function() {
                        OrderForm.temp[OrderForm.temp.length] = this.value;
                    });
                    OrderForm.validateObject[OrderForm.validateArrayFields[field] + '[]'] = OrderForm.temp;
                }
            }

            if ($('#accept').length) {
                if ($('#accept input[name=accept]').length) {
                    OrderForm.validateObject['accept'] = $('#accept input[name=accept]:checked').val();
                } else {
                    if ($('#accept[type=checkbox]').length) {
                        OrderForm.validateObject['accept'] = $('#accept:checked').val();
                    } else {
                        OrderForm.validateObject['accept'] = $('#accept').val();
                    }
                }
            }

            if (onValidate == undefined) {
                onValidate = OrderForm.onValidate;
            }

            if (!OrderForm.isResumes || (OrderForm.isResumes && OrderForm.isResubmit)) {
                $.ajaxSetup({
                    async: false
                });
            }

            $.post(OrderForm.validateAction, OrderForm.validateObject, onValidate);
            if (OrderForm.isResumes && OrderForm.isResubmit) {
                $.ajaxSetup({
                    async: true
                });
            }

            if (OrderForm.sp_validation) {
                OrderForm.was_proceed = 1;
            }

            return false;
        },

        hideValidationErrors: function() {
            for (field in OrderForm.validateFields) {
                $('#error_' + OrderForm.validateFields[field]).hide();
                $row = $('#row_' + +OrderForm.validateFields[field]);
                if ($row.length == 0) $row = $('#' + OrderForm.validateFields[field]).parents('tr:first');
                $row.removeClass('validation-error');
                $('#' + OrderForm.validateFields[field]).removeClass('validation_error');
                $div = $('div.eot');
                if ($div.length) {
                    $('#' + OrderForm.validateFields[field]).css("background-color", "#FFFFFF");
                }

            }

            for (field in OrderForm.validateArrayFields) {
                $div = $('div.eot');
                if ($div.length) {
                    $('#' + OrderForm.validateFields[field]).css("background-color", "#FFFFFF");
                }
                if ($('#' + OrderForm.validateArrayFields[field] + ' input').length) {
                    $('#' + OrderForm.validateArrayFields[field] + ' input').each(function() {
                        $(this).removeClass('validation_error');
                    });

                }
                if ($('#' + OrderForm.validateArrayFields[field] + ' div.validation_error').length) {
                    $('#' + OrderForm.validateArrayFields[field] + ' div.validation_error').each(function() {
                        $(this).hide();
                    });
                }
            }
        },
        hideValidationFieldError: function(field) {
            field.removeClass('validation_error');
            field.parents('td').find('div.validation_error:visible').hide();
            OrderForm.hideValidationFieldState(field);
        },
        hideValidationFieldState: function(field) {
            field.parents('td').find('.state').removeClass('state_true');
            field.parents('td').find('.state').removeClass('state_false');
        },
        filterErors: function(data, field) {
            var errors = '{}';
            if (typeof data[field] != "undefined") {
                errors = '{"' + field + '" : ' + data[field] + '}';
            }
            return errors;
        },
        onValidateFieldRespond: function(data, field) {
            OrderForm.hideValidationFieldError(field);
            errors = eval('(' + data + ')');
            for (error in errors) {
                $('#error_' + error).show();
                $row = $('#row_' + error);
                $row.addClass('validation-error');
                $row = $('#row_' + error);
                $row.addClass('validation-error');
                $('#' + error).addClass('validation_error');
                $('#' + error).parents('td').find('.state').addClass('state_false');
                $('#' + error).parents('td').find('.state').removeClass('state_true');
            }
        },
        onValidateRespond: function(data) {
            OrderForm.hideValidationErrors();
            errors = eval('(' + data + ')');
            OrderForm.form_valid = 1;
            for (error in errors) {
                if (errors[error] != true) {
                    for (index in errors[error]) {
                        $div = $('#' + error + ' div.validation_error');
                        if ($div.length) {
                            $($div[index]).show();
                            $($('#' + error + ' input')[index]).addClass('validation_error');


                        } else {
                            $('#error_' + error).show();
                            $('#row_' + error).addClass('validation-error');
                            $('#' + error).addClass('validation_error');
                        }
                    }
                } else {
                    $div = $('div.eot');
                    if ($div.length) {
                        $('#' + error).css("background-color", "#ff9999");
                    }
                    $('#error_' + error).show();
                    $row = $('#row_' + error);
                    if ($row.length == 0) $row = $('#' + error).parents('tr:first');
                    $row.addClass('validation-error');
                    $('#' + error).addClass('validation_error');
                    $('#' + error).parents('td').find('.state').addClass('state_false');
                    $('#' + error).parents('td').find('.state').removeClass('state_true');
                    if (OrderForm.sp_validation) {
                        $('#' + error).addClass('error_field');
                        if ($('#' + error + '-button').length > 0) {
                            $('#' + error + '-button').addClass('error_field');
                        }
                        if (error == 'phone1') {
                            $('#phone1_area').addClass('error_field');
                            $('#phone1_number').addClass('error_field');
                        }
                    }
                    if (OrderForm.BD_validation) {
                        $('#' + error).addClass('error_field');
                        if ($('#' + error + '-button')) {
                            $('#' + error + '-button').addClass('error_field');
                        }
                        $('#' + error).parents('div:first').addClass('valid_error_mark');
                        if (error == 'phone1') {
                            $('#phone1_area').addClass('error_field');
                            $('#phone1_number').addClass('error_field');
                        }
                        if (error == 'phone2') {
                            $('#phone2_area').addClass('error_field');
                            $('#phone2_number').addClass('error_field');
                        }
                    }
                    if (OrderForm.BTP_validation) {
                        if (error == 'phone1_number') {
                            $('#error_phone1').show();
                        }
                    }
                }
                OrderForm.form_valid = 0;
            }
            for (ind in OrderForm.afterOnValidateRespond) {
                OrderForm.afterOnValidateRespond[ind].call(OrderForm, this, errors);
            }
        },
        onValidateField: function(field) {
            return function(data, textStatus) {
                if (field.val() != '' || field.is('select') || field.is('#phone1')) {
                    var errors = OrderForm.filterErors(eval('(' + data + ')'), field.attr('id'));
                    var obj = eval('(' + errors + ')');
                    size = 0;
                    for (i in obj) {
                        size++;
                    }
                    if (size == 0) {
                        field.removeClass('validation_error');
                        field.parents('td').find('div.validation_error:visible').hide();
                        field.parents('td').find('.state').addClass('state_true');
                        field.parents('td').find('.state').removeClass('state_false');
                        if ($('#retype_email').length > 0) {
                            if (field.attr('id') == 'email' && $('#retype_email').val().length > 0) {
                                OrderForm.validate(OrderForm.onValidateField($('#retype_email')));
                            }
                        }
                    } else {
                        OrderForm.onValidateFieldRespond(errors, field);
                    }
                }
                if (OrderForm.previewImgBtn) {
                    $('#submit_order_form').css('background', OrderForm.previewImgBtn);
                } else {
                    $('#submit_order_form').val(OrderForm.previewName);
                    $('#submit_order_form').removeClass("button_wait_validation");
                }
                $('#submit_order_form').removeAttr('disabled');
            };
        },
        onValidate: function(data) {
            if (OrderForm.onValidateDataPreprocessor) {
                data = OrderForm.onValidateDataPreprocessor(data);
            }
            OrderForm.onValidateRespond(data);
            if (OrderForm.form_valid) {
                OrderForm.submitValidatedForm();
            } else {
                if (OrderForm.isResumes) {
                    OrderForm.hideLoading();
                }
                if (OrderForm.previewImgBtn) {
                    $('#submit_order_form').css('background', OrderForm.previewImgBtn);
                } else {
                    $('#submit_order_form').val(OrderForm.previewName);
                    $('#submit_order_form').removeClass("button_wait_validation");
                }
                $('#submit_order_form').removeAttr('disabled');
                errors = eval('(' + data + ')');
                if ($('.validation_error:visible:first').length > 0) {
                    $t = $('.validation_error:visible:first');
                    while ($t[0].id == '') {
                        $t = $t.parent();
                    }
                    window.location.href = (OrderForm.switch_to_row_after_error ? '#row_' : '#') + $t[0].id;
                }
            }
        },

        setDiscountValue: function(value) {
            var discount = OrderForm.formatCurrency(value.discount, OrderForm.values.curr.value),
                discount_percent = Math.round(100 * value.discount / (value.cost_per_page * params.numpages * params.numpapers));

            if ($('#discount').attr('tagName') != 'INPUT') {
                $('#discount').html(discount);
            } else {
                $('#discount').val(discount);
            }

            if ($('#discount_percent').attr('tagName') != 'INPUT') {
                $('#discount_percent').html(discount_percent);
            } else {
                $('#discount_percent').val(discount_percent);
            }
        },

        showDiscount: function() {
            if (!OrderForm.isQuote) {
                $('#discount_span').show();
                $('#total_without_discount').show();
            }
        },

        hidePassword: function() {
            $('#row_password').hide();
            OrderForm.repaintTable();
        },

        showPassword: function() {
            $('#row_password').show();
            $('#password').val('');
            $('#row_password div.validation_error').hide();
            OrderForm.repaintTable();
        },

        hideDiscount: function() {
            $('#discount_span').hide();
            $('#total_without_discount').hide();
        },

        getSelectedHours: function() {
            if (OrderForm.hours[OrderForm.getDoctypeValue()]) {
                return OrderForm.hours[OrderForm.getDoctypeValue()][OrderForm.values.urgency.value];
            }
            return 0;
        },

        formatCurrency: function(value, currency) {
            result = currency + ' ' + value;
            if (OrderForm.currenciesFormat[currency]) {
                format = OrderForm.currenciesFormat[currency];
                result = format.replace('%s', value);
            }
            return result;
        },
        getSelectedParams: function() {
            var categoryElm = $('#order_category');
            interval = OrderForm.values.o_interval && OrderForm.values.o_interval.value ? OrderForm.values.o_interval.value : 0;

            if (OrderForm.values.curr && OrderForm.values.curr.value && OrderForm.currencyRates.USD[OrderForm.values.curr.value]) {
                multiplier = OrderForm.currencyRates.USD[OrderForm.values.curr.value];
            } else {
                multiplier = 1;
            }
            if (!OrderForm.values.order_category && categoryElm.length) {
                this.saveValue(categoryElm.get(0));
            }

            params = {
                doctype_id: OrderForm.doctype,
                urgency_id: OrderForm.values.urgency.value,
                wrlevel_id: (OrderForm.values.wrlevel ? OrderForm.values.wrlevel.value : 1),
                category_id: OrderForm.values.order_category ? OrderForm.values.order_category.value : (OrderForm.doctype == 182 ? 65 : 0),
                interval: interval,
                currencyRate: multiplier,
                numpages: Math.max(OrderForm.values.numpages && OrderForm.values.numpages.value ? OrderForm.values.numpages.value : 1, 1),
                numpapers: Math.max(OrderForm.values.numpapers && OrderForm.values.numpapers.value ? OrderForm.values.numpapers.value : 1, 1),
                cover_letters: Math.max(OrderForm.values.cover_letters && OrderForm.values.cover_letters.value ? OrderForm.values.cover_letters.value : 1, 1)
            };
            return params;
        },
        enableSubmit: function() {},
        TermConditionPopup: {
            isShow: false,
            needShow: function() {
                result = false;
                if (OrderForm.step != 3) {
                    $accept_input = $('[name=accept]:checked');
                    if (
                        $('#terms-conditions-popup').length > 0 && !(
                            $accept_input.val() == '1' ||
                            $accept_input.val() == 'accept' ||
                            OrderForm.isPreview ||
                            $('[name=accept]').length &&
                            $('[name=accept]:first').attr('type') == 'hidden'
                        )
                    ) {
                        result = true;
                    }
                }
                return result;
            },
            show: function() {
                $('#terms-conditions-popup').show();
                $('body').append('<div id="premium-dialog-overlay"></div>');
                if (-[1, ]);
                else { // if IE
                    $('#order_form').find('select').css('visibility', 'hidden');
                }
                OrderForm.TermConditionPopup.isShow = true;
            },

            hide: function() {
                $('#terms-conditions-popup').hide();
                $('#premium-dialog-overlay').remove();
                if (-[1, ]);
                else { // if IE
                    $('#order_form').find('select').css('visibility', 'visible');
                }

                // Show preview button
                if (OrderForm.previewImgBtn) {
                    $('#submit_order_form').css('background', OrderForm.previewImgBtn);
                } else {
                    $('#submit_order_form').val(OrderForm.previewName);
                    $('#submit_order_form').removeClass("button_wait_validation");
                }
                $('#submit_order_form').removeAttr('disabled');

                OrderForm.TermConditionPopup.isShow = false;
            },
            accept: function() {
                OrderForm.submitValidatedForm();
            }
        },
        submitValidatedForm: function() {
            if (!OrderForm.isResumes) {
                OrderForm.form_valid = 0;
            }
            if (!OrderForm.TermConditionPopup.isShow && OrderForm.TermConditionPopup.needShow()) {
                OrderForm.TermConditionPopup.show();
            } else {
                if (OrderForm.isPaypal) {
                    OrderForm.trackGA('payment');
                }

                $('#order_form').append('<input type="hidden" name="proceed" value="true" />');
                $('input[name=cancel]').attr('disabled', 'disabled');
                if (!OrderForm.ForceSubmit) {
                    $('input[name=preview]').removeAttr('disabled');
                }

                OrderForm.callSubmitFromSubmitValidatedForm = true;

                $('#order_form').submit();
                if ((OrderForm.isResumes || OrderForm.isTranslation) && !OrderForm.isResubmit) {
                    $('#back_order_form_step_1').attr('disabled', 'disabled')
                    $('#submit_order_form_step_2').attr('disabled', 'disabled')
                    $('#submit_order_form_step_1').click();
                }
                if (!OrderForm.isResumes && !OrderForm.isTranslation || OrderForm.isResubmit) {
                    $('#order_form').find('input[type=submit],input[type=image]').click();
                }
                if (OrderForm.isResumes) {
                    OrderForm.showLoading();
                }
            }
        },
        submitResumes: function() {
            var submit = $('#submit_order_form').val();
            var proceedButton = $('#' + submit);
            proceedButton.toggleClass('please-wait');
            switch (proceedButton.attr('id')) {
                case 'submit_order_form_step_2':
                    if (OrderForm.isResubmit) {
                        if (OrderForm.form_valid) {
                            $('input,textarea,select').removeAttr('disabled');
                            proceedButton.removeAttr('disabled');
                            return true;
                        } else {
                            OrderForm.validate();
                            OrderForm.trackGA();
                            OrderForm.showBlock(2);
                        }
                    } else {
                        if ($('#accept').attr('checked') == false) {
                            OrderForm.form_valid = false;
                            $("#error_accept").show();
                            OrderForm.hideLoading();
                            break;
                        } else {
                            $("#error_accept").hide();
                        }
                        OrderForm.changeURLStep(2);
                        OrderForm.trackGA();
                        OrderForm.showBlock(1);
                    }
                    $('.step_image').attr('src', OrderForm.secondStepImage);
                    $('#order_testimonials').hide();
                    $('#clr_block').hide();
                    $('#img_block1').hide();
                    $('#img_block2').hide();
                    $('#features_order_block1').hide();
                    $('#features_order_block3').hide();
                    OrderForm.hideLoading();
                    break;
                case 'submit_order_form_step_1':
                    if (OrderForm.form_valid) {
                        $('input:not(.disabled_email_input), textarea, select').removeAttr('disabled', 'disabled');
                        return true;
                    }
                    OrderForm.enableInputs();
                    OrderForm.validate();
                    break;
                case 'back_order_form_step_1':
                    OrderForm.changeURLStep(1);
                    $('#features_order_block1').show();
                    $('#features_order_block3').show();
                    OrderForm.showBlock(2);
                    OrderForm.hideLoading();
                    break;
            }
            proceedButton.toggleClass('please-wait');

        },
        submit: function() {
            if (OrderForm.callSubmitFromSubmitValidatedForm) {
                OrderForm.callSubmitFromSubmitValidatedForm = false;
                return true;
            }

            if (OrderForm.isResumes || OrderForm.isTranslation) {
                if (OrderForm.form_valid) {
                    return OrderForm.submitResumes();
                } else {
                    OrderForm.showLoading();
                    OrderForm.submitResumes();
                }
            } else {
                OrderForm.validate();

                if (OrderForm.form_valid) {
                    OrderForm.submitValidatedForm();
                }

                OrderForm.form_valid = 0;
            }
            return false;
        },

        showFeaturePrice: function(ind, price) {
            var $element = $('#additional_' + ind + '_price'),
                span_price = $element.find("span.vas_price");
            if (span_price.length) {
                span_price.html(price);
            } else if ($element.hasClass("pretty")) {
                var html = $(".holderWrap", $element).clone(true);
                if (html.length) {
                    $element.html(html);
                    $element.prepend(price);
                } else {
                    $element.html(price);
                }
            } else {
                $element.html(price);
            }

            for (a in OrderForm.afterShowFeaturePrice) {
                OrderForm.afterShowFeaturePrice[a].call(OrderForm, {
                    id: ind,
                    price: price,
                    element: $element
                });
            }
        },
        getVasCount: function(ind) {
            result = 1;
            if ((a = OrderForm.values['count_additional_' + ind])) {
                result = Math.max(result, a.value);
            }
            return result;
        },
        updateLinearSelects: function() {
            var $selects = $('#order_form select.linear');

            for (var i = 0; i < $selects.length; i++) {
                var container = OrderForm.linear_options.container.clone();

                if ($selects[i].id) {
                    container.attr('id', 'linear-select-' + $selects[i].id);
                }

                for (var j = 0; j < $selects[i].options.length; j++) {
                    var entry = OrderForm.linear_options.entry.clone();

                    if ((j + 1) == $selects[i].options.length) {
                        entry.addClass('linear-last-opt');
                    } else if (j == 0) {
                        entry.addClass('linear-first-opt');
                    }

                    if ($selects[i].options[j].selected == true) {
                        if ((j + 1) == $selects[i].options.length) {
                            entry.addClass('linear-last-opt-selected');
                        } else if (j == 0) {
                            entry.addClass('linear-first-opt-selected');
                        }

                        entry.addClass('selected');
                    }

                    entry.append('<input type="hidden" value="' + $selects[i].options[j].value + '" />');

                    if (OrderForm.linear_options.wrap_title && OrderForm.linear_options.wrap_title instanceof jQuery) {
                        var el = OrderForm.linear_options.wrap_title.clone();
                        entry.append(el.append($selects[i].options[j].text));
                    } else {
                        entry.append($selects[i].options[j].text);
                    }

                    if (OrderForm.linear_options.wrap_entry && OrderForm.linear_options.wrap_entry instanceof jQuery) {
                        var entry_wrap_el = OrderForm.linear_options.wrap_entry.clone();
                        container.append(entry_wrap_el.append(entry));
                    } else {
                        if (!OrderForm.linear_options.reverse) {
                            container.append(entry);
                        } else {
                            container.prepend(entry);
                        }
                    }

                    if (OrderForm.linearSelectDelimiter && j !== $selects[i].options.length - 1) {
                        container.append("&nbsp;" + OrderForm.linearSelectDelimiter + " &nbsp;");
                    }
                }

                $($selects[i]).hide().after(container);
            }

            $('.linear-select a').click(function() {
                if (!$(this).hasClass('selected')) {
                    var container = $(this).parent();

                    if (OrderForm.linear_options.wrap_entry) {
                        container = container.parent();
                    }

                    $select = container.parent().find('select');
                    $select.val($(this).find('input').val()).change();

                    container.find('.selected')
                        .removeClass('linear-last-opt-selected')
                        .removeClass('linear-first-opt-selected')
                        .removeClass('selected');

                    if ($(this).hasClass('linear-last-opt')) {
                        $(this).addClass('linear-last-opt-selected');
                    }

                    if ($(this).hasClass('linear-last-opt')) {
                        $(this).addClass('linear-first-opt-selected');
                    }

                    $(this).addClass('selected');
                }

                return false;
            });
        },
        updateCheckboxes: function() {
            $checkboxes = $('#order_form input[type=checkbox]:visible');
            for (i = 0; i < $checkboxes.length; i++) {
                controlId = '';
                if ($checkboxes[i].id) {
                    controlId = ' id="sweet-checkbox-' + $checkboxes[i].id + '"';
                }
                $class = $checkboxes[i].checked ? ' checked' : '';
                $control = $('<span class="sweet-checkbox" ' + controlId + ' />');
                $button = $('<a href="#" class="' + $class + '">&nbsp;</a>');
                $button.append('<input type="hidden" value="' + $checkboxes[i].value + '" />');
                $control.append($button);
                $($checkboxes[i]).hide().after($control);
            }

            $('#order_form .sweet-checkbox a').click(function() {
                if (!$(this).hasClass('checked')) {
                    $checkbox = $(this).parent().parent().find('input[type=checkbox]');
                    $checkbox.click();
                    $checkbox.attr('checked', 'checked');
                    $(this).addClass('checked');
                } else {
                    $checkbox = $(this).parent().parent().find('input[type=checkbox]');
                    $checkbox.click();
                    $checkbox.attr('checked', '');
                    $(this).removeClass('checked');
                }
                OrderForm.onInputChange.call($checkbox[0]);
                return false;
            });
        },
        hideZeroPriceDoctypes: function() {
            $('#doctype tr').show();
            _trs = $('#doctypes_group .doctype_radiolist tr').get();
            j = 0;
            for (i = 0; i < _trs.length; i++) {
                if ($(_trs[i]).find('td.price label').length && $(_trs[i]).find('td.price label').text().match(/\d+/)[0] == 0) {
                    $(_trs[i]).css('display', 'none');
                } else {

                    if ($(_trs[i]).find('input').length) {
                        $(_trs[i]).removeClass('even');
                        if (j % 2) {
                            $(_trs[i]).removeClass('odd');
                        } else {
                            $(_trs[i]).addClass('odd');
                        }
                    }
                    j++;
                }
            }
        },
        offeredFeature: function(inputObj) {
            if (OrderForm.isResubmit && !OrderForm.isPreview && OrderForm.adminAuthorized) {
                $(inputObj).attr('disabled', 'disabled');
                $('#' + $(inputObj).attr('id') + '_loading').show();

                var loc = location.href;
                loc = loc.substring(0, loc.indexOf('resubmit') + 8) + '.offeredfeature/' + loc.substring(loc.indexOf('resubmit') + 9, loc.indexOf('resubmit') + 17);
                doctype_id = parseInt($(inputObj).attr('id').replace('additional_offered_', ''));

                data = {
                    'doctype_id': doctype_id
                };
                $.ajax({
                    type: 'POST',
                    url: loc,
                    data: data,
                    dataType: 'json',
                    success: function(data) {
                        if (data.success != true) {
                            alert(data.message);
                        }
                        $('#' + $(inputObj).attr('id') + '_loading').hide();
                        $(inputObj).attr('id');
                    }
                });
            }
        },
        showLoading: function() {
            OrderForm.resizeFuzz();
            $('input, textarea, select').attr('disabled', 'disabled');
            $('#fuzz').css('display', 'block');
        },
        hideLoading: function() {
            OrderForm.enableInputs();
            $('#fuzz').css('display', 'none');
        },
        enableInputs: function() {
            $('input:not(.disabled_email_input), textarea, select').removeAttr('disabled', 'disabled');
        },
        resizeFuzz: function() {
            $('#fuzz').css("height", $(document).height());
            $('#fuzz').css("width", $(document).width());
        },
        changeURLStep: function(step) {
            var location_parts = location.href.split('#');
            if (location_parts.length && step && location_parts[1] != ('block' + step)) {
                location.href = location_parts[0] + '#block' + step;
            }
        },
        showBlock: function(index) {
            $('.formblock').hide();
            $('.top_text').toggle();
            $('#block' + index).show();

            if (OrderForm.isTranslation) {
                $('#step_number' + index).show();
                $('#submit_order_form_step_' + index).show();
            }

            $('html, body').animate({
                scrollTop: 0
            }, 'slow');
            if (index == 1) OrderForm.form_valid = false;
        },
        trackGA: function(page) {
            if (typeof page == "undefined") {
                page = 'order_step2';
            }
            try {
                _gaq.push(['_trackPageview', page]);
            } catch (err) {}
        }
    };

    var FormRules = {

        initialize: function() {
            FormRules.initTopicRules();
        },

        initTopicRules: function() {
            $('#topic').keyup(function() {
                if (this.value.length == 256) {
                    if (!FormRules.issetErrorMaxTopicSymbols()) {
                        $('#topic').parent().append('<div id="error_max_symbols" class="validation_error">Maximum 256 symbols allowed</div>');
                        $('#error_max_symbols').css('display', 'block');
                    }
                } else {
                    if (FormRules.issetErrorMaxTopicSymbols()) {
                        $('#error_max_symbols').remove();
                    }
                }
            });
        },
        issetErrorMaxTopicSymbols: function() {
            if ($('#error_max_symbols').length != 0) {
                return true;
            }

            return false;
        }
    };
    var OrderFormInit = function() {
        OrderForm.initialize();
        FormRules.initialize();

        $('#img_add_phone2').click(function() {
            $('#row_phone2').show();
            OrderForm.insertValInOtherObject($('#country'));
            $('#phone2_country').val($('#phone1_country').val());
            $('#img_add_phone2').hide();
            $('#row_phone1 .phone_hint').hide();
        });
        $('#img_delete_phone2').click(function() {
            $('#row_phone2').hide();
            $('#row_phone2').find('input:text').val('');
            $('#row_phone1 .phone_hint').show();
            $('#img_add_phone2').show();
        });

        $('#terms-conditions-popup-leave').click(function() {
            OrderForm.TermConditionPopup.hide();
            return false
        });
        $('#terms-conditions-popup-accept').click(function() {
            OrderForm.TermConditionPopup.accept();
            return false
        });
        $('.offered_feature').click(function() {
            OrderForm.offeredFeature(this);
            return false
        });
        if (OrderForm.switch_to_phone) {
            if (OrderForm.isResumes || OrderForm.isTranslation) {
                //phone area
                $('#phone1_area').focus(function() {
                    if ($(this).val() == 'area') {
                        $(this).removeClass('phone_default');
                        $(this).val('');
                    }
                    OrderForm.hideValidationFieldError(($('#phone1')));
                })
                $('#phone1_area').blur(function() {
                    if ($(this).val() == '') {
                        $(this).addClass('phone_default');
                        $(this).val('area');
                    } else {
                        if ($('#phone1_number').val() != 'number' && $('#phone1_number').val() != '') {
                            OrderForm.validate(OrderForm.onValidateField($('#phone1')));
                        }
                    }
                })
                //phone number
                $('#phone1_number').focus(function() {
                    if ($(this).val() == 'number') {
                        $(this).removeClass('phone_default');
                        $(this).val('');
                    }
                    OrderForm.hideValidationFieldError(($('#phone1')));
                })
                $('#phone1_number').blur(function() {
                    if ($(this).val() == '') {
                        $(this).addClass('phone_default');
                        $(this).val('number');
                    } else {
                        if ($('#phone1_area').val() != 'area') {
                            OrderForm.validate(OrderForm.onValidateField($('#phone1')));
                        }
                    }
                })
            }
            $('#phone1_area').keyup(function() {
                var input = $(this).val();
                if (input.length == $(this).attr('maxlength')) {
                    $('#phone1_area').trigger('change');
                    $('#phone1_number').focus();
                }
            });
            $('#phone2_area').keyup(function() {
                var input = $(this).val();
                if (input.length == $(this).attr('maxlength')) {
                    $('#phone2_area').trigger('change');
                    $('#phone2_number').focus();
                }
            });
            $('input[type=number]#phone1_number, input[type=number]#phone2_number').keypress(function() {
                var value = $(this).val();

                if (value.length >= $(this).attr('maxlength')) {
                    $(this).val(value.substr(0, $(this).attr('maxlength') - 1));
                }
            });
        }
    };
    OrderForm.languageStyle = {
        label: '',
        optionUS: '1',
        optionUK: 'I want a UK writer',
        optionUS_M: '2',
        optionUK_M: 'English (UK)',
        activeClass: 'langstyle-active',
        typeLink: false,

        init: function() {
            var uk10 = $('#additional_204');

            if (uk10.length > 0) {
                OrderForm.languageStyle.field = uk10;
                OrderForm.languageStyle.optionUK = 'I want a UK writer';
            } else {
                OrderForm.languageStyle.field = $('#additional_142');
            }
            if ($('#row_order_category td.main_column').length > 0) {
                $('#row_order_category td.main_column').append($('#langstyle_container')).append($('#row_langstyle td.foreign_column #phone_order_hint'));
                $('#row_langstyle').hide();
            } else {
                $('#row_langstyle').show();
            }
        },

        customizeStyleSelect: function() {
            if (OrderForm.isPreview) {
                return;
            }
            var usbutton, ukbutton;
            OrderForm.languageStyle.init();

            if ($('#langstyle').length && OrderForm.languageStyle.field.length && !$('#uk_writer').length && !$('#us_writer').length) {
                $('#lstyle_options').html('');
                $('#row_langstyle').children('td:first').html(OrderForm.languageStyle.label + $('#row_langstyle').children('td:first').html());
                if (OrderForm.languageStyle.field.attr('type') == 'checkbox') {
                    if (!OrderForm.languageStyle.typeLink) {
                        usbutton = '<input type="radio" name="lstyle" id="us_writer" ' + (OrderForm.languageStyle.field.attr('checked') ? '' : 'checked="checked"') + '><label for="us_writer">' + OrderForm.languageStyle.optionUS + '</label>';
                        ukbutton = '<input type="radio" name="lstyle" id="uk_writer" ' + (OrderForm.languageStyle.field.attr('checked') ? 'checked="checked"' : '') + ' /><label for="uk_writer">' + OrderForm.languageStyle.optionUK + '</label>';
                    } else {
                        usbutton = '<a href="#" id="us_writer" ' + (OrderForm.languageStyle.field.attr('checked') ? '' : 'class="' + OrderForm.languageStyle.activeClass + '"') + '> ' + OrderForm.languageStyle.optionUS_M + ' </a>';
                        ukbutton = '<a href="#" id="uk_writer" ' + (OrderForm.languageStyle.field.attr('checked') ? 'class="' + OrderForm.languageStyle.activeClass + '"' : '') + '> ' + OrderForm.languageStyle.optionUK_M + ' </a>';
                    }

                    radio_html = '<div id="lstyle_options"><span class="lstyle_option">' + usbutton + '</span><input type="checkbox" value="" name="" id="uk_us_switcher"  ' + (OrderForm.languageStyle.field.attr('checked') ? 'checked="checked"' : '') + '/><span>' + OrderForm.languageStyle.optionUK + '</span>';
                    radio_html_m = '<div id="lstyle_options"><span class="lstyle_option">' + usbutton + '</span><input type="checkbox" value="" name="" id="uk_us_switcher" ' + (OrderForm.languageStyle.field.attr('checked') ? 'checked="checked"' : '') + '/><span>' + OrderForm.languageStyle.optionUK + '</span>';
                    if (OrderForm.languageStyle.typeLink) {
                        radio_html += '&nbsp;/&nbsp';
                        radio_html_m += '&nbsp;/&nbsp';
                    }
                    radio_html += '<span class="lstyle_option">' + ukbutton + '</span></div>';
                    if (!OrderForm.languageStyle.typeLink) {
                        radio_html_m += '<br />';
                    }
                    radio_html_m += '<span class="lstyle_option">' + ukbutton + '</span></div>';
                    if (OrderForm.languageStyle.typeLink) {
                        radio_html_m += '<br/>(+10% to the order total for UK writer)';
                    }
                }

                OrderForm.languageStyle.stepEdit();
            }
        },
        setLabel: function(label_name) {
            if (label_name) {
                OrderForm.languageStyle.label = label_name;
            }
        },
        stepEdit: function() {
            if (OrderForm.languageStyle.field.attr('checked')) {
                $('#langstyle').children('[value=2]').attr('selected', true);
            } else if ($('#langstyle').children('[value=2]').attr('selected')) {
                $('#langstyle').children('[value=2]').attr('selected', true);
            } else {
                $('#langstyle').children('[value=1]').attr('selected', true);
            }
            if (!$('#row_langstyle').length) {
                $('#langstyle_container').html(radio_html_m);
            } else {
                $('#langstyle_container').html(radio_html);
            }

            OrderForm.languageStyle.bindEventsEdit();
        },

        bindEventsEdit: function() {
            $('#us_writer').click(function() {
                $(this).addClass(OrderForm.languageStyle.activeClass);
                $('#uk_writer').removeClass(OrderForm.languageStyle.activeClass);
                $('#langstyle').children('[value=1]').attr('selected', true);
                OrderForm.languageStyle.field.attr('checked', '').change();
                if (OrderForm.languageStyle.typeLink) {
                    return false;
                }
            })

            $('#uk_writer').click(function() {
                $(this).addClass(OrderForm.languageStyle.activeClass);
                $('#us_writer').removeClass(OrderForm.languageStyle.activeClass);
                $('#langstyle').children('[value=2]').attr('selected', true);
                OrderForm.languageStyle.field.attr('checked', 'checked').change();
                if (OrderForm.languageStyle.typeLink) {
                    return false;
                }
            });

            $('#uk_us_switcher').click(function() {
                if ($(this).attr('checked')) {
                    $('#us_writer').click();
                } else {
                    $('#uk_writer').click();
                }
            });
        }
    };
    OrderForm.languageStyle.setLabel('');
    OrderForm.sp_validation = 1;
    OrderForm.SP_NewDesign = {
        init: function() {
            me = OrderForm.SP_NewDesign;
            $('#row_additional_142').show();
            if (!OrderForm.isPreview) {
                me.pickOutSteps();
                $('select#doctype, select#urgency, select#numberOfSources, select#style, select#country')
                    .selectmenu({
                        maxHeight: 200,
                        style: 'dropdown'
                    });
            }
            $('#numpages')
                .selectmenu({
                    maxHeight: 200,
                    style: 'dropdown',
                    blur: function() {
                        OrderForm.SP_NewDesign.blurSelectMenuEl()
                    }
                });
            $('#order_category')
                .selectmenu({
                    maxHeight: 200,
                    style: 'dropdown',
                    blur: function() {
                        OrderForm.SP_NewDesign.blurSelectMenuEl2()
                    }
                });
        },
        blurSelectMenuEl: function() {
            checkSomeRequiredFields(8);
        },
        blurSelectMenuEl2: function() {
            checkSomeRequiredFields(9);
        },
        changePages: function(elObj) {
            if (elObj.id == 'o_interval' || elObj.id == 'urgency' || elObj.id == 'wrlevel') {
                if ($('#numpages-menu').length > 0) {
                    $('#numpages').selectmenu('destroy');
                }
                $('#numpages').selectmenu({
                    maxHeight: 200,
                    style: 'dropdown'
                });
            }
        },
        pickOutSteps: function() {
            $('#personal_info input, #personal_info select, #personal_info a, #personal_info span').focus(function() {
                $('.order_title')
                    .removeClass('order_title_1_active')
                    .removeClass('order_title_2_active')
                    .removeClass('order_title_3_active');
                $('.order_title_1').addClass('order_title_1_active');
            });
            $('#order_details input, #order_details select, #order_details textarea, #order_details a, #order_details span').focus(function() {
                $('.order_title')
                    .removeClass('order_title_1_active')
                    .removeClass('order_title_2_active')
                    .removeClass('order_title_3_active');
                if ($('.order_title_2').hasClass('order_title_2_customer')) {
                    $('.order_title_2').addClass('order_title_1_active');
                } else {
                    $('.order_title_2').addClass('order_title_2_active');
                }
            });
            $('.feature_tr input, .feature_tr select, .feature_tr textarea, .feature_tr a, .feature_tr span').focus(function() {
                $('.order_title')
                    .removeClass('order_title_1_active')
                    .removeClass('order_title_2_active')
                    .removeClass('order_title_3_active');
                if ($('.order_title_3').hasClass('order_title_3_customer')) {
                    $('.order_title_3').addClass('order_title_2_active');
                } else {
                    $('.order_title_3').addClass('order_title_3_active');
                }

            });
        }
    };

    function AjaxCheckEl(el, display_error) {
        if ($("#email").length > 0) {
            if ($('#email').val() == '') {
                display_error && $('#email').addClass('error_field');
                $('#email_ok').remove();
                return false;
            }

            var data_fields = new Object();

            if ($('#' + el)) {
                eval("data_fields." + el + "='" + $('#' + el).val() + "'");
            }

            $.ajax({
                type: 'POST',
                url: '/order.validate/',
                data: data_fields,
                success: function(data) {
                    var answer = eval('(' + data + ')');
                    if (answer[el]) {
                        switch (el) {
                            case 'email':
                                display_error && $('#email').addClass('error_field');
                                $('#email' + '_ok').remove();
                                break;
                        }
                    } else {
                        switch (el) {
                            case 'email':
                                $('#email').removeClass('error_field');
                                if ($('#email' + '_ok').attr('class') == undefined) {
                                    //$('#row_email td:last-child').append('<div id="email_ok" class="ok_field"></div>');
                                }
                                checkRetypeEmail(display_error);
                                break;
                        }
                    }
                }
            });
        }
    }

    function checkFirstName(display_error) {
        if ($('#firstname').length > 0) {
            if ($('#firstname').val() == '' || $('#firstname').val() == 'First name') {
                display_error && $('#firstname').addClass('error_field');
                $('#firstname_ok').remove();
            } else if ($('#firstname').val().length > 0) {
                $('#firstname').removeClass('error_field');
            }
            if ($('#lastname').val() != '' && $('#lastname').val() != 'Last name' && $('#firstname').val() != '' && $('#firstname').val() != 'First name') {
                if ($('#firstname_ok').attr('class') == undefined) {
                    //$('#row_firstname td:last-child').append('<div id="firstname_ok" class="ok_field"></div>');
                }
            }
        }
    }

    function checkLastName(display_error) {
        if ($('#lastname').length > 0) {
            if ($('#lastname').val() == '' || $('#lastname').val() == 'Last name') {
                display_error && $('#lastname').addClass('error_field');
                $('#firstname_ok').remove();
            } else if ($('#firstname').val() != '' && $('#firstname').val() != 'First name') {
                $('#lastname').removeClass('error_field');
                if ($('#firstname_ok').attr('class') == undefined) {
                    //$('#row_firstname td:last-child').append('<div id="firstname_ok" class="ok_field"></div>');
                }
            }
        }
    }

    function checkRetypeEmail(display_error) {
        if ($('#retype_email').length > 0) {
            if ($('#retype_email').val() == '' || $('#retype_email').val() != $('#email').val()) {
                display_error && $('#retype_email').addClass('error_field');
                $('#retype_email' + '_ok').remove();
            } else if ($('#retype_email').val() != '') {
                $('#retype_email').removeClass('error_field');
                if ($('#retype_email' + '_ok').length == 0 && $('#email_ok').length) {
                    //$('#row_retype_email td:last-child').append('<div id="retype_email_ok" class="ok_field"></div>');
                }
            }
        }
    }

    function checkCountry(display_error) {
        if ($('#country').val() == '') {
            display_error && $('#country-button').addClass('error_field');
            $('#phone1_number' + '_ok').remove();
        } else {
            $('#country-button').removeClass('error_field');
        }
    }

    function checkPhone1(display_error) {
        if ($('#phone1_number').length > 0) {
            var intRegex = /^\d+$/;
            var y = $('#phone1_number').val();
            y = y.replace(/\s/g, '');
            var x = $('#phone1_area').val();
            x = x.replace(/\s/g, '');
            if ($('#phone1_number').val() == '' || $('#phone1_number').val() == 'number' || !intRegex.test(y)) {
                display_error && $('#phone1_number').addClass('error_field');
                $('#phone1_number_ok').remove();
            } else {
                $('#phone1_number').removeClass('error_field');
            }

            if ($('#phone1_area').val() == '' || $('#phone1_area').val() == 'area' || !intRegex.test(x)) {
                display_error && $('#phone1_area').addClass('error_field');
                $('#phone1_number' + '_ok').remove();
            } else {
                $('#phone1_area').removeClass('error_field');
            }
            if ($('#country').val() == '') {
                display_error && $('#country-button').addClass('error_field');
                $('#phone1_number' + '_ok').remove();
            } else {
                $('#country-button').removeClass('error_field');
            }
            if ($('#phone1_number').val() != '' && $('#phone1_number').val() != 'number' && $('#country').val() != '' && $('#phone1_area').val() != '' && $('#phone1_area').val() != 'area' && $('#phone1_number' + '_ok').attr('class') == undefined && intRegex.test(y) && intRegex.test(x)) {
                //$('#row_phone1 td:nth-child(4)').append('<div id="phone1_number_ok" class="ok_field"></div>');
            }
        }
    }

    function checkPhone1Area(display_error) {
        var intRegex = /^\d+$/;
        var y = $('#phone1_area').val();
        y = y.replace(/\s/g, '');
        if ($('#phone1_area').val() == '' || $('#phone1_area').val() == 'area' || !intRegex.test(y)) {
            display_error && ('#phone1_area').addClass('error_field');
            $('#phone1_number' + '_ok').remove();
        } else {
            $('#phone1_area').removeClass('error_field');
        }
    }

    function checkPhone2Area() {
        if ($('#phone2').length > 0) {
            if ($('#phone2_number_ok').length != 0) {
                return false;
            }
            var stripped = $('#phone2').val().replace(/[\(\)\.\-\ ]/g, '');
            if (stripped.value != "" && parseInt(stripped) && stripped.length > 6) {
                //$('#row_phone2 td:nth-child(3)').append('<div id="phone2_number_ok" class="ok_field"></div>');
            } else {
                $('#phone2_number_ok').remove();
            }
        }
    }

    function checkTopic(display_error) {
        if ($('#topic').val() == '') {
            display_error && $('#topic').addClass('error_field');
            $('#topic' + '_ok').remove();
        } else {
            $('#topic').removeClass('error_field');
            if ($('#topic' + '_ok').attr('class') == undefined) {
                //$('#row_topic td:last-child').append('<div id="topic_ok" class="ok_field"></div>');
            }
        }
    }

    function checkDetails(display_error) {
        if ($('#details').val() == '') {
            display_error && $('#details').addClass('error_field');
            $('#details' + '_ok').remove();
        } else {
            $('#details').removeClass('error_field');
            if ($('#details' + '_ok').attr('class') == undefined) {
                num = ($('#row_details td:nth-child(2) > div').length == 1) ? 1 : 2;
                //$('#row_details td:nth-child(2) > div:nth-child(' + num + ')').after('<div id="details_ok" class="ok_field"></div>');
            }
        }
    }

    function checkNumpages(display_error) {
        if ($('#numpages').val() == '' || $('#numpages').val() == 'select' || $('#numpages').val() == '0') {
            display_error && $('#numpages' + ' ~a').addClass('error_field');
            $('#numpages' + '_ok').remove();
        } else if ($('#numpages').val() != '') {
            $('#numpages' + ' ~a').removeClass('error_field');
            if ($('#numpages' + '_ok').attr('class') == undefined) {
                num = ($('#row_numpages td:nth-child(2) > div').length == 1) ? 1 : 2;
                //$('#row_numpages td:nth-child(2) > div:nth-child(' + num + ')').after('<div id="numpages_ok" class="ok_field"></div>');
            }
        }
    }



    function checkOrderCategory(n, display_error) {
        if ($('#order_category').val() == '' || $('#order_category').val() == 'select' || $('#order_category').val() == '0') {
            display_error && $('#order_category' + ' ~a').addClass('error_field');
            $('#order_category' + '_ok').remove();
        } else if ($('#order_category').val() != '') {
            $('#order_category' + ' ~a').removeClass('error_field');
            if ($('#order_category' + '_ok').attr('class') == undefined) {
                //$('#row_order_category td:nth-child(2) div:nth-child('+n+')').after('<div id="order_category_ok" class="ok_field"></div>');
            }
        }

    }

    function checkSelects(id, display_error) {
        if ($('#' + id).length > 0 && id != 'numpages') {
            if (id != 'doctype' && ($('#' + id).val() == '' || $('#' + id).val() == 'select' || $('#' + id).val() == '0')) {
                display_error && $('#' + id + ' ~a').addClass('error_field');
                $('#' + id + '_ok').remove();
            } else {
                $('#' + id + ' ~a').removeClass('error_field');
                if ($('#' + id + '_ok').attr('class') == undefined) {
                    //$('#row_'+id+' td:nth-child(2)').append('<div id="'+id+'_ok" class="ok_field"></div>');
                }
            }
        }
    }

    function checkSomeSelects(display_error) {
        checkSelects('numberOfSources', display_error);
        checkSelects('urgency', display_error);
        checkSelects('academic_level', display_error);
        checkSelects('doctype', display_error);
        checkNumpages(false);
        checkPhone2Area();
    }

    function checkAllRequiredFields() {
        checkFirstName();
        checkLastName();
        checkPhone1();
        AjaxCheckEl('email');
        checkRetypeEmail();
        checkNumpages();
        checkOrderCategory(2);
        checkSelects();
        checkTopic();
        checkDetails();
        return false;
    }

    function ckeckOnSwitchDoctype(check_on_load) {
        checkTopic(check_on_load);
        checkDetails(check_on_load);
        checkOrderCategory(1, check_on_load);
        checkOrderCategory(2, check_on_load);
        checkNumpages(check_on_load);

        $('#topic').keyup(function() {
            checkTopic(false)
        });
        $('#details').keyup(function() {
            checkDetails(false)
        });
        $('#order_category').change(function() {
            checkOrderCategory(1, false);
            checkOrderCategory(2, false)
        });
        $('#numpages').change(function() {
            checkNumpages(false)
        });
        $('#country').change(function() {
            checkCountry(false)
        });
    }

    function checkSomeRequiredFields(n, display_error) {
        switch (n) {
            case 2:
                checkFirstName(display_error);
                checkLastName(display_error);
                break;
            case 3:
                checkFirstName(display_error);
                checkLastName(display_error);
                break;
            case 4:
                checkFirstName(display_error);
                checkLastName(display_error);
                checkRetypeEmail(display_error)
                break;
            case 5:
                checkFirstName(display_error);
                checkLastName(display_error);
                checkPhone1(display_error);
                checkRetypeEmail(display_error);
                break;
            case 6:
                checkFirstName(display_error);
                checkLastName(display_error);
                checkPhone1(display_error);
                checkRetypeEmail(display_error);
                checkTopic(display_error);
                break;
            case 7:
                checkFirstName(display_error);
                checkLastName(display_error);
                checkPhone1(display_error);
                checkRetypeEmail(display_error);
                checkTopic(display_error);
                checkDetails(display_error);
                break;
            case 8:
                checkFirstName(display_error);
                checkLastName(display_error);
                checkPhone1(display_error);
                checkRetypeEmail(display_error);
                checkNumpages(display_error);
                checkTopic(display_error);
                checkDetails(display_error);
                break;
            case 9:
                checkFirstName(display_error);
                checkLastName(display_error);
                checkPhone1(display_error);
                checkRetypeEmail(display_error);
                checkNumpages(display_error);
                checkTopic(display_error);
                checkDetails(display_error);
                checkOrderCategory(2, display_error);
                break;
            case 10:
                checkFirstName(display_error);
                checkLastName(display_error);
                checkPhone1(display_error);
                checkPhone2Area();
                AjaxCheckEl('email', display_error);
                checkRetypeEmail(display_error);
                checkNumpages(display_error);
                checkTopic(display_error);
                checkDetails(display_error);
                checkOrderCategory(1, display_error);
                break;
        }

        return false;
    }

    OrderForm.afterSwitchForms.push(OrderForm.languageStyle.customizeStyleSelect);

    //! ninja essays cfg
    OrderForm.doctype = 0;
    OrderForm.hours = [{
        "1": 168,
        "3": 120,
        "4": 96,
        "5": 72,
        "6": 48,
        "7": 24,
        "8": 12,
        "9": 6,
        "10": 3,
        "11": 240
    }];
    OrderForm.prices = [{
        "1": {
            "1": [20.99],
            "3": [21.99],
            "4": [22.99],
            "5": [24.99],
            "6": [32.99],
            "7": [34.99],
            "8": [36.99],
            "9": [38.99],
            "10": [41.99],
            "11": [19.99]
        },
        "2": {
            "1": [22.99],
            "3": [23.99],
            "4": [24.99],
            "5": [26.99],
            "6": [34.99],
            "7": [36.99],
            "8": [38.99],
            "9": [40.99],
            "10": [43.99],
            "11": [21.99]
        },
        "17": {
            "1": [11.99],
            "3": [13.99],
            "4": [15.99],
            "5": [17.99],
            "6": [22.99],
            "7": [25.99],
            "8": [27.99],
            "9": [29.99],
            "10": [32.99],
            "11": [10.99]
        },
        "18": {
            "1": [13.99],
            "3": [15.99],
            "4": [17.99],
            "5": [18.99],
            "6": [24.99],
            "7": [27.99],
            "8": [30.99],
            "9": [32.99],
            "10": [35.99],
            "11": [12.99]
        },
        "19": {
            "1": [14.99],
            "3": [16.99],
            "4": [18.99],
            "5": [20.99],
            "6": [25.99],
            "7": [28.99],
            "8": [31.99],
            "9": [33.99],
            "10": [36.99],
            "11": [13.99]
        },
        "20": {
            "1": [15.99],
            "3": [17.99],
            "4": [19.99],
            "5": [21.99],
            "6": [26.99],
            "7": [29.99],
            "8": [32.99],
            "9": [34.99],
            "10": [37.99],
            "11": [14.99]
        },
        "21": {
            "1": [16.99],
            "3": [18.99],
            "4": [20.99],
            "5": [22.99],
            "6": [27.99],
            "7": [30.99],
            "8": [33.99],
            "9": [35.99],
            "10": [38.99],
            "11": [15.99]
        },
        "22": {
            "1": [17.99],
            "3": [19.99],
            "4": [21.99],
            "5": [23.99],
            "6": [28.99],
            "7": [31.99],
            "8": [34.99],
            "9": [36.99],
            "10": [39.99],
            "11": [16.99]
        },
        "27": {
            "1": [24.99],
            "3": [25.99],
            "4": [27.99],
            "5": [30.99],
            "6": [36.99],
            "7": [40.99],
            "8": [44.99],
            "9": [47.99],
            "10": [52.99],
            "11": [23.99]
        }
    }];
    OrderForm.free_vas = [];
    OrderForm.admin_free_vas = [];
    OrderForm.fieldDoctypes = {
        "10": null,
        "11": null,
        "13": null,
        "50": null,
        "34": null,
        "12": null,
        "88": null,
        "15": null,
        "69": null,
        "151": "237",
        "153": "185",
        "152": "187",
        "27": null,
        "143": null,
        "135": null,
        "18": null,
        "19": null,
        "20": null,
        "142": "224",
        "130": null,
        "21": null,
        "86": null,
        "87": null,
        "8": null,
        "9": null
    };
    OrderForm.limits = [{
        "1": {
            "5": "40",
            "6": "19",
            "7": "12",
            "8": "8",
            "9": "4",
            "10": "3"
        },
        "2": {
            "5": "40",
            "6": "19",
            "7": "12",
            "8": "8",
            "9": "4",
            "10": "3"
        },
        "27": {
            "5": "40",
            "6": "19",
            "7": "12",
            "8": "8",
            "9": "4",
            "10": "3"
        }
    }];
    OrderForm.price_groups = [
        []
    ];
    OrderForm.currencyRates = {
        "AUD": {
            "AUD": 1,
            "CAD": 1.003,
            "EUR": 0.6783,
            "GBP": 0.551,
            "USD": 0.9239
        },
        "CAD": {
            "AUD": 0.997,
            "CAD": 1,
            "EUR": 0.6763,
            "GBP": 0.5494,
            "USD": 0.9211
        },
        "EUR": {
            "AUD": 1.4743,
            "CAD": 1.4786,
            "EUR": 1,
            "GBP": 0.8124,
            "USD": 1.362
        },
        "GBP": {
            "AUD": 1.8149,
            "CAD": 1.8202,
            "EUR": 1.2309,
            "GBP": 1,
            "USD": 1.6767
        },
        "USD": {
            "AUD": 1.0824,
            "CAD": 1.0857,
            "EUR": 0.7342,
            "GBP": 0.5964,
            "USD": 1
        }
    };

    /******************************************************************/
    OrderForm.discountCodeCoefficient = 0;
    OrderForm.discountCodeType = 0;
    OrderForm.showOriginalPrice = 0;
    /******************************************************************/

    OrderForm.secondStepImage = "";
    OrderForm.validateAction = '/order.validate';
    OrderForm.isPreview = false;
    OrderForm.isResubmit = false;
    OrderForm.isEdit = 0;
    OrderForm.isCalculator = false;
    OrderForm.orderCode = '';
    OrderForm.isQuote = false;
    OrderForm.isResumes = false;
    OrderForm.isPaypal = false;
    OrderForm.nonWordsProducts = {
        "124": 124,
        "125": 125,
        "126": 126,
        "182": 182,
        "139": 139,
        "222": 222,
        "51": 51,
        "234": 234,
        "235": 235,
        "260": 260,
        "261": 261,
        "262": 262
    };
    OrderForm.featurePrices = new Array();
    OrderForm.featureDiscountable = new Array();
    OrderForm.coverLetterId = 50;
    OrderForm.withCoverLetterIds = {
        "34": 34,
        "50": 50,
        "133": 133,
        "137": 137,
        "138": 138
    };
    OrderForm.values.o_interval = {
        value: '0'
    };
    OrderForm.featurePrices[147] = function() {

        condition_free = [];
        if (OrderForm.values.doctype) {
            doctype_id = OrderForm.values.doctype.value;
        } else {
            doctype_id = OrderForm.doctype;
        }
        urgency_id = OrderForm.values.urgency.value;
        wrlevel_id = (OrderForm.values.wrlevel ? OrderForm.values.wrlevel.value : 0);
        free_from_link = false;

        if ((condition_free && condition_free[doctype_id] && condition_free[doctype_id][urgency_id] && condition_free[doctype_id][urgency_id][wrlevel_id]) || free_from_link) {
            return 0;
        } else {
            return 9.99;
        }
    };
    OrderForm.featureDiscountable[147] = false;
    OrderForm.featurePrices[151] = function() {
        feature_fields = {
            "0": ["152", "153"],
            "1": ["152", "153"],
            "3": ["152", "153"],
            "13": ["152", "153"],
            "14": ["152", "153"],
            "15": ["152", "153"],
            "37": ["152", "153"],
            "38": ["152", "153"],
            "39": ["152", "153"],
            "40": ["152", "153"],
            "51": ["152", "153"],
            "80": ["152", "153"],
            "81": ["152", "153"],
            "82": ["147", "153"],
            "83": ["152", "153"],
            "84": ["152", "153"],
            "85": ["152", "153"],
            "124": ["147", "153"],
            "125": ["152", "153"],
            "126": ["152", "153"],
            "134": ["152", "153"],
            "135": ["152", "153"],
            "136": ["152", "153"],
            "142": ["152", "153"],
            "143": ["152", "153"],
            "144": ["152", "153"],
            "145": ["152", "153"],
            "146": ["152", "153"],
            "147": ["152", "153"],
            "148": ["152", "153"],
            "149": ["152", "153"],
            "150": ["152", "153"],
            "151": ["152", "153"],
            "152": ["152", "153"],
            "153": ["152", "153"],
            "154": ["152", "153"],
            "156": ["152", "153"],
            "157": ["152", "153"],
            "158": ["147", "153"],
            "159": ["147", "153"],
            "163": ["152", "153"],
            "168": ["152", "153"],
            "169": ["152", "153"],
            "170": ["152", "153"],
            "171": ["152", "153"],
            "172": ["152", "153"],
            "173": ["152", "153"],
            "174": ["152", "153"],
            "235": ["152", "153"],
            "242": ["152", "153"]
        };
        fields = feature_fields[OrderForm.doctype];
        result = 0;
        free_from_link = false;

        if (free_from_link) {
            return 0;
        }

        for (a in fields) {
            result += OrderForm.featurePrices[fields[a]](fields[a], 1);
        }
        return result * 0.6;
    };
    OrderForm.featureDiscountable[151] = false;
    OrderForm.featurePrices[153] = function() {
        condition_free = [];
        doctype_id = OrderForm.values.doctype.value;
        urgency_id = OrderForm.values.urgency.value;
        wrlevel_id = (OrderForm.values.wrlevel ? OrderForm.values.wrlevel.value : 0);
        currency_rate = OrderForm.currencyRates.USD[OrderForm.values.curr.value];
        free_from_link = false;
        free_over_order_total = parseFloat('0');
        if ((free_over_order_total > 0) && OrderForm.total_without_discount && OrderForm.total_without_discount > (free_over_order_total * currency_rate)) {
            return 0;
        }

        if ((condition_free && condition_free[doctype_id] && condition_free[doctype_id][urgency_id] && condition_free[doctype_id][urgency_id][wrlevel_id]) || free_from_link) {
            return 0;
        } else {
            return Math.max(1, parseInt($('#numpages').val())) * 5.99;
        }
    };
    OrderForm.featureDiscountable[153] = false;
    OrderForm.featurePrices[152] = function() {
        condition_free = [];
        doctype_id = OrderForm.values.doctype.value;
        urgency_id = OrderForm.values.urgency.value;
        wrlevel_id = (OrderForm.values.wrlevel ? OrderForm.values.wrlevel.value : 0);
        free_from_link = false;

        if ((condition_free && condition_free[doctype_id] && condition_free[doctype_id][urgency_id] && condition_free[doctype_id][urgency_id][wrlevel_id]) || free_from_link) {
            return 0;
        } else {
            currency_rate = OrderForm.currencyRates[OrderForm.values.curr.value].USD;
            return OrderForm.cost_per_page *
                Math.max(OrderForm.values.numpages && OrderForm.values.numpages.value ? OrderForm.values.numpages.value : 1, 1) *
                Math.max(OrderForm.values.numpapers && OrderForm.values.numpapers.value ? OrderForm.values.numpapers.value : 1, 1) *
                currency_rate * 25 / 100;
        }
    };
    OrderForm.featureDiscountable[152] = false;
    OrderForm.featurePrices[142] = function() {
        free_from_link = false;
        if (free_from_link) {
            return 0;
        }
        currency_rate = OrderForm.currencyRates[OrderForm.values.curr.value].USD;
        return OrderForm.cost_per_page *
            Math.max(OrderForm.values.numpages && OrderForm.values.numpages.value ? OrderForm.values.numpages.value : 1, 1) *
            Math.max(OrderForm.values.numpapers && OrderForm.values.numpapers.value ? OrderForm.values.numpapers.value : 1, 1) *
            currency_rate * 5 / 100;
    };
    OrderForm.featureDiscountable[142] = false;
    OrderForm.currenciesFormat = {
        "USD": "$%s",
        "EUR": "&euro;%s",
        "GBP": "&pound;%s",
        "AUD": "A$ %s"
    };
    OrderForm.cppDiscountRules = {
        "15": 5,
        "51": 10,
        "101": 15
    };


    OrderForm.nonTechDoctypes = [125, 126, 152, 163, 174, 182, 217, 222];
    OrderForm.techCategories = [65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77];
    OrderForm.version1 = true;
    OrderForm.version1_1 = false;
    OrderForm.country = [{
        "c_id": "93",
        "name": "Afghanistan",
        "id_country": "4"
    }, {
        "c_id": "355",
        "name": "Albania",
        "id_country": "5"
    }, {
        "c_id": "213",
        "name": "Algeria",
        "id_country": "6"
    }, {
        "c_id": "684",
        "name": "American Samoa",
        "id_country": "7"
    }, {
        "c_id": "376",
        "name": "Andorra",
        "id_country": "8"
    }, {
        "c_id": "244",
        "name": "Angola",
        "id_country": "9"
    }, {
        "c_id": "264*",
        "name": "Anguilla",
        "id_country": "10"
    }, {
        "c_id": "672",
        "name": "Antarctica",
        "id_country": "11"
    }, {
        "c_id": "268*",
        "name": "Antigua",
        "id_country": "12"
    }, {
        "c_id": "54",
        "name": "Argentina",
        "id_country": "13"
    }, {
        "c_id": "374",
        "name": "Armenia",
        "id_country": "14"
    }, {
        "c_id": "297",
        "name": "Aruba",
        "id_country": "15"
    }, {
        "c_id": "247",
        "name": "Ascension Island",
        "id_country": "16"
    }, {
        "c_id": "61",
        "name": "Australia",
        "id_country": "17"
    }, {
        "c_id": "43",
        "name": "Austria",
        "id_country": "18"
    }, {
        "c_id": "994",
        "name": "Azberbaijan",
        "id_country": "19"
    }, {
        "c_id": "242*",
        "name": "Bahamas",
        "id_country": "20"
    }, {
        "c_id": "973",
        "name": "Bahrain",
        "id_country": "21"
    }, {
        "c_id": "880",
        "name": "Bangladesh",
        "id_country": "22"
    }, {
        "c_id": "246*",
        "name": "Barbados",
        "id_country": "23"
    }, {
        "c_id": "268*",
        "name": "Barbuda",
        "id_country": "24"
    }, {
        "c_id": "375",
        "name": "Belarus",
        "id_country": "25"
    }, {
        "c_id": "32",
        "name": "Belgium",
        "id_country": "26"
    }, {
        "c_id": "501",
        "name": "Belize",
        "id_country": "27"
    }, {
        "c_id": "229",
        "name": "Benin",
        "id_country": "28"
    }, {
        "c_id": "441*",
        "name": "Bermuda",
        "id_country": "29"
    }, {
        "c_id": "975",
        "name": "Bhutan",
        "id_country": "30"
    }, {
        "c_id": "591",
        "name": "Bolivia",
        "id_country": "31"
    }, {
        "c_id": "387",
        "name": "Bosnia",
        "id_country": "32"
    }, {
        "c_id": "267",
        "name": "Botswana",
        "id_country": "33"
    }, {
        "c_id": "55",
        "name": "Brazil",
        "id_country": "34"
    }, {
        "c_id": "284*",
        "name": "British Virgin Islands",
        "id_country": "35"
    }, {
        "c_id": "673",
        "name": "Brunei",
        "id_country": "36"
    }, {
        "c_id": "359",
        "name": "Bulgaria",
        "id_country": "37"
    }, {
        "c_id": "226",
        "name": "Burkina Faso",
        "id_country": "38"
    }, {
        "c_id": "257",
        "name": "Burundi",
        "id_country": "40"
    }, {
        "c_id": "855",
        "name": "Cambodia",
        "id_country": "41"
    }, {
        "c_id": "237",
        "name": "Cameroon",
        "id_country": "42"
    }, {
        "c_id": "1",
        "name": "Canada",
        "id_country": "240"
    }, {
        "c_id": "238",
        "name": "Cape Verde Islands",
        "id_country": "44"
    }, {
        "c_id": "345*",
        "name": "Cayman Islands",
        "id_country": "45"
    }, {
        "c_id": "236",
        "name": "Central African Rep.",
        "id_country": "46"
    }, {
        "c_id": "235",
        "name": "Chad",
        "id_country": "47"
    }, {
        "c_id": "56",
        "name": "Chile",
        "id_country": "48"
    }, {
        "c_id": "86",
        "name": "China",
        "id_country": "49"
    }, {
        "c_id": "61",
        "name": "Christmas Island",
        "id_country": "50"
    }, {
        "c_id": "61",
        "name": "Cocos Islands",
        "id_country": "51"
    }, {
        "c_id": "57",
        "name": "Colombia",
        "id_country": "52"
    }, {
        "c_id": "269",
        "name": "Comoros",
        "id_country": "53"
    }, {
        "c_id": "242",
        "name": "Congo",
        "id_country": "54"
    }, {
        "c_id": "243",
        "name": "Congo, Dem. Rep. of",
        "id_country": "55"
    }, {
        "c_id": "682",
        "name": "Cook Islands",
        "id_country": "56"
    }, {
        "c_id": "506",
        "name": "Costa Rica",
        "id_country": "57"
    }, {
        "c_id": "385",
        "name": "Croatia",
        "id_country": "58"
    }, {
        "c_id": "53",
        "name": "Cuba",
        "id_country": "59"
    }, {
        "c_id": "357",
        "name": "Cyprus",
        "id_country": "60"
    }, {
        "c_id": "420",
        "name": "Czech Republic",
        "id_country": "61"
    }, {
        "c_id": "45",
        "name": "Denmark",
        "id_country": "62"
    }, {
        "c_id": "246",
        "name": "Diego Garcia",
        "id_country": "63"
    }, {
        "c_id": "253",
        "name": "Djibouti",
        "id_country": "64"
    }, {
        "c_id": "767*",
        "name": "Dominica",
        "id_country": "65"
    }, {
        "c_id": "809*",
        "name": "Dominican Rep.",
        "id_country": "66"
    }, {
        "c_id": "593",
        "name": "Ecuador",
        "id_country": "67"
    }, {
        "c_id": "20",
        "name": "Egypt",
        "id_country": "68"
    }, {
        "c_id": "503",
        "name": "El Salvador",
        "id_country": "69"
    }, {
        "c_id": "240",
        "name": "Equatorial Guinea",
        "id_country": "70"
    }, {
        "c_id": "291",
        "name": "Eritrea",
        "id_country": "71"
    }, {
        "c_id": "372",
        "name": "Estonia",
        "id_country": "72"
    }, {
        "c_id": "251",
        "name": "Ethiopia",
        "id_country": "73"
    }, {
        "c_id": "298",
        "name": "Faeroe Islands",
        "id_country": "74"
    }, {
        "c_id": "500",
        "name": "Falkland Islands",
        "id_country": "75"
    }, {
        "c_id": "679",
        "name": "Fiji Islands",
        "id_country": "76"
    }, {
        "c_id": "358",
        "name": "Finland",
        "id_country": "77"
    }, {
        "c_id": "33",
        "name": "France",
        "id_country": "78"
    }, {
        "c_id": "594",
        "name": "French Guiana",
        "id_country": "80"
    }, {
        "c_id": "689",
        "name": "French Polynesia",
        "id_country": "81"
    }, {
        "c_id": "241",
        "name": "Gabon",
        "id_country": "82"
    }, {
        "c_id": "220",
        "name": "Gambia",
        "id_country": "83"
    }, {
        "c_id": "995",
        "name": "Georgia",
        "id_country": "84"
    }, {
        "c_id": "49",
        "name": "Germany",
        "id_country": "85"
    }, {
        "c_id": "233",
        "name": "Ghana",
        "id_country": "86"
    }, {
        "c_id": "350",
        "name": "Gibraltar",
        "id_country": "87"
    }, {
        "c_id": "30",
        "name": "Greece",
        "id_country": "88"
    }, {
        "c_id": "299",
        "name": "Greenland",
        "id_country": "89"
    }, {
        "c_id": "473*",
        "name": "Grenada",
        "id_country": "90"
    }, {
        "c_id": "590",
        "name": "Guadeloupe",
        "id_country": "91"
    }, {
        "c_id": "671",
        "name": "Guam",
        "id_country": "92"
    }, {
        "c_id": "502",
        "name": "Guatemala",
        "id_country": "94"
    }, {
        "c_id": "224",
        "name": "Guinea",
        "id_country": "95"
    }, {
        "c_id": "245",
        "name": "Guinea Bissau",
        "id_country": "96"
    }, {
        "c_id": "592",
        "name": "Guyana",
        "id_country": "97"
    }, {
        "c_id": "509",
        "name": "Haiti",
        "id_country": "98"
    }, {
        "c_id": "504",
        "name": "Honduras",
        "id_country": "99"
    }, {
        "c_id": "852",
        "name": "Hong Kong",
        "id_country": "100"
    }, {
        "c_id": "36",
        "name": "Hungary",
        "id_country": "101"
    }, {
        "c_id": "354",
        "name": "Iceland",
        "id_country": "102"
    }, {
        "c_id": "91",
        "name": "India",
        "id_country": "103"
    }, {
        "c_id": "62",
        "name": "Indonesia",
        "id_country": "104"
    }, {
        "c_id": "98",
        "name": "Iran",
        "id_country": "105"
    }, {
        "c_id": "964",
        "name": "Iraq",
        "id_country": "106"
    }, {
        "c_id": "353",
        "name": "Ireland",
        "id_country": "107"
    }, {
        "c_id": "972",
        "name": "Israel",
        "id_country": "108"
    }, {
        "c_id": "39",
        "name": "Italy",
        "id_country": "109"
    }, {
        "c_id": "876*",
        "name": "Jamaica",
        "id_country": "111"
    }, {
        "c_id": "81",
        "name": "Japan",
        "id_country": "112"
    }, {
        "c_id": "962",
        "name": "Jordan",
        "id_country": "113"
    }, {
        "c_id": "7",
        "name": "Kazakhstan",
        "id_country": "114"
    }, {
        "c_id": "254",
        "name": "Kenya",
        "id_country": "115"
    }, {
        "c_id": "686",
        "name": "Kiribati",
        "id_country": "116"
    }, {
        "c_id": "850",
        "name": "Korea, North",
        "id_country": "117"
    }, {
        "c_id": "82",
        "name": "Korea, South",
        "id_country": "118"
    }, {
        "c_id": "965",
        "name": "Kuwait",
        "id_country": "119"
    }, {
        "c_id": "996",
        "name": "Kyrgyzstan",
        "id_country": "120"
    }, {
        "c_id": "856",
        "name": "Laos",
        "id_country": "121"
    }, {
        "c_id": "371",
        "name": "Latvia",
        "id_country": "122"
    }, {
        "c_id": "961",
        "name": "Lebanon",
        "id_country": "123"
    }, {
        "c_id": "266",
        "name": "Lesotho",
        "id_country": "124"
    }, {
        "c_id": "231",
        "name": "Liberia",
        "id_country": "125"
    }, {
        "c_id": "218",
        "name": "Libya",
        "id_country": "126"
    }, {
        "c_id": "423",
        "name": "Liechtenstein",
        "id_country": "127"
    }, {
        "c_id": "370",
        "name": "Lithuania",
        "id_country": "128"
    }, {
        "c_id": "352",
        "name": "Luxembourg",
        "id_country": "129"
    }, {
        "c_id": "853",
        "name": "Macau",
        "id_country": "130"
    }, {
        "c_id": "389",
        "name": "Macedonia",
        "id_country": "131"
    }, {
        "c_id": "261",
        "name": "Madagascar",
        "id_country": "132"
    }, {
        "c_id": "265",
        "name": "Malawi",
        "id_country": "133"
    }, {
        "c_id": "60",
        "name": "Malaysia",
        "id_country": "134"
    }, {
        "c_id": "960",
        "name": "Maldives",
        "id_country": "135"
    }, {
        "c_id": "223",
        "name": "Mali",
        "id_country": "136"
    }, {
        "c_id": "356",
        "name": "Malta",
        "id_country": "137"
    }, {
        "c_id": "670*",
        "name": "Mariana Islands",
        "id_country": "138"
    }, {
        "c_id": "692",
        "name": "Marshall Islands",
        "id_country": "139"
    }, {
        "c_id": "596",
        "name": "Martinique",
        "id_country": "140"
    }, {
        "c_id": "222",
        "name": "Mauritania",
        "id_country": "141"
    }, {
        "c_id": "230",
        "name": "Mauritius",
        "id_country": "142"
    }, {
        "c_id": "269",
        "name": "Mayotte Islands",
        "id_country": "143"
    }, {
        "c_id": "52",
        "name": "Mexico",
        "id_country": "144"
    }, {
        "c_id": "691",
        "name": "Micronesia",
        "id_country": "145"
    }, {
        "c_id": "373",
        "name": "Moldova",
        "id_country": "147"
    }, {
        "c_id": "377",
        "name": "Monaco",
        "id_country": "148"
    }, {
        "c_id": "976",
        "name": "Mongolia",
        "id_country": "149"
    }, {
        "c_id": "664*",
        "name": "Montserrat",
        "id_country": "150"
    }, {
        "c_id": "212",
        "name": "Morocco",
        "id_country": "151"
    }, {
        "c_id": "258",
        "name": "Mozambique",
        "id_country": "152"
    }, {
        "c_id": "95",
        "name": "Myanmar (Burma)",
        "id_country": "153"
    }, {
        "c_id": "264",
        "name": "Namibia",
        "id_country": "154"
    }, {
        "c_id": "674",
        "name": "Nauru",
        "id_country": "155"
    }, {
        "c_id": "977",
        "name": "Nepal",
        "id_country": "156"
    }, {
        "c_id": "31",
        "name": "Netherlands",
        "id_country": "157"
    }, {
        "c_id": "599",
        "name": "Netherlands Antilles",
        "id_country": "158"
    }, {
        "c_id": "869*",
        "name": "Nevis",
        "id_country": "159"
    }, {
        "c_id": "687",
        "name": "New Caledonia",
        "id_country": "160"
    }, {
        "c_id": "64",
        "name": "New Zealand",
        "id_country": "161"
    }, {
        "c_id": "505",
        "name": "Nicaragua",
        "id_country": "162"
    }, {
        "c_id": "227",
        "name": "Niger",
        "id_country": "163"
    }, {
        "c_id": "234",
        "name": "Nigeria",
        "id_country": "164"
    }, {
        "c_id": "683",
        "name": "Niue",
        "id_country": "165"
    }, {
        "c_id": "672",
        "name": "Norfolk Island",
        "id_country": "166"
    }, {
        "c_id": "47",
        "name": "Norway",
        "id_country": "167"
    }, {
        "c_id": "968",
        "name": "Oman",
        "id_country": "168"
    }, {
        "c_id": "92",
        "name": "Pakistan",
        "id_country": "169"
    }, {
        "c_id": "680",
        "name": "Palau",
        "id_country": "170"
    }, {
        "c_id": "970",
        "name": "Palestine",
        "id_country": "171"
    }, {
        "c_id": "507",
        "name": "Panama",
        "id_country": "172"
    }, {
        "c_id": "675",
        "name": "Papua New Guinea",
        "id_country": "173"
    }, {
        "c_id": "595",
        "name": "Paraguay",
        "id_country": "174"
    }, {
        "c_id": "51",
        "name": "Peru",
        "id_country": "175"
    }, {
        "c_id": "63",
        "name": "Philippines",
        "id_country": "176"
    }, {
        "c_id": "48",
        "name": "Poland",
        "id_country": "177"
    }, {
        "c_id": "351",
        "name": "Portugal",
        "id_country": "178"
    }, {
        "c_id": "787*",
        "name": "Puerto Rico",
        "id_country": "179"
    }, {
        "c_id": "974",
        "name": "Qatar",
        "id_country": "180"
    }, {
        "c_id": "262",
        "name": "Reunion Island",
        "id_country": "181"
    }, {
        "c_id": "40",
        "name": "Romania",
        "id_country": "182"
    }, {
        "c_id": "7",
        "name": "Russia",
        "id_country": "183"
    }, {
        "c_id": "250",
        "name": "Rwanda",
        "id_country": "184"
    }, {
        "c_id": "378",
        "name": "San Marino",
        "id_country": "190"
    }, {
        "c_id": "239",
        "name": "Sao Tome & Principe",
        "id_country": "191"
    }, {
        "c_id": "966",
        "name": "Saudi Arabia",
        "id_country": "192"
    }, {
        "c_id": "221",
        "name": "Senegal",
        "id_country": "193"
    }, {
        "c_id": "381",
        "name": "Serbia",
        "id_country": "194"
    }, {
        "c_id": "248",
        "name": "Seychelles",
        "id_country": "195"
    }, {
        "c_id": "232",
        "name": "Sierra Leone",
        "id_country": "196"
    }, {
        "c_id": "65",
        "name": "Singapore",
        "id_country": "197"
    }, {
        "c_id": "421",
        "name": "Slovakia",
        "id_country": "198"
    }, {
        "c_id": "386",
        "name": "Slovenia",
        "id_country": "199"
    }, {
        "c_id": "677",
        "name": "Solomon Islands",
        "id_country": "200"
    }, {
        "c_id": "252",
        "name": "Somalia",
        "id_country": "201"
    }, {
        "c_id": "27",
        "name": "South Africa",
        "id_country": "202"
    }, {
        "c_id": "34",
        "name": "Spain",
        "id_country": "203"
    }, {
        "c_id": "94",
        "name": "Sri Lanka",
        "id_country": "204"
    }, {
        "c_id": "290",
        "name": "St. Helena",
        "id_country": "185"
    }, {
        "c_id": "869*",
        "name": "St. Kitts",
        "id_country": "186"
    }, {
        "c_id": "758*",
        "name": "St. Lucia",
        "id_country": "187"
    }, {
        "c_id": "508",
        "name": "St. Perre & Miquelon",
        "id_country": "188"
    }, {
        "c_id": "784*",
        "name": "St. Vincent",
        "id_country": "189"
    }, {
        "c_id": "249",
        "name": "Sudan",
        "id_country": "205"
    }, {
        "c_id": "597",
        "name": "Suriname",
        "id_country": "206"
    }, {
        "c_id": "268",
        "name": "Swaziland",
        "id_country": "207"
    }, {
        "c_id": "46",
        "name": "Sweden",
        "id_country": "208"
    }, {
        "c_id": "41",
        "name": "Switzerland",
        "id_country": "209"
    }, {
        "c_id": "963",
        "name": "Syria",
        "id_country": "210"
    }, {
        "c_id": "886",
        "name": "Taiwan",
        "id_country": "211"
    }, {
        "c_id": "992",
        "name": "Tajikistan",
        "id_country": "212"
    }, {
        "c_id": "255",
        "name": "Tanzania",
        "id_country": "213"
    }, {
        "c_id": "66",
        "name": "Thailand",
        "id_country": "214"
    }, {
        "c_id": "228",
        "name": "Togo",
        "id_country": "215"
    }, {
        "c_id": "676",
        "name": "Tonga",
        "id_country": "216"
    }, {
        "c_id": "868*",
        "name": "Trinidad & Tobago",
        "id_country": "217"
    }, {
        "c_id": "216",
        "name": "Tunisia",
        "id_country": "218"
    }, {
        "c_id": "90",
        "name": "Turkey",
        "id_country": "219"
    }, {
        "c_id": "993",
        "name": "Turkmenistan",
        "id_country": "220"
    }, {
        "c_id": "649*",
        "name": "Turks & Caicos",
        "id_country": "221"
    }, {
        "c_id": "688",
        "name": "Tuvalu",
        "id_country": "222"
    }, {
        "c_id": "256",
        "name": "Uganda",
        "id_country": "223"
    }, {
        "c_id": "380",
        "name": "Ukraine",
        "id_country": "224"
    }, {
        "c_id": "971",
        "name": "United Arab Emirates",
        "id_country": "225"
    }, {
        "c_id": "44",
        "name": "United Kingdom",
        "id_country": "226"
    }, {
        "c_id": "598",
        "name": "Uruguay",
        "id_country": "227"
    }, {
        "c_id": "1",
        "name": "USA",
        "id_country": "3"
    }, {
        "c_id": "998",
        "name": "Uzbekistan",
        "id_country": "228"
    }, {
        "c_id": "678",
        "name": "Vanuatu",
        "id_country": "229"
    }, {
        "c_id": "39",
        "name": "Vatican City",
        "id_country": "230"
    }, {
        "c_id": "58",
        "name": "Venezuela",
        "id_country": "231"
    }, {
        "c_id": "84",
        "name": "Vietnam",
        "id_country": "232"
    }, {
        "c_id": "681",
        "name": "Wallis & Futuna",
        "id_country": "234"
    }, {
        "c_id": "685",
        "name": "Western Samoa",
        "id_country": "235"
    }, {
        "c_id": "967",
        "name": "Yemen",
        "id_country": "236"
    }, {
        "c_id": "381",
        "name": "Yugoslavia",
        "id_country": "237"
    }, {
        "c_id": "260",
        "name": "Zambia",
        "id_country": "238"
    }, {
        "c_id": "263",
        "name": "Zimbabwe",
        "id_country": "239"
    }];

    OrderForm.country = [{
        "c_id": "93",
        "name": "Afghanistan",
        "id_country": "4"
    }, {
        "c_id": "355",
        "name": "Albania",
        "id_country": "5"
    }, {
        "c_id": "213",
        "name": "Algeria",
        "id_country": "6"
    }, {
        "c_id": "684",
        "name": "American Samoa",
        "id_country": "7"
    }, {
        "c_id": "376",
        "name": "Andorra",
        "id_country": "8"
    }, {
        "c_id": "244",
        "name": "Angola",
        "id_country": "9"
    }, {
        "c_id": "264*",
        "name": "Anguilla",
        "id_country": "10"
    }, {
        "c_id": "672",
        "name": "Antarctica",
        "id_country": "11"
    }, {
        "c_id": "268*",
        "name": "Antigua",
        "id_country": "12"
    }, {
        "c_id": "54",
        "name": "Argentina",
        "id_country": "13"
    }, {
        "c_id": "374",
        "name": "Armenia",
        "id_country": "14"
    }, {
        "c_id": "297",
        "name": "Aruba",
        "id_country": "15"
    }, {
        "c_id": "247",
        "name": "Ascension Island",
        "id_country": "16"
    }, {
        "c_id": "61",
        "name": "Australia",
        "id_country": "17"
    }, {
        "c_id": "43",
        "name": "Austria",
        "id_country": "18"
    }, {
        "c_id": "994",
        "name": "Azberbaijan",
        "id_country": "19"
    }, {
        "c_id": "242*",
        "name": "Bahamas",
        "id_country": "20"
    }, {
        "c_id": "973",
        "name": "Bahrain",
        "id_country": "21"
    }, {
        "c_id": "880",
        "name": "Bangladesh",
        "id_country": "22"
    }, {
        "c_id": "246*",
        "name": "Barbados",
        "id_country": "23"
    }, {
        "c_id": "268*",
        "name": "Barbuda",
        "id_country": "24"
    }, {
        "c_id": "375",
        "name": "Belarus",
        "id_country": "25"
    }, {
        "c_id": "32",
        "name": "Belgium",
        "id_country": "26"
    }, {
        "c_id": "501",
        "name": "Belize",
        "id_country": "27"
    }, {
        "c_id": "229",
        "name": "Benin",
        "id_country": "28"
    }, {
        "c_id": "441*",
        "name": "Bermuda",
        "id_country": "29"
    }, {
        "c_id": "975",
        "name": "Bhutan",
        "id_country": "30"
    }, {
        "c_id": "591",
        "name": "Bolivia",
        "id_country": "31"
    }, {
        "c_id": "387",
        "name": "Bosnia",
        "id_country": "32"
    }, {
        "c_id": "267",
        "name": "Botswana",
        "id_country": "33"
    }, {
        "c_id": "55",
        "name": "Brazil",
        "id_country": "34"
    }, {
        "c_id": "284*",
        "name": "British Virgin Islands",
        "id_country": "35"
    }, {
        "c_id": "673",
        "name": "Brunei",
        "id_country": "36"
    }, {
        "c_id": "359",
        "name": "Bulgaria",
        "id_country": "37"
    }, {
        "c_id": "226",
        "name": "Burkina Faso",
        "id_country": "38"
    }, {
        "c_id": "257",
        "name": "Burundi",
        "id_country": "40"
    }, {
        "c_id": "855",
        "name": "Cambodia",
        "id_country": "41"
    }, {
        "c_id": "237",
        "name": "Cameroon",
        "id_country": "42"
    }, {
        "c_id": "1",
        "name": "Canada",
        "id_country": "240"
    }, {
        "c_id": "238",
        "name": "Cape Verde Islands",
        "id_country": "44"
    }, {
        "c_id": "345*",
        "name": "Cayman Islands",
        "id_country": "45"
    }, {
        "c_id": "236",
        "name": "Central African Rep.",
        "id_country": "46"
    }, {
        "c_id": "235",
        "name": "Chad",
        "id_country": "47"
    }, {
        "c_id": "56",
        "name": "Chile",
        "id_country": "48"
    }, {
        "c_id": "86",
        "name": "China",
        "id_country": "49"
    }, {
        "c_id": "61",
        "name": "Christmas Island",
        "id_country": "50"
    }, {
        "c_id": "61",
        "name": "Cocos Islands",
        "id_country": "51"
    }, {
        "c_id": "57",
        "name": "Colombia",
        "id_country": "52"
    }, {
        "c_id": "269",
        "name": "Comoros",
        "id_country": "53"
    }, {
        "c_id": "242",
        "name": "Congo",
        "id_country": "54"
    }, {
        "c_id": "243",
        "name": "Congo, Dem. Rep. of",
        "id_country": "55"
    }, {
        "c_id": "682",
        "name": "Cook Islands",
        "id_country": "56"
    }, {
        "c_id": "506",
        "name": "Costa Rica",
        "id_country": "57"
    }, {
        "c_id": "385",
        "name": "Croatia",
        "id_country": "58"
    }, {
        "c_id": "53",
        "name": "Cuba",
        "id_country": "59"
    }, {
        "c_id": "357",
        "name": "Cyprus",
        "id_country": "60"
    }, {
        "c_id": "420",
        "name": "Czech Republic",
        "id_country": "61"
    }, {
        "c_id": "45",
        "name": "Denmark",
        "id_country": "62"
    }, {
        "c_id": "246",
        "name": "Diego Garcia",
        "id_country": "63"
    }, {
        "c_id": "253",
        "name": "Djibouti",
        "id_country": "64"
    }, {
        "c_id": "767*",
        "name": "Dominica",
        "id_country": "65"
    }, {
        "c_id": "809*",
        "name": "Dominican Rep.",
        "id_country": "66"
    }, {
        "c_id": "593",
        "name": "Ecuador",
        "id_country": "67"
    }, {
        "c_id": "20",
        "name": "Egypt",
        "id_country": "68"
    }, {
        "c_id": "503",
        "name": "El Salvador",
        "id_country": "69"
    }, {
        "c_id": "240",
        "name": "Equatorial Guinea",
        "id_country": "70"
    }, {
        "c_id": "291",
        "name": "Eritrea",
        "id_country": "71"
    }, {
        "c_id": "372",
        "name": "Estonia",
        "id_country": "72"
    }, {
        "c_id": "251",
        "name": "Ethiopia",
        "id_country": "73"
    }, {
        "c_id": "298",
        "name": "Faeroe Islands",
        "id_country": "74"
    }, {
        "c_id": "500",
        "name": "Falkland Islands",
        "id_country": "75"
    }, {
        "c_id": "679",
        "name": "Fiji Islands",
        "id_country": "76"
    }, {
        "c_id": "358",
        "name": "Finland",
        "id_country": "77"
    }, {
        "c_id": "33",
        "name": "France",
        "id_country": "78"
    }, {
        "c_id": "594",
        "name": "French Guiana",
        "id_country": "80"
    }, {
        "c_id": "689",
        "name": "French Polynesia",
        "id_country": "81"
    }, {
        "c_id": "241",
        "name": "Gabon",
        "id_country": "82"
    }, {
        "c_id": "220",
        "name": "Gambia",
        "id_country": "83"
    }, {
        "c_id": "995",
        "name": "Georgia",
        "id_country": "84"
    }, {
        "c_id": "49",
        "name": "Germany",
        "id_country": "85"
    }, {
        "c_id": "233",
        "name": "Ghana",
        "id_country": "86"
    }, {
        "c_id": "350",
        "name": "Gibraltar",
        "id_country": "87"
    }, {
        "c_id": "30",
        "name": "Greece",
        "id_country": "88"
    }, {
        "c_id": "299",
        "name": "Greenland",
        "id_country": "89"
    }, {
        "c_id": "473*",
        "name": "Grenada",
        "id_country": "90"
    }, {
        "c_id": "590",
        "name": "Guadeloupe",
        "id_country": "91"
    }, {
        "c_id": "671",
        "name": "Guam",
        "id_country": "92"
    }, {
        "c_id": "502",
        "name": "Guatemala",
        "id_country": "94"
    }, {
        "c_id": "224",
        "name": "Guinea",
        "id_country": "95"
    }, {
        "c_id": "245",
        "name": "Guinea Bissau",
        "id_country": "96"
    }, {
        "c_id": "592",
        "name": "Guyana",
        "id_country": "97"
    }, {
        "c_id": "509",
        "name": "Haiti",
        "id_country": "98"
    }, {
        "c_id": "504",
        "name": "Honduras",
        "id_country": "99"
    }, {
        "c_id": "852",
        "name": "Hong Kong",
        "id_country": "100"
    }, {
        "c_id": "36",
        "name": "Hungary",
        "id_country": "101"
    }, {
        "c_id": "354",
        "name": "Iceland",
        "id_country": "102"
    }, {
        "c_id": "91",
        "name": "India",
        "id_country": "103"
    }, {
        "c_id": "62",
        "name": "Indonesia",
        "id_country": "104"
    }, {
        "c_id": "98",
        "name": "Iran",
        "id_country": "105"
    }, {
        "c_id": "964",
        "name": "Iraq",
        "id_country": "106"
    }, {
        "c_id": "353",
        "name": "Ireland",
        "id_country": "107"
    }, {
        "c_id": "972",
        "name": "Israel",
        "id_country": "108"
    }, {
        "c_id": "39",
        "name": "Italy",
        "id_country": "109"
    }, {
        "c_id": "876*",
        "name": "Jamaica",
        "id_country": "111"
    }, {
        "c_id": "81",
        "name": "Japan",
        "id_country": "112"
    }, {
        "c_id": "962",
        "name": "Jordan",
        "id_country": "113"
    }, {
        "c_id": "7",
        "name": "Kazakhstan",
        "id_country": "114"
    }, {
        "c_id": "254",
        "name": "Kenya",
        "id_country": "115"
    }, {
        "c_id": "686",
        "name": "Kiribati",
        "id_country": "116"
    }, {
        "c_id": "850",
        "name": "Korea, North",
        "id_country": "117"
    }, {
        "c_id": "82",
        "name": "Korea, South",
        "id_country": "118"
    }, {
        "c_id": "965",
        "name": "Kuwait",
        "id_country": "119"
    }, {
        "c_id": "996",
        "name": "Kyrgyzstan",
        "id_country": "120"
    }, {
        "c_id": "856",
        "name": "Laos",
        "id_country": "121"
    }, {
        "c_id": "371",
        "name": "Latvia",
        "id_country": "122"
    }, {
        "c_id": "961",
        "name": "Lebanon",
        "id_country": "123"
    }, {
        "c_id": "266",
        "name": "Lesotho",
        "id_country": "124"
    }, {
        "c_id": "231",
        "name": "Liberia",
        "id_country": "125"
    }, {
        "c_id": "218",
        "name": "Libya",
        "id_country": "126"
    }, {
        "c_id": "423",
        "name": "Liechtenstein",
        "id_country": "127"
    }, {
        "c_id": "370",
        "name": "Lithuania",
        "id_country": "128"
    }, {
        "c_id": "352",
        "name": "Luxembourg",
        "id_country": "129"
    }, {
        "c_id": "853",
        "name": "Macau",
        "id_country": "130"
    }, {
        "c_id": "389",
        "name": "Macedonia",
        "id_country": "131"
    }, {
        "c_id": "261",
        "name": "Madagascar",
        "id_country": "132"
    }, {
        "c_id": "265",
        "name": "Malawi",
        "id_country": "133"
    }, {
        "c_id": "60",
        "name": "Malaysia",
        "id_country": "134"
    }, {
        "c_id": "960",
        "name": "Maldives",
        "id_country": "135"
    }, {
        "c_id": "223",
        "name": "Mali",
        "id_country": "136"
    }, {
        "c_id": "356",
        "name": "Malta",
        "id_country": "137"
    }, {
        "c_id": "670*",
        "name": "Mariana Islands",
        "id_country": "138"
    }, {
        "c_id": "692",
        "name": "Marshall Islands",
        "id_country": "139"
    }, {
        "c_id": "596",
        "name": "Martinique",
        "id_country": "140"
    }, {
        "c_id": "222",
        "name": "Mauritania",
        "id_country": "141"
    }, {
        "c_id": "230",
        "name": "Mauritius",
        "id_country": "142"
    }, {
        "c_id": "269",
        "name": "Mayotte Islands",
        "id_country": "143"
    }, {
        "c_id": "52",
        "name": "Mexico",
        "id_country": "144"
    }, {
        "c_id": "691",
        "name": "Micronesia",
        "id_country": "145"
    }, {
        "c_id": "373",
        "name": "Moldova",
        "id_country": "147"
    }, {
        "c_id": "377",
        "name": "Monaco",
        "id_country": "148"
    }, {
        "c_id": "976",
        "name": "Mongolia",
        "id_country": "149"
    }, {
        "c_id": "664*",
        "name": "Montserrat",
        "id_country": "150"
    }, {
        "c_id": "212",
        "name": "Morocco",
        "id_country": "151"
    }, {
        "c_id": "258",
        "name": "Mozambique",
        "id_country": "152"
    }, {
        "c_id": "95",
        "name": "Myanmar (Burma)",
        "id_country": "153"
    }, {
        "c_id": "264",
        "name": "Namibia",
        "id_country": "154"
    }, {
        "c_id": "674",
        "name": "Nauru",
        "id_country": "155"
    }, {
        "c_id": "977",
        "name": "Nepal",
        "id_country": "156"
    }, {
        "c_id": "31",
        "name": "Netherlands",
        "id_country": "157"
    }, {
        "c_id": "599",
        "name": "Netherlands Antilles",
        "id_country": "158"
    }, {
        "c_id": "869*",
        "name": "Nevis",
        "id_country": "159"
    }, {
        "c_id": "687",
        "name": "New Caledonia",
        "id_country": "160"
    }, {
        "c_id": "64",
        "name": "New Zealand",
        "id_country": "161"
    }, {
        "c_id": "505",
        "name": "Nicaragua",
        "id_country": "162"
    }, {
        "c_id": "227",
        "name": "Niger",
        "id_country": "163"
    }, {
        "c_id": "234",
        "name": "Nigeria",
        "id_country": "164"
    }, {
        "c_id": "683",
        "name": "Niue",
        "id_country": "165"
    }, {
        "c_id": "672",
        "name": "Norfolk Island",
        "id_country": "166"
    }, {
        "c_id": "47",
        "name": "Norway",
        "id_country": "167"
    }, {
        "c_id": "968",
        "name": "Oman",
        "id_country": "168"
    }, {
        "c_id": "92",
        "name": "Pakistan",
        "id_country": "169"
    }, {
        "c_id": "680",
        "name": "Palau",
        "id_country": "170"
    }, {
        "c_id": "970",
        "name": "Palestine",
        "id_country": "171"
    }, {
        "c_id": "507",
        "name": "Panama",
        "id_country": "172"
    }, {
        "c_id": "675",
        "name": "Papua New Guinea",
        "id_country": "173"
    }, {
        "c_id": "595",
        "name": "Paraguay",
        "id_country": "174"
    }, {
        "c_id": "51",
        "name": "Peru",
        "id_country": "175"
    }, {
        "c_id": "63",
        "name": "Philippines",
        "id_country": "176"
    }, {
        "c_id": "48",
        "name": "Poland",
        "id_country": "177"
    }, {
        "c_id": "351",
        "name": "Portugal",
        "id_country": "178"
    }, {
        "c_id": "787*",
        "name": "Puerto Rico",
        "id_country": "179"
    }, {
        "c_id": "974",
        "name": "Qatar",
        "id_country": "180"
    }, {
        "c_id": "262",
        "name": "Reunion Island",
        "id_country": "181"
    }, {
        "c_id": "40",
        "name": "Romania",
        "id_country": "182"
    }, {
        "c_id": "7",
        "name": "Russia",
        "id_country": "183"
    }, {
        "c_id": "250",
        "name": "Rwanda",
        "id_country": "184"
    }, {
        "c_id": "378",
        "name": "San Marino",
        "id_country": "190"
    }, {
        "c_id": "239",
        "name": "Sao Tome & Principe",
        "id_country": "191"
    }, {
        "c_id": "966",
        "name": "Saudi Arabia",
        "id_country": "192"
    }, {
        "c_id": "221",
        "name": "Senegal",
        "id_country": "193"
    }, {
        "c_id": "381",
        "name": "Serbia",
        "id_country": "194"
    }, {
        "c_id": "248",
        "name": "Seychelles",
        "id_country": "195"
    }, {
        "c_id": "232",
        "name": "Sierra Leone",
        "id_country": "196"
    }, {
        "c_id": "65",
        "name": "Singapore",
        "id_country": "197"
    }, {
        "c_id": "421",
        "name": "Slovakia",
        "id_country": "198"
    }, {
        "c_id": "386",
        "name": "Slovenia",
        "id_country": "199"
    }, {
        "c_id": "677",
        "name": "Solomon Islands",
        "id_country": "200"
    }, {
        "c_id": "252",
        "name": "Somalia",
        "id_country": "201"
    }, {
        "c_id": "27",
        "name": "South Africa",
        "id_country": "202"
    }, {
        "c_id": "34",
        "name": "Spain",
        "id_country": "203"
    }, {
        "c_id": "94",
        "name": "Sri Lanka",
        "id_country": "204"
    }, {
        "c_id": "290",
        "name": "St. Helena",
        "id_country": "185"
    }, {
        "c_id": "869*",
        "name": "St. Kitts",
        "id_country": "186"
    }, {
        "c_id": "758*",
        "name": "St. Lucia",
        "id_country": "187"
    }, {
        "c_id": "508",
        "name": "St. Perre & Miquelon",
        "id_country": "188"
    }, {
        "c_id": "784*",
        "name": "St. Vincent",
        "id_country": "189"
    }, {
        "c_id": "249",
        "name": "Sudan",
        "id_country": "205"
    }, {
        "c_id": "597",
        "name": "Suriname",
        "id_country": "206"
    }, {
        "c_id": "268",
        "name": "Swaziland",
        "id_country": "207"
    }, {
        "c_id": "46",
        "name": "Sweden",
        "id_country": "208"
    }, {
        "c_id": "41",
        "name": "Switzerland",
        "id_country": "209"
    }, {
        "c_id": "963",
        "name": "Syria",
        "id_country": "210"
    }, {
        "c_id": "886",
        "name": "Taiwan",
        "id_country": "211"
    }, {
        "c_id": "992",
        "name": "Tajikistan",
        "id_country": "212"
    }, {
        "c_id": "255",
        "name": "Tanzania",
        "id_country": "213"
    }, {
        "c_id": "66",
        "name": "Thailand",
        "id_country": "214"
    }, {
        "c_id": "228",
        "name": "Togo",
        "id_country": "215"
    }, {
        "c_id": "676",
        "name": "Tonga",
        "id_country": "216"
    }, {
        "c_id": "868*",
        "name": "Trinidad & Tobago",
        "id_country": "217"
    }, {
        "c_id": "216",
        "name": "Tunisia",
        "id_country": "218"
    }, {
        "c_id": "90",
        "name": "Turkey",
        "id_country": "219"
    }, {
        "c_id": "993",
        "name": "Turkmenistan",
        "id_country": "220"
    }, {
        "c_id": "649*",
        "name": "Turks & Caicos",
        "id_country": "221"
    }, {
        "c_id": "688",
        "name": "Tuvalu",
        "id_country": "222"
    }, {
        "c_id": "256",
        "name": "Uganda",
        "id_country": "223"
    }, {
        "c_id": "380",
        "name": "Ukraine",
        "id_country": "224"
    }, {
        "c_id": "971",
        "name": "United Arab Emirates",
        "id_country": "225"
    }, {
        "c_id": "44",
        "name": "United Kingdom",
        "id_country": "226"
    }, {
        "c_id": "598",
        "name": "Uruguay",
        "id_country": "227"
    }, {
        "c_id": "1",
        "name": "USA",
        "id_country": "3"
    }, {
        "c_id": "998",
        "name": "Uzbekistan",
        "id_country": "228"
    }, {
        "c_id": "678",
        "name": "Vanuatu",
        "id_country": "229"
    }, {
        "c_id": "39",
        "name": "Vatican City",
        "id_country": "230"
    }, {
        "c_id": "58",
        "name": "Venezuela",
        "id_country": "231"
    }, {
        "c_id": "84",
        "name": "Vietnam",
        "id_country": "232"
    }, {
        "c_id": "681",
        "name": "Wallis & Futuna",
        "id_country": "234"
    }, {
        "c_id": "685",
        "name": "Western Samoa",
        "id_country": "235"
    }, {
        "c_id": "967",
        "name": "Yemen",
        "id_country": "236"
    }, {
        "c_id": "381",
        "name": "Yugoslavia",
        "id_country": "237"
    }, {
        "c_id": "260",
        "name": "Zambia",
        "id_country": "238"
    }, {
        "c_id": "263",
        "name": "Zimbabwe",
        "id_country": "239"
    }];

    OrderForm.validationImgBtn = '';
    OrderForm.validationName = '';

    OrderForm.removePrefWriterImg = '<img class="delete" src="/images/delete16x16.gif" title="Remove preferred writer" alt="x" />';

    OrderForm.BTP_validation = false;
    OrderForm.ForceSubmit = false;


    function globalInit() {
        $(document).ready(function() {
            if (document.order_form !== undefined) {
                document.order_form.addEventListener("submit", function () {
                   return OrderForm.submit();
                });
            }
            if ($('.btn_edit_order_prev').length == 0) {
                checkSomeRequiredFields(10, false);
            }
            $('#personal_info input').focus(function() {
                // $(this).css('color','#000');
            });
            $('#personal_info input').blur(function() {
                //$(this).css('color','#888');
            });
            $('#country').change(function() {
                checkCountry(false)
            });
            $('#firstname').keyup(function() {
                checkFirstName(false)
            });
            $('#lastname').keyup(function() {
                checkLastName(false)
            });
            $('#phone1_area').keyup(function() {
                checkPhone1Area(false)
            });
            $('#phone1_number').keyup(function() {
                checkPhone1(false)
            });
            $('#firstname').click(function() {
                if (this.value == 'First name') {
                    this.value = '';
                }
            });
            $('#lastname').click(function() {
                if (this.value == 'Last name') {
                    this.value = '';
                }
            });
            $('#phone1_area').click(function() {
                if (this.value == 'area') {
                    this.value = '';
                }
            });
            $('#phone1_number').click(function() {
                if (this.value == 'number') {
                    this.value = '';
                }
            });
            $('#email').blur(function() {
                AjaxCheckEl('email', false)
            });
            $('#retype_email').blur(function() {
                checkRetypeEmail(false)
            });
            $('#phone2').keyup(function() {
                checkPhone2Area()
            });


            $('#topic').keyup(function() {
                checkTopic(false)
            });
            $('#details').keyup(function() {
                checkDetails(false)
            });
            $('#order_category').change(function() {
                checkOrderCategory(1, false);
                checkOrderCategory(2, false)
            });
            $('#numpages').change(function() {
                checkNumpages(false)
            });

            OrderForm.repaintTable();
            OrderForm.afterSwitchForms.push(OrderForm.SP_NewDesign.init);
            OrderForm.afterOnInputChange.push(OrderForm.SP_NewDesign.changePages);
            OrderForm.SP_NewDesign.init();

            OrderForm.languageStyle.customizeStyleSelect();
            OrderForm.updateCheckboxes();
            OrderForm.afterSwitchForms.push(OrderForm.updateCheckboxes);
            $('#topic').focus();
            $('#firstname').focus();

            if (typeof(OrderFormExternalInit) == 'undefined' || OrderFormExternalInit == false) {
                OrderFormInit();
            }
        })

    }

    return {
        init: globalInit
    }
});
