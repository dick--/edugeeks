define(function(){
	var configMenu = {
		topNav: '.topNav',
		mobileMenuPanel: '.mobileMenuPanel',
		logoHighRes: '.logo.highres',
		loginArea: '.loginArea',
		socials: '.socials'
	}

	function insertAfter(elem, refElem) {
 	   return refElem.parentNode.insertBefore(elem, refElem.nextSibling);
	}

	function refreshElementsMobile() {
		//refreshing menus positions for mobiles
		var menu = document.querySelector(configMenu.topNav),
			topSelector = document.querySelector(configMenu.mobileMenuPanel),
			bottomSelector = document.querySelector(configMenu.logoHighRes),
			loginArea = document.querySelector(configMenu.loginArea),
			socials = document.querySelector(configMenu.socials);

		if (window.innerWidth < 768) {
			insertAfter(menu,topSelector);
			insertAfter(socials,topSelector);
			insertAfter(loginArea,topSelector);
		} else {
			insertAfter(menu,bottomSelector);
			bottomSelector.parentNode.insertBefore(loginArea,bottomSelector);
			bottomSelector.parentNode.insertBefore(socials,bottomSelector);
		}
	}

	function init() {
		window.addEventListener("resize", function() {
			refreshElementsMobile();
		}, true);

		refreshElementsMobile();
	}

	return {
		init: init
	}
})