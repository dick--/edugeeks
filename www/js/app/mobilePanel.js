define(['app/class'],function(classs){

	var configMenu = {
		naviActionBtnId: 'menuBtnAction',
		optActionBtnId: 'optionsBtnAction',
		naviMenuId: 'mobileNaviMenu',
		optMenuId: 'mobileOptionsMenu',
		openedMenu: 'openedMenu',
		openedOpt: 'openedOpt',
		mainMenuSel: '.mainMenu'
	}
	var isIos = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );

	function menuToggler(e) {
		if (e.target.id.indexOf(configMenu.naviActionBtnId) !== -1) {
			if (e.target.parentNode.parentNode.className.indexOf(configMenu.openedOpt) !== -1) {
				classs.toggle(e.target.parentNode.parentNode, configMenu.openedOpt);
				classs.toggle(e.target.parentNode.parentNode, configMenu.openedMenu);
			} else {
				classs.toggle(e.target.parentNode.parentNode, configMenu.openedMenu);
			}
		}
	}
	function optToggler(e) {
		if (e.target.id.indexOf(configMenu.optActionBtnId) !== -1) {
			if (e.target.parentNode.parentNode.className.indexOf(configMenu.openedMenu) !== -1) {
				classs.toggle(e.target.parentNode.parentNode, configMenu.openedMenu);
				classs.toggle(e.target.parentNode.parentNode, configMenu.openedOpt);
			} else {
				classs.toggle(e.target.parentNode.parentNode, configMenu.openedOpt);
			}
		}
	}


	function init() {
		if (isIos) {
			document.body.addEventListener('touchstart', function (e) {
				menuToggler(e);
				optToggler(e);
			}, true);
			document.body.addEventListener('touchend', function (e) {
				menuToggler(e);
				optToggler(e);
			}, true);
		} else {
			document.body.addEventListener('click', function (e) {
				menuToggler(e);
				optToggler(e);
			}, false);
		}
	}
	
	return {
		init: init
	}
})