define(function(order) {
    OrderForm.doctype = 0;
    OrderForm.hours = [{
        "1": 168,
        "3": 120,
        "4": 96,
        "5": 72,
        "6": 48,
        "7": 24,
        "8": 12,
        "9": 6,
        "10": 3,
        "11": 240
    }];
    OrderForm.prices = [{
        "1": {
            "1": [20.99],
            "3": [21.99],
            "4": [22.99],
            "5": [24.99],
            "6": [32.99],
            "7": [34.99],
            "8": [36.99],
            "9": [38.99],
            "10": [41.99],
            "11": [19.99]
        },
        "2": {
            "1": [22.99],
            "3": [23.99],
            "4": [24.99],
            "5": [26.99],
            "6": [34.99],
            "7": [36.99],
            "8": [38.99],
            "9": [40.99],
            "10": [43.99],
            "11": [21.99]
        },
        "17": {
            "1": [11.99],
            "3": [13.99],
            "4": [15.99],
            "5": [17.99],
            "6": [22.99],
            "7": [25.99],
            "8": [27.99],
            "9": [29.99],
            "10": [32.99],
            "11": [10.99]
        },
        "18": {
            "1": [13.99],
            "3": [15.99],
            "4": [17.99],
            "5": [18.99],
            "6": [24.99],
            "7": [27.99],
            "8": [30.99],
            "9": [32.99],
            "10": [35.99],
            "11": [12.99]
        },
        "19": {
            "1": [14.99],
            "3": [16.99],
            "4": [18.99],
            "5": [20.99],
            "6": [25.99],
            "7": [28.99],
            "8": [31.99],
            "9": [33.99],
            "10": [36.99],
            "11": [13.99]
        },
        "20": {
            "1": [15.99],
            "3": [17.99],
            "4": [19.99],
            "5": [21.99],
            "6": [26.99],
            "7": [29.99],
            "8": [32.99],
            "9": [34.99],
            "10": [37.99],
            "11": [14.99]
        },
        "21": {
            "1": [16.99],
            "3": [18.99],
            "4": [20.99],
            "5": [22.99],
            "6": [27.99],
            "7": [30.99],
            "8": [33.99],
            "9": [35.99],
            "10": [38.99],
            "11": [15.99]
        },
        "22": {
            "1": [17.99],
            "3": [19.99],
            "4": [21.99],
            "5": [23.99],
            "6": [28.99],
            "7": [31.99],
            "8": [34.99],
            "9": [36.99],
            "10": [39.99],
            "11": [16.99]
        },
        "27": {
            "1": [24.99],
            "3": [25.99],
            "4": [27.99],
            "5": [30.99],
            "6": [36.99],
            "7": [40.99],
            "8": [44.99],
            "9": [47.99],
            "10": [52.99],
            "11": [23.99]
        }
    }];
    OrderForm.free_vas = [];
    OrderForm.admin_free_vas = [];
    OrderForm.fieldDoctypes = {
        "10": null,
        "11": null,
        "13": null,
        "50": null,
        "34": null,
        "12": null,
        "88": null,
        "15": null,
        "69": null,
        "151": "237",
        "153": "185",
        "152": "187",
        "27": null,
        "143": null,
        "135": null,
        "18": null,
        "19": null,
        "20": null,
        "142": "224",
        "130": null,
        "21": null,
        "86": null,
        "87": null,
        "8": null,
        "9": null
    };
    OrderForm.limits = [{
        "1": {
            "5": "40",
            "6": "19",
            "7": "12",
            "8": "8",
            "9": "4",
            "10": "3"
        },
        "2": {
            "5": "40",
            "6": "19",
            "7": "12",
            "8": "8",
            "9": "4",
            "10": "3"
        },
        "27": {
            "5": "40",
            "6": "19",
            "7": "12",
            "8": "8",
            "9": "4",
            "10": "3"
        }
    }];
    OrderForm.price_groups = [
        []
    ];
    OrderForm.currencyRates = {
        "AUD": {
            "AUD": 1,
            "CAD": 1.003,
            "EUR": 0.6783,
            "GBP": 0.551,
            "USD": 0.9239
        },
        "CAD": {
            "AUD": 0.997,
            "CAD": 1,
            "EUR": 0.6763,
            "GBP": 0.5494,
            "USD": 0.9211
        },
        "EUR": {
            "AUD": 1.4743,
            "CAD": 1.4786,
            "EUR": 1,
            "GBP": 0.8124,
            "USD": 1.362
        },
        "GBP": {
            "AUD": 1.8149,
            "CAD": 1.8202,
            "EUR": 1.2309,
            "GBP": 1,
            "USD": 1.6767
        },
        "USD": {
            "AUD": 1.0824,
            "CAD": 1.0857,
            "EUR": 0.7342,
            "GBP": 0.5964,
            "USD": 1
        }
    };

    /******************************************************************/
    OrderForm.discountCodeCoefficient = 0;
    OrderForm.discountCodeType = 0;
    OrderForm.showOriginalPrice = 0;
    /******************************************************************/

    OrderForm.secondStepImage = "";
    OrderForm.validateAction = '/order.validate';
    OrderForm.isPreview = false;
    OrderForm.isResubmit = false;
    OrderForm.isEdit = 0;
    OrderForm.isCalculator = false;
    OrderForm.orderCode = '';
    OrderForm.isQuote = false;
    OrderForm.isResumes = false;
    OrderForm.isPaypal = false;
    OrderForm.nonWordsProducts = {
        "124": 124,
        "125": 125,
        "126": 126,
        "182": 182,
        "139": 139,
        "222": 222,
        "51": 51,
        "234": 234,
        "235": 235,
        "260": 260,
        "261": 261,
        "262": 262
    };
    OrderForm.featurePrices = new Array();
    OrderForm.featureDiscountable = new Array();
    OrderForm.coverLetterId = 50;
    OrderForm.withCoverLetterIds = {
        "34": 34,
        "50": 50,
        "133": 133,
        "137": 137,
        "138": 138
    };
    OrderForm.values.o_interval = {
        value: '0'
    };
    OrderForm.featurePrices[147] = function() {

        condition_free = [];
        if (OrderForm.values.doctype) {
            doctype_id = OrderForm.values.doctype.value;
        } else {
            doctype_id = OrderForm.doctype;
        }
        urgency_id = OrderForm.values.urgency.value;
        wrlevel_id = (OrderForm.values.wrlevel ? OrderForm.values.wrlevel.value : 0);
        free_from_link = false;

        if ((condition_free && condition_free[doctype_id] && condition_free[doctype_id][urgency_id] && condition_free[doctype_id][urgency_id][wrlevel_id]) || free_from_link) {
            return 0;
        } else {
            return 9.99;
        }
    };
    OrderForm.featureDiscountable[147] = false;
    OrderForm.featurePrices[151] = function() {
        feature_fields = {
            "0": ["152", "153"],
            "1": ["152", "153"],
            "3": ["152", "153"],
            "13": ["152", "153"],
            "14": ["152", "153"],
            "15": ["152", "153"],
            "37": ["152", "153"],
            "38": ["152", "153"],
            "39": ["152", "153"],
            "40": ["152", "153"],
            "51": ["152", "153"],
            "80": ["152", "153"],
            "81": ["152", "153"],
            "82": ["147", "153"],
            "83": ["152", "153"],
            "84": ["152", "153"],
            "85": ["152", "153"],
            "124": ["147", "153"],
            "125": ["152", "153"],
            "126": ["152", "153"],
            "134": ["152", "153"],
            "135": ["152", "153"],
            "136": ["152", "153"],
            "142": ["152", "153"],
            "143": ["152", "153"],
            "144": ["152", "153"],
            "145": ["152", "153"],
            "146": ["152", "153"],
            "147": ["152", "153"],
            "148": ["152", "153"],
            "149": ["152", "153"],
            "150": ["152", "153"],
            "151": ["152", "153"],
            "152": ["152", "153"],
            "153": ["152", "153"],
            "154": ["152", "153"],
            "156": ["152", "153"],
            "157": ["152", "153"],
            "158": ["147", "153"],
            "159": ["147", "153"],
            "163": ["152", "153"],
            "168": ["152", "153"],
            "169": ["152", "153"],
            "170": ["152", "153"],
            "171": ["152", "153"],
            "172": ["152", "153"],
            "173": ["152", "153"],
            "174": ["152", "153"],
            "235": ["152", "153"],
            "242": ["152", "153"]
        };
        fields = feature_fields[OrderForm.doctype];
        result = 0;
        free_from_link = false;

        if (free_from_link) {
            return 0;
        }

        for (a in fields) {
            result += OrderForm.featurePrices[fields[a]](fields[a], 1);
        }
        return result * 0.6;
    };
    OrderForm.featureDiscountable[151] = false;
    OrderForm.featurePrices[153] = function() {
        condition_free = [];
        doctype_id = OrderForm.values.doctype.value;
        urgency_id = OrderForm.values.urgency.value;
        wrlevel_id = (OrderForm.values.wrlevel ? OrderForm.values.wrlevel.value : 0);
        currency_rate = OrderForm.currencyRates.USD[OrderForm.values.curr.value];
        free_from_link = false;
        free_over_order_total = parseFloat('0');
        if ((free_over_order_total > 0) && OrderForm.total_without_discount && OrderForm.total_without_discount > (free_over_order_total * currency_rate)) {
            return 0;
        }

        if ((condition_free && condition_free[doctype_id] && condition_free[doctype_id][urgency_id] && condition_free[doctype_id][urgency_id][wrlevel_id]) || free_from_link) {
            return 0;
        } else {
            return Math.max(1, parseInt($('#numpages').val())) * 5.99;
        }
    };
    OrderForm.featureDiscountable[153] = false;
    OrderForm.featurePrices[152] = function() {
        condition_free = [];
        doctype_id = OrderForm.values.doctype.value;
        urgency_id = OrderForm.values.urgency.value;
        wrlevel_id = (OrderForm.values.wrlevel ? OrderForm.values.wrlevel.value : 0);
        free_from_link = false;

        if ((condition_free && condition_free[doctype_id] && condition_free[doctype_id][urgency_id] && condition_free[doctype_id][urgency_id][wrlevel_id]) || free_from_link) {
            return 0;
        } else {
            currency_rate = OrderForm.currencyRates[OrderForm.values.curr.value].USD;
            return OrderForm.cost_per_page *
                Math.max(OrderForm.values.numpages && OrderForm.values.numpages.value ? OrderForm.values.numpages.value : 1, 1) *
                Math.max(OrderForm.values.numpapers && OrderForm.values.numpapers.value ? OrderForm.values.numpapers.value : 1, 1) *
                currency_rate * 25 / 100;
        }
    };
    OrderForm.featureDiscountable[152] = false;
    OrderForm.featurePrices[142] = function() {
        free_from_link = false;
        if (free_from_link) {
            return 0;
        }
        currency_rate = OrderForm.currencyRates[OrderForm.values.curr.value].USD;
        return OrderForm.cost_per_page *
            Math.max(OrderForm.values.numpages && OrderForm.values.numpages.value ? OrderForm.values.numpages.value : 1, 1) *
            Math.max(OrderForm.values.numpapers && OrderForm.values.numpapers.value ? OrderForm.values.numpapers.value : 1, 1) *
            currency_rate * 5 / 100;
    };
    OrderForm.featureDiscountable[142] = false;
    OrderForm.currenciesFormat = {
        "USD": "$%s",
        "EUR": "&euro;%s",
        "GBP": "&pound;%s",
        "AUD": "A$ %s"
    };
    OrderForm.cppDiscountRules = {
        "15": 5,
        "51": 10,
        "101": 15
    };


    OrderForm.nonTechDoctypes = [125, 126, 152, 163, 174, 182, 217, 222];
    OrderForm.techCategories = [65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77];
    OrderForm.version1 = true;
    OrderForm.version1_1 = false;
    OrderForm.country = [{
        "c_id": "93",
        "name": "Afghanistan",
        "id_country": "4"
    }, {
        "c_id": "355",
        "name": "Albania",
        "id_country": "5"
    }, {
        "c_id": "213",
        "name": "Algeria",
        "id_country": "6"
    }, {
        "c_id": "684",
        "name": "American Samoa",
        "id_country": "7"
    }, {
        "c_id": "376",
        "name": "Andorra",
        "id_country": "8"
    }, {
        "c_id": "244",
        "name": "Angola",
        "id_country": "9"
    }, {
        "c_id": "264*",
        "name": "Anguilla",
        "id_country": "10"
    }, {
        "c_id": "672",
        "name": "Antarctica",
        "id_country": "11"
    }, {
        "c_id": "268*",
        "name": "Antigua",
        "id_country": "12"
    }, {
        "c_id": "54",
        "name": "Argentina",
        "id_country": "13"
    }, {
        "c_id": "374",
        "name": "Armenia",
        "id_country": "14"
    }, {
        "c_id": "297",
        "name": "Aruba",
        "id_country": "15"
    }, {
        "c_id": "247",
        "name": "Ascension Island",
        "id_country": "16"
    }, {
        "c_id": "61",
        "name": "Australia",
        "id_country": "17"
    }, {
        "c_id": "43",
        "name": "Austria",
        "id_country": "18"
    }, {
        "c_id": "994",
        "name": "Azberbaijan",
        "id_country": "19"
    }, {
        "c_id": "242*",
        "name": "Bahamas",
        "id_country": "20"
    }, {
        "c_id": "973",
        "name": "Bahrain",
        "id_country": "21"
    }, {
        "c_id": "880",
        "name": "Bangladesh",
        "id_country": "22"
    }, {
        "c_id": "246*",
        "name": "Barbados",
        "id_country": "23"
    }, {
        "c_id": "268*",
        "name": "Barbuda",
        "id_country": "24"
    }, {
        "c_id": "375",
        "name": "Belarus",
        "id_country": "25"
    }, {
        "c_id": "32",
        "name": "Belgium",
        "id_country": "26"
    }, {
        "c_id": "501",
        "name": "Belize",
        "id_country": "27"
    }, {
        "c_id": "229",
        "name": "Benin",
        "id_country": "28"
    }, {
        "c_id": "441*",
        "name": "Bermuda",
        "id_country": "29"
    }, {
        "c_id": "975",
        "name": "Bhutan",
        "id_country": "30"
    }, {
        "c_id": "591",
        "name": "Bolivia",
        "id_country": "31"
    }, {
        "c_id": "387",
        "name": "Bosnia",
        "id_country": "32"
    }, {
        "c_id": "267",
        "name": "Botswana",
        "id_country": "33"
    }, {
        "c_id": "55",
        "name": "Brazil",
        "id_country": "34"
    }, {
        "c_id": "284*",
        "name": "British Virgin Islands",
        "id_country": "35"
    }, {
        "c_id": "673",
        "name": "Brunei",
        "id_country": "36"
    }, {
        "c_id": "359",
        "name": "Bulgaria",
        "id_country": "37"
    }, {
        "c_id": "226",
        "name": "Burkina Faso",
        "id_country": "38"
    }, {
        "c_id": "257",
        "name": "Burundi",
        "id_country": "40"
    }, {
        "c_id": "855",
        "name": "Cambodia",
        "id_country": "41"
    }, {
        "c_id": "237",
        "name": "Cameroon",
        "id_country": "42"
    }, {
        "c_id": "1",
        "name": "Canada",
        "id_country": "240"
    }, {
        "c_id": "238",
        "name": "Cape Verde Islands",
        "id_country": "44"
    }, {
        "c_id": "345*",
        "name": "Cayman Islands",
        "id_country": "45"
    }, {
        "c_id": "236",
        "name": "Central African Rep.",
        "id_country": "46"
    }, {
        "c_id": "235",
        "name": "Chad",
        "id_country": "47"
    }, {
        "c_id": "56",
        "name": "Chile",
        "id_country": "48"
    }, {
        "c_id": "86",
        "name": "China",
        "id_country": "49"
    }, {
        "c_id": "61",
        "name": "Christmas Island",
        "id_country": "50"
    }, {
        "c_id": "61",
        "name": "Cocos Islands",
        "id_country": "51"
    }, {
        "c_id": "57",
        "name": "Colombia",
        "id_country": "52"
    }, {
        "c_id": "269",
        "name": "Comoros",
        "id_country": "53"
    }, {
        "c_id": "242",
        "name": "Congo",
        "id_country": "54"
    }, {
        "c_id": "243",
        "name": "Congo, Dem. Rep. of",
        "id_country": "55"
    }, {
        "c_id": "682",
        "name": "Cook Islands",
        "id_country": "56"
    }, {
        "c_id": "506",
        "name": "Costa Rica",
        "id_country": "57"
    }, {
        "c_id": "385",
        "name": "Croatia",
        "id_country": "58"
    }, {
        "c_id": "53",
        "name": "Cuba",
        "id_country": "59"
    }, {
        "c_id": "357",
        "name": "Cyprus",
        "id_country": "60"
    }, {
        "c_id": "420",
        "name": "Czech Republic",
        "id_country": "61"
    }, {
        "c_id": "45",
        "name": "Denmark",
        "id_country": "62"
    }, {
        "c_id": "246",
        "name": "Diego Garcia",
        "id_country": "63"
    }, {
        "c_id": "253",
        "name": "Djibouti",
        "id_country": "64"
    }, {
        "c_id": "767*",
        "name": "Dominica",
        "id_country": "65"
    }, {
        "c_id": "809*",
        "name": "Dominican Rep.",
        "id_country": "66"
    }, {
        "c_id": "593",
        "name": "Ecuador",
        "id_country": "67"
    }, {
        "c_id": "20",
        "name": "Egypt",
        "id_country": "68"
    }, {
        "c_id": "503",
        "name": "El Salvador",
        "id_country": "69"
    }, {
        "c_id": "240",
        "name": "Equatorial Guinea",
        "id_country": "70"
    }, {
        "c_id": "291",
        "name": "Eritrea",
        "id_country": "71"
    }, {
        "c_id": "372",
        "name": "Estonia",
        "id_country": "72"
    }, {
        "c_id": "251",
        "name": "Ethiopia",
        "id_country": "73"
    }, {
        "c_id": "298",
        "name": "Faeroe Islands",
        "id_country": "74"
    }, {
        "c_id": "500",
        "name": "Falkland Islands",
        "id_country": "75"
    }, {
        "c_id": "679",
        "name": "Fiji Islands",
        "id_country": "76"
    }, {
        "c_id": "358",
        "name": "Finland",
        "id_country": "77"
    }, {
        "c_id": "33",
        "name": "France",
        "id_country": "78"
    }, {
        "c_id": "594",
        "name": "French Guiana",
        "id_country": "80"
    }, {
        "c_id": "689",
        "name": "French Polynesia",
        "id_country": "81"
    }, {
        "c_id": "241",
        "name": "Gabon",
        "id_country": "82"
    }, {
        "c_id": "220",
        "name": "Gambia",
        "id_country": "83"
    }, {
        "c_id": "995",
        "name": "Georgia",
        "id_country": "84"
    }, {
        "c_id": "49",
        "name": "Germany",
        "id_country": "85"
    }, {
        "c_id": "233",
        "name": "Ghana",
        "id_country": "86"
    }, {
        "c_id": "350",
        "name": "Gibraltar",
        "id_country": "87"
    }, {
        "c_id": "30",
        "name": "Greece",
        "id_country": "88"
    }, {
        "c_id": "299",
        "name": "Greenland",
        "id_country": "89"
    }, {
        "c_id": "473*",
        "name": "Grenada",
        "id_country": "90"
    }, {
        "c_id": "590",
        "name": "Guadeloupe",
        "id_country": "91"
    }, {
        "c_id": "671",
        "name": "Guam",
        "id_country": "92"
    }, {
        "c_id": "502",
        "name": "Guatemala",
        "id_country": "94"
    }, {
        "c_id": "224",
        "name": "Guinea",
        "id_country": "95"
    }, {
        "c_id": "245",
        "name": "Guinea Bissau",
        "id_country": "96"
    }, {
        "c_id": "592",
        "name": "Guyana",
        "id_country": "97"
    }, {
        "c_id": "509",
        "name": "Haiti",
        "id_country": "98"
    }, {
        "c_id": "504",
        "name": "Honduras",
        "id_country": "99"
    }, {
        "c_id": "852",
        "name": "Hong Kong",
        "id_country": "100"
    }, {
        "c_id": "36",
        "name": "Hungary",
        "id_country": "101"
    }, {
        "c_id": "354",
        "name": "Iceland",
        "id_country": "102"
    }, {
        "c_id": "91",
        "name": "India",
        "id_country": "103"
    }, {
        "c_id": "62",
        "name": "Indonesia",
        "id_country": "104"
    }, {
        "c_id": "98",
        "name": "Iran",
        "id_country": "105"
    }, {
        "c_id": "964",
        "name": "Iraq",
        "id_country": "106"
    }, {
        "c_id": "353",
        "name": "Ireland",
        "id_country": "107"
    }, {
        "c_id": "972",
        "name": "Israel",
        "id_country": "108"
    }, {
        "c_id": "39",
        "name": "Italy",
        "id_country": "109"
    }, {
        "c_id": "876*",
        "name": "Jamaica",
        "id_country": "111"
    }, {
        "c_id": "81",
        "name": "Japan",
        "id_country": "112"
    }, {
        "c_id": "962",
        "name": "Jordan",
        "id_country": "113"
    }, {
        "c_id": "7",
        "name": "Kazakhstan",
        "id_country": "114"
    }, {
        "c_id": "254",
        "name": "Kenya",
        "id_country": "115"
    }, {
        "c_id": "686",
        "name": "Kiribati",
        "id_country": "116"
    }, {
        "c_id": "850",
        "name": "Korea, North",
        "id_country": "117"
    }, {
        "c_id": "82",
        "name": "Korea, South",
        "id_country": "118"
    }, {
        "c_id": "965",
        "name": "Kuwait",
        "id_country": "119"
    }, {
        "c_id": "996",
        "name": "Kyrgyzstan",
        "id_country": "120"
    }, {
        "c_id": "856",
        "name": "Laos",
        "id_country": "121"
    }, {
        "c_id": "371",
        "name": "Latvia",
        "id_country": "122"
    }, {
        "c_id": "961",
        "name": "Lebanon",
        "id_country": "123"
    }, {
        "c_id": "266",
        "name": "Lesotho",
        "id_country": "124"
    }, {
        "c_id": "231",
        "name": "Liberia",
        "id_country": "125"
    }, {
        "c_id": "218",
        "name": "Libya",
        "id_country": "126"
    }, {
        "c_id": "423",
        "name": "Liechtenstein",
        "id_country": "127"
    }, {
        "c_id": "370",
        "name": "Lithuania",
        "id_country": "128"
    }, {
        "c_id": "352",
        "name": "Luxembourg",
        "id_country": "129"
    }, {
        "c_id": "853",
        "name": "Macau",
        "id_country": "130"
    }, {
        "c_id": "389",
        "name": "Macedonia",
        "id_country": "131"
    }, {
        "c_id": "261",
        "name": "Madagascar",
        "id_country": "132"
    }, {
        "c_id": "265",
        "name": "Malawi",
        "id_country": "133"
    }, {
        "c_id": "60",
        "name": "Malaysia",
        "id_country": "134"
    }, {
        "c_id": "960",
        "name": "Maldives",
        "id_country": "135"
    }, {
        "c_id": "223",
        "name": "Mali",
        "id_country": "136"
    }, {
        "c_id": "356",
        "name": "Malta",
        "id_country": "137"
    }, {
        "c_id": "670*",
        "name": "Mariana Islands",
        "id_country": "138"
    }, {
        "c_id": "692",
        "name": "Marshall Islands",
        "id_country": "139"
    }, {
        "c_id": "596",
        "name": "Martinique",
        "id_country": "140"
    }, {
        "c_id": "222",
        "name": "Mauritania",
        "id_country": "141"
    }, {
        "c_id": "230",
        "name": "Mauritius",
        "id_country": "142"
    }, {
        "c_id": "269",
        "name": "Mayotte Islands",
        "id_country": "143"
    }, {
        "c_id": "52",
        "name": "Mexico",
        "id_country": "144"
    }, {
        "c_id": "691",
        "name": "Micronesia",
        "id_country": "145"
    }, {
        "c_id": "373",
        "name": "Moldova",
        "id_country": "147"
    }, {
        "c_id": "377",
        "name": "Monaco",
        "id_country": "148"
    }, {
        "c_id": "976",
        "name": "Mongolia",
        "id_country": "149"
    }, {
        "c_id": "664*",
        "name": "Montserrat",
        "id_country": "150"
    }, {
        "c_id": "212",
        "name": "Morocco",
        "id_country": "151"
    }, {
        "c_id": "258",
        "name": "Mozambique",
        "id_country": "152"
    }, {
        "c_id": "95",
        "name": "Myanmar (Burma)",
        "id_country": "153"
    }, {
        "c_id": "264",
        "name": "Namibia",
        "id_country": "154"
    }, {
        "c_id": "674",
        "name": "Nauru",
        "id_country": "155"
    }, {
        "c_id": "977",
        "name": "Nepal",
        "id_country": "156"
    }, {
        "c_id": "31",
        "name": "Netherlands",
        "id_country": "157"
    }, {
        "c_id": "599",
        "name": "Netherlands Antilles",
        "id_country": "158"
    }, {
        "c_id": "869*",
        "name": "Nevis",
        "id_country": "159"
    }, {
        "c_id": "687",
        "name": "New Caledonia",
        "id_country": "160"
    }, {
        "c_id": "64",
        "name": "New Zealand",
        "id_country": "161"
    }, {
        "c_id": "505",
        "name": "Nicaragua",
        "id_country": "162"
    }, {
        "c_id": "227",
        "name": "Niger",
        "id_country": "163"
    }, {
        "c_id": "234",
        "name": "Nigeria",
        "id_country": "164"
    }, {
        "c_id": "683",
        "name": "Niue",
        "id_country": "165"
    }, {
        "c_id": "672",
        "name": "Norfolk Island",
        "id_country": "166"
    }, {
        "c_id": "47",
        "name": "Norway",
        "id_country": "167"
    }, {
        "c_id": "968",
        "name": "Oman",
        "id_country": "168"
    }, {
        "c_id": "92",
        "name": "Pakistan",
        "id_country": "169"
    }, {
        "c_id": "680",
        "name": "Palau",
        "id_country": "170"
    }, {
        "c_id": "970",
        "name": "Palestine",
        "id_country": "171"
    }, {
        "c_id": "507",
        "name": "Panama",
        "id_country": "172"
    }, {
        "c_id": "675",
        "name": "Papua New Guinea",
        "id_country": "173"
    }, {
        "c_id": "595",
        "name": "Paraguay",
        "id_country": "174"
    }, {
        "c_id": "51",
        "name": "Peru",
        "id_country": "175"
    }, {
        "c_id": "63",
        "name": "Philippines",
        "id_country": "176"
    }, {
        "c_id": "48",
        "name": "Poland",
        "id_country": "177"
    }, {
        "c_id": "351",
        "name": "Portugal",
        "id_country": "178"
    }, {
        "c_id": "787*",
        "name": "Puerto Rico",
        "id_country": "179"
    }, {
        "c_id": "974",
        "name": "Qatar",
        "id_country": "180"
    }, {
        "c_id": "262",
        "name": "Reunion Island",
        "id_country": "181"
    }, {
        "c_id": "40",
        "name": "Romania",
        "id_country": "182"
    }, {
        "c_id": "7",
        "name": "Russia",
        "id_country": "183"
    }, {
        "c_id": "250",
        "name": "Rwanda",
        "id_country": "184"
    }, {
        "c_id": "378",
        "name": "San Marino",
        "id_country": "190"
    }, {
        "c_id": "239",
        "name": "Sao Tome & Principe",
        "id_country": "191"
    }, {
        "c_id": "966",
        "name": "Saudi Arabia",
        "id_country": "192"
    }, {
        "c_id": "221",
        "name": "Senegal",
        "id_country": "193"
    }, {
        "c_id": "381",
        "name": "Serbia",
        "id_country": "194"
    }, {
        "c_id": "248",
        "name": "Seychelles",
        "id_country": "195"
    }, {
        "c_id": "232",
        "name": "Sierra Leone",
        "id_country": "196"
    }, {
        "c_id": "65",
        "name": "Singapore",
        "id_country": "197"
    }, {
        "c_id": "421",
        "name": "Slovakia",
        "id_country": "198"
    }, {
        "c_id": "386",
        "name": "Slovenia",
        "id_country": "199"
    }, {
        "c_id": "677",
        "name": "Solomon Islands",
        "id_country": "200"
    }, {
        "c_id": "252",
        "name": "Somalia",
        "id_country": "201"
    }, {
        "c_id": "27",
        "name": "South Africa",
        "id_country": "202"
    }, {
        "c_id": "34",
        "name": "Spain",
        "id_country": "203"
    }, {
        "c_id": "94",
        "name": "Sri Lanka",
        "id_country": "204"
    }, {
        "c_id": "290",
        "name": "St. Helena",
        "id_country": "185"
    }, {
        "c_id": "869*",
        "name": "St. Kitts",
        "id_country": "186"
    }, {
        "c_id": "758*",
        "name": "St. Lucia",
        "id_country": "187"
    }, {
        "c_id": "508",
        "name": "St. Perre & Miquelon",
        "id_country": "188"
    }, {
        "c_id": "784*",
        "name": "St. Vincent",
        "id_country": "189"
    }, {
        "c_id": "249",
        "name": "Sudan",
        "id_country": "205"
    }, {
        "c_id": "597",
        "name": "Suriname",
        "id_country": "206"
    }, {
        "c_id": "268",
        "name": "Swaziland",
        "id_country": "207"
    }, {
        "c_id": "46",
        "name": "Sweden",
        "id_country": "208"
    }, {
        "c_id": "41",
        "name": "Switzerland",
        "id_country": "209"
    }, {
        "c_id": "963",
        "name": "Syria",
        "id_country": "210"
    }, {
        "c_id": "886",
        "name": "Taiwan",
        "id_country": "211"
    }, {
        "c_id": "992",
        "name": "Tajikistan",
        "id_country": "212"
    }, {
        "c_id": "255",
        "name": "Tanzania",
        "id_country": "213"
    }, {
        "c_id": "66",
        "name": "Thailand",
        "id_country": "214"
    }, {
        "c_id": "228",
        "name": "Togo",
        "id_country": "215"
    }, {
        "c_id": "676",
        "name": "Tonga",
        "id_country": "216"
    }, {
        "c_id": "868*",
        "name": "Trinidad & Tobago",
        "id_country": "217"
    }, {
        "c_id": "216",
        "name": "Tunisia",
        "id_country": "218"
    }, {
        "c_id": "90",
        "name": "Turkey",
        "id_country": "219"
    }, {
        "c_id": "993",
        "name": "Turkmenistan",
        "id_country": "220"
    }, {
        "c_id": "649*",
        "name": "Turks & Caicos",
        "id_country": "221"
    }, {
        "c_id": "688",
        "name": "Tuvalu",
        "id_country": "222"
    }, {
        "c_id": "256",
        "name": "Uganda",
        "id_country": "223"
    }, {
        "c_id": "380",
        "name": "Ukraine",
        "id_country": "224"
    }, {
        "c_id": "971",
        "name": "United Arab Emirates",
        "id_country": "225"
    }, {
        "c_id": "44",
        "name": "United Kingdom",
        "id_country": "226"
    }, {
        "c_id": "598",
        "name": "Uruguay",
        "id_country": "227"
    }, {
        "c_id": "1",
        "name": "USA",
        "id_country": "3"
    }, {
        "c_id": "998",
        "name": "Uzbekistan",
        "id_country": "228"
    }, {
        "c_id": "678",
        "name": "Vanuatu",
        "id_country": "229"
    }, {
        "c_id": "39",
        "name": "Vatican City",
        "id_country": "230"
    }, {
        "c_id": "58",
        "name": "Venezuela",
        "id_country": "231"
    }, {
        "c_id": "84",
        "name": "Vietnam",
        "id_country": "232"
    }, {
        "c_id": "681",
        "name": "Wallis & Futuna",
        "id_country": "234"
    }, {
        "c_id": "685",
        "name": "Western Samoa",
        "id_country": "235"
    }, {
        "c_id": "967",
        "name": "Yemen",
        "id_country": "236"
    }, {
        "c_id": "381",
        "name": "Yugoslavia",
        "id_country": "237"
    }, {
        "c_id": "260",
        "name": "Zambia",
        "id_country": "238"
    }, {
        "c_id": "263",
        "name": "Zimbabwe",
        "id_country": "239"
    }];

    OrderForm.country = [{
        "c_id": "93",
        "name": "Afghanistan",
        "id_country": "4"
    }, {
        "c_id": "355",
        "name": "Albania",
        "id_country": "5"
    }, {
        "c_id": "213",
        "name": "Algeria",
        "id_country": "6"
    }, {
        "c_id": "684",
        "name": "American Samoa",
        "id_country": "7"
    }, {
        "c_id": "376",
        "name": "Andorra",
        "id_country": "8"
    }, {
        "c_id": "244",
        "name": "Angola",
        "id_country": "9"
    }, {
        "c_id": "264*",
        "name": "Anguilla",
        "id_country": "10"
    }, {
        "c_id": "672",
        "name": "Antarctica",
        "id_country": "11"
    }, {
        "c_id": "268*",
        "name": "Antigua",
        "id_country": "12"
    }, {
        "c_id": "54",
        "name": "Argentina",
        "id_country": "13"
    }, {
        "c_id": "374",
        "name": "Armenia",
        "id_country": "14"
    }, {
        "c_id": "297",
        "name": "Aruba",
        "id_country": "15"
    }, {
        "c_id": "247",
        "name": "Ascension Island",
        "id_country": "16"
    }, {
        "c_id": "61",
        "name": "Australia",
        "id_country": "17"
    }, {
        "c_id": "43",
        "name": "Austria",
        "id_country": "18"
    }, {
        "c_id": "994",
        "name": "Azberbaijan",
        "id_country": "19"
    }, {
        "c_id": "242*",
        "name": "Bahamas",
        "id_country": "20"
    }, {
        "c_id": "973",
        "name": "Bahrain",
        "id_country": "21"
    }, {
        "c_id": "880",
        "name": "Bangladesh",
        "id_country": "22"
    }, {
        "c_id": "246*",
        "name": "Barbados",
        "id_country": "23"
    }, {
        "c_id": "268*",
        "name": "Barbuda",
        "id_country": "24"
    }, {
        "c_id": "375",
        "name": "Belarus",
        "id_country": "25"
    }, {
        "c_id": "32",
        "name": "Belgium",
        "id_country": "26"
    }, {
        "c_id": "501",
        "name": "Belize",
        "id_country": "27"
    }, {
        "c_id": "229",
        "name": "Benin",
        "id_country": "28"
    }, {
        "c_id": "441*",
        "name": "Bermuda",
        "id_country": "29"
    }, {
        "c_id": "975",
        "name": "Bhutan",
        "id_country": "30"
    }, {
        "c_id": "591",
        "name": "Bolivia",
        "id_country": "31"
    }, {
        "c_id": "387",
        "name": "Bosnia",
        "id_country": "32"
    }, {
        "c_id": "267",
        "name": "Botswana",
        "id_country": "33"
    }, {
        "c_id": "55",
        "name": "Brazil",
        "id_country": "34"
    }, {
        "c_id": "284*",
        "name": "British Virgin Islands",
        "id_country": "35"
    }, {
        "c_id": "673",
        "name": "Brunei",
        "id_country": "36"
    }, {
        "c_id": "359",
        "name": "Bulgaria",
        "id_country": "37"
    }, {
        "c_id": "226",
        "name": "Burkina Faso",
        "id_country": "38"
    }, {
        "c_id": "257",
        "name": "Burundi",
        "id_country": "40"
    }, {
        "c_id": "855",
        "name": "Cambodia",
        "id_country": "41"
    }, {
        "c_id": "237",
        "name": "Cameroon",
        "id_country": "42"
    }, {
        "c_id": "1",
        "name": "Canada",
        "id_country": "240"
    }, {
        "c_id": "238",
        "name": "Cape Verde Islands",
        "id_country": "44"
    }, {
        "c_id": "345*",
        "name": "Cayman Islands",
        "id_country": "45"
    }, {
        "c_id": "236",
        "name": "Central African Rep.",
        "id_country": "46"
    }, {
        "c_id": "235",
        "name": "Chad",
        "id_country": "47"
    }, {
        "c_id": "56",
        "name": "Chile",
        "id_country": "48"
    }, {
        "c_id": "86",
        "name": "China",
        "id_country": "49"
    }, {
        "c_id": "61",
        "name": "Christmas Island",
        "id_country": "50"
    }, {
        "c_id": "61",
        "name": "Cocos Islands",
        "id_country": "51"
    }, {
        "c_id": "57",
        "name": "Colombia",
        "id_country": "52"
    }, {
        "c_id": "269",
        "name": "Comoros",
        "id_country": "53"
    }, {
        "c_id": "242",
        "name": "Congo",
        "id_country": "54"
    }, {
        "c_id": "243",
        "name": "Congo, Dem. Rep. of",
        "id_country": "55"
    }, {
        "c_id": "682",
        "name": "Cook Islands",
        "id_country": "56"
    }, {
        "c_id": "506",
        "name": "Costa Rica",
        "id_country": "57"
    }, {
        "c_id": "385",
        "name": "Croatia",
        "id_country": "58"
    }, {
        "c_id": "53",
        "name": "Cuba",
        "id_country": "59"
    }, {
        "c_id": "357",
        "name": "Cyprus",
        "id_country": "60"
    }, {
        "c_id": "420",
        "name": "Czech Republic",
        "id_country": "61"
    }, {
        "c_id": "45",
        "name": "Denmark",
        "id_country": "62"
    }, {
        "c_id": "246",
        "name": "Diego Garcia",
        "id_country": "63"
    }, {
        "c_id": "253",
        "name": "Djibouti",
        "id_country": "64"
    }, {
        "c_id": "767*",
        "name": "Dominica",
        "id_country": "65"
    }, {
        "c_id": "809*",
        "name": "Dominican Rep.",
        "id_country": "66"
    }, {
        "c_id": "593",
        "name": "Ecuador",
        "id_country": "67"
    }, {
        "c_id": "20",
        "name": "Egypt",
        "id_country": "68"
    }, {
        "c_id": "503",
        "name": "El Salvador",
        "id_country": "69"
    }, {
        "c_id": "240",
        "name": "Equatorial Guinea",
        "id_country": "70"
    }, {
        "c_id": "291",
        "name": "Eritrea",
        "id_country": "71"
    }, {
        "c_id": "372",
        "name": "Estonia",
        "id_country": "72"
    }, {
        "c_id": "251",
        "name": "Ethiopia",
        "id_country": "73"
    }, {
        "c_id": "298",
        "name": "Faeroe Islands",
        "id_country": "74"
    }, {
        "c_id": "500",
        "name": "Falkland Islands",
        "id_country": "75"
    }, {
        "c_id": "679",
        "name": "Fiji Islands",
        "id_country": "76"
    }, {
        "c_id": "358",
        "name": "Finland",
        "id_country": "77"
    }, {
        "c_id": "33",
        "name": "France",
        "id_country": "78"
    }, {
        "c_id": "594",
        "name": "French Guiana",
        "id_country": "80"
    }, {
        "c_id": "689",
        "name": "French Polynesia",
        "id_country": "81"
    }, {
        "c_id": "241",
        "name": "Gabon",
        "id_country": "82"
    }, {
        "c_id": "220",
        "name": "Gambia",
        "id_country": "83"
    }, {
        "c_id": "995",
        "name": "Georgia",
        "id_country": "84"
    }, {
        "c_id": "49",
        "name": "Germany",
        "id_country": "85"
    }, {
        "c_id": "233",
        "name": "Ghana",
        "id_country": "86"
    }, {
        "c_id": "350",
        "name": "Gibraltar",
        "id_country": "87"
    }, {
        "c_id": "30",
        "name": "Greece",
        "id_country": "88"
    }, {
        "c_id": "299",
        "name": "Greenland",
        "id_country": "89"
    }, {
        "c_id": "473*",
        "name": "Grenada",
        "id_country": "90"
    }, {
        "c_id": "590",
        "name": "Guadeloupe",
        "id_country": "91"
    }, {
        "c_id": "671",
        "name": "Guam",
        "id_country": "92"
    }, {
        "c_id": "502",
        "name": "Guatemala",
        "id_country": "94"
    }, {
        "c_id": "224",
        "name": "Guinea",
        "id_country": "95"
    }, {
        "c_id": "245",
        "name": "Guinea Bissau",
        "id_country": "96"
    }, {
        "c_id": "592",
        "name": "Guyana",
        "id_country": "97"
    }, {
        "c_id": "509",
        "name": "Haiti",
        "id_country": "98"
    }, {
        "c_id": "504",
        "name": "Honduras",
        "id_country": "99"
    }, {
        "c_id": "852",
        "name": "Hong Kong",
        "id_country": "100"
    }, {
        "c_id": "36",
        "name": "Hungary",
        "id_country": "101"
    }, {
        "c_id": "354",
        "name": "Iceland",
        "id_country": "102"
    }, {
        "c_id": "91",
        "name": "India",
        "id_country": "103"
    }, {
        "c_id": "62",
        "name": "Indonesia",
        "id_country": "104"
    }, {
        "c_id": "98",
        "name": "Iran",
        "id_country": "105"
    }, {
        "c_id": "964",
        "name": "Iraq",
        "id_country": "106"
    }, {
        "c_id": "353",
        "name": "Ireland",
        "id_country": "107"
    }, {
        "c_id": "972",
        "name": "Israel",
        "id_country": "108"
    }, {
        "c_id": "39",
        "name": "Italy",
        "id_country": "109"
    }, {
        "c_id": "876*",
        "name": "Jamaica",
        "id_country": "111"
    }, {
        "c_id": "81",
        "name": "Japan",
        "id_country": "112"
    }, {
        "c_id": "962",
        "name": "Jordan",
        "id_country": "113"
    }, {
        "c_id": "7",
        "name": "Kazakhstan",
        "id_country": "114"
    }, {
        "c_id": "254",
        "name": "Kenya",
        "id_country": "115"
    }, {
        "c_id": "686",
        "name": "Kiribati",
        "id_country": "116"
    }, {
        "c_id": "850",
        "name": "Korea, North",
        "id_country": "117"
    }, {
        "c_id": "82",
        "name": "Korea, South",
        "id_country": "118"
    }, {
        "c_id": "965",
        "name": "Kuwait",
        "id_country": "119"
    }, {
        "c_id": "996",
        "name": "Kyrgyzstan",
        "id_country": "120"
    }, {
        "c_id": "856",
        "name": "Laos",
        "id_country": "121"
    }, {
        "c_id": "371",
        "name": "Latvia",
        "id_country": "122"
    }, {
        "c_id": "961",
        "name": "Lebanon",
        "id_country": "123"
    }, {
        "c_id": "266",
        "name": "Lesotho",
        "id_country": "124"
    }, {
        "c_id": "231",
        "name": "Liberia",
        "id_country": "125"
    }, {
        "c_id": "218",
        "name": "Libya",
        "id_country": "126"
    }, {
        "c_id": "423",
        "name": "Liechtenstein",
        "id_country": "127"
    }, {
        "c_id": "370",
        "name": "Lithuania",
        "id_country": "128"
    }, {
        "c_id": "352",
        "name": "Luxembourg",
        "id_country": "129"
    }, {
        "c_id": "853",
        "name": "Macau",
        "id_country": "130"
    }, {
        "c_id": "389",
        "name": "Macedonia",
        "id_country": "131"
    }, {
        "c_id": "261",
        "name": "Madagascar",
        "id_country": "132"
    }, {
        "c_id": "265",
        "name": "Malawi",
        "id_country": "133"
    }, {
        "c_id": "60",
        "name": "Malaysia",
        "id_country": "134"
    }, {
        "c_id": "960",
        "name": "Maldives",
        "id_country": "135"
    }, {
        "c_id": "223",
        "name": "Mali",
        "id_country": "136"
    }, {
        "c_id": "356",
        "name": "Malta",
        "id_country": "137"
    }, {
        "c_id": "670*",
        "name": "Mariana Islands",
        "id_country": "138"
    }, {
        "c_id": "692",
        "name": "Marshall Islands",
        "id_country": "139"
    }, {
        "c_id": "596",
        "name": "Martinique",
        "id_country": "140"
    }, {
        "c_id": "222",
        "name": "Mauritania",
        "id_country": "141"
    }, {
        "c_id": "230",
        "name": "Mauritius",
        "id_country": "142"
    }, {
        "c_id": "269",
        "name": "Mayotte Islands",
        "id_country": "143"
    }, {
        "c_id": "52",
        "name": "Mexico",
        "id_country": "144"
    }, {
        "c_id": "691",
        "name": "Micronesia",
        "id_country": "145"
    }, {
        "c_id": "373",
        "name": "Moldova",
        "id_country": "147"
    }, {
        "c_id": "377",
        "name": "Monaco",
        "id_country": "148"
    }, {
        "c_id": "976",
        "name": "Mongolia",
        "id_country": "149"
    }, {
        "c_id": "664*",
        "name": "Montserrat",
        "id_country": "150"
    }, {
        "c_id": "212",
        "name": "Morocco",
        "id_country": "151"
    }, {
        "c_id": "258",
        "name": "Mozambique",
        "id_country": "152"
    }, {
        "c_id": "95",
        "name": "Myanmar (Burma)",
        "id_country": "153"
    }, {
        "c_id": "264",
        "name": "Namibia",
        "id_country": "154"
    }, {
        "c_id": "674",
        "name": "Nauru",
        "id_country": "155"
    }, {
        "c_id": "977",
        "name": "Nepal",
        "id_country": "156"
    }, {
        "c_id": "31",
        "name": "Netherlands",
        "id_country": "157"
    }, {
        "c_id": "599",
        "name": "Netherlands Antilles",
        "id_country": "158"
    }, {
        "c_id": "869*",
        "name": "Nevis",
        "id_country": "159"
    }, {
        "c_id": "687",
        "name": "New Caledonia",
        "id_country": "160"
    }, {
        "c_id": "64",
        "name": "New Zealand",
        "id_country": "161"
    }, {
        "c_id": "505",
        "name": "Nicaragua",
        "id_country": "162"
    }, {
        "c_id": "227",
        "name": "Niger",
        "id_country": "163"
    }, {
        "c_id": "234",
        "name": "Nigeria",
        "id_country": "164"
    }, {
        "c_id": "683",
        "name": "Niue",
        "id_country": "165"
    }, {
        "c_id": "672",
        "name": "Norfolk Island",
        "id_country": "166"
    }, {
        "c_id": "47",
        "name": "Norway",
        "id_country": "167"
    }, {
        "c_id": "968",
        "name": "Oman",
        "id_country": "168"
    }, {
        "c_id": "92",
        "name": "Pakistan",
        "id_country": "169"
    }, {
        "c_id": "680",
        "name": "Palau",
        "id_country": "170"
    }, {
        "c_id": "970",
        "name": "Palestine",
        "id_country": "171"
    }, {
        "c_id": "507",
        "name": "Panama",
        "id_country": "172"
    }, {
        "c_id": "675",
        "name": "Papua New Guinea",
        "id_country": "173"
    }, {
        "c_id": "595",
        "name": "Paraguay",
        "id_country": "174"
    }, {
        "c_id": "51",
        "name": "Peru",
        "id_country": "175"
    }, {
        "c_id": "63",
        "name": "Philippines",
        "id_country": "176"
    }, {
        "c_id": "48",
        "name": "Poland",
        "id_country": "177"
    }, {
        "c_id": "351",
        "name": "Portugal",
        "id_country": "178"
    }, {
        "c_id": "787*",
        "name": "Puerto Rico",
        "id_country": "179"
    }, {
        "c_id": "974",
        "name": "Qatar",
        "id_country": "180"
    }, {
        "c_id": "262",
        "name": "Reunion Island",
        "id_country": "181"
    }, {
        "c_id": "40",
        "name": "Romania",
        "id_country": "182"
    }, {
        "c_id": "7",
        "name": "Russia",
        "id_country": "183"
    }, {
        "c_id": "250",
        "name": "Rwanda",
        "id_country": "184"
    }, {
        "c_id": "378",
        "name": "San Marino",
        "id_country": "190"
    }, {
        "c_id": "239",
        "name": "Sao Tome & Principe",
        "id_country": "191"
    }, {
        "c_id": "966",
        "name": "Saudi Arabia",
        "id_country": "192"
    }, {
        "c_id": "221",
        "name": "Senegal",
        "id_country": "193"
    }, {
        "c_id": "381",
        "name": "Serbia",
        "id_country": "194"
    }, {
        "c_id": "248",
        "name": "Seychelles",
        "id_country": "195"
    }, {
        "c_id": "232",
        "name": "Sierra Leone",
        "id_country": "196"
    }, {
        "c_id": "65",
        "name": "Singapore",
        "id_country": "197"
    }, {
        "c_id": "421",
        "name": "Slovakia",
        "id_country": "198"
    }, {
        "c_id": "386",
        "name": "Slovenia",
        "id_country": "199"
    }, {
        "c_id": "677",
        "name": "Solomon Islands",
        "id_country": "200"
    }, {
        "c_id": "252",
        "name": "Somalia",
        "id_country": "201"
    }, {
        "c_id": "27",
        "name": "South Africa",
        "id_country": "202"
    }, {
        "c_id": "34",
        "name": "Spain",
        "id_country": "203"
    }, {
        "c_id": "94",
        "name": "Sri Lanka",
        "id_country": "204"
    }, {
        "c_id": "290",
        "name": "St. Helena",
        "id_country": "185"
    }, {
        "c_id": "869*",
        "name": "St. Kitts",
        "id_country": "186"
    }, {
        "c_id": "758*",
        "name": "St. Lucia",
        "id_country": "187"
    }, {
        "c_id": "508",
        "name": "St. Perre & Miquelon",
        "id_country": "188"
    }, {
        "c_id": "784*",
        "name": "St. Vincent",
        "id_country": "189"
    }, {
        "c_id": "249",
        "name": "Sudan",
        "id_country": "205"
    }, {
        "c_id": "597",
        "name": "Suriname",
        "id_country": "206"
    }, {
        "c_id": "268",
        "name": "Swaziland",
        "id_country": "207"
    }, {
        "c_id": "46",
        "name": "Sweden",
        "id_country": "208"
    }, {
        "c_id": "41",
        "name": "Switzerland",
        "id_country": "209"
    }, {
        "c_id": "963",
        "name": "Syria",
        "id_country": "210"
    }, {
        "c_id": "886",
        "name": "Taiwan",
        "id_country": "211"
    }, {
        "c_id": "992",
        "name": "Tajikistan",
        "id_country": "212"
    }, {
        "c_id": "255",
        "name": "Tanzania",
        "id_country": "213"
    }, {
        "c_id": "66",
        "name": "Thailand",
        "id_country": "214"
    }, {
        "c_id": "228",
        "name": "Togo",
        "id_country": "215"
    }, {
        "c_id": "676",
        "name": "Tonga",
        "id_country": "216"
    }, {
        "c_id": "868*",
        "name": "Trinidad & Tobago",
        "id_country": "217"
    }, {
        "c_id": "216",
        "name": "Tunisia",
        "id_country": "218"
    }, {
        "c_id": "90",
        "name": "Turkey",
        "id_country": "219"
    }, {
        "c_id": "993",
        "name": "Turkmenistan",
        "id_country": "220"
    }, {
        "c_id": "649*",
        "name": "Turks & Caicos",
        "id_country": "221"
    }, {
        "c_id": "688",
        "name": "Tuvalu",
        "id_country": "222"
    }, {
        "c_id": "256",
        "name": "Uganda",
        "id_country": "223"
    }, {
        "c_id": "380",
        "name": "Ukraine",
        "id_country": "224"
    }, {
        "c_id": "971",
        "name": "United Arab Emirates",
        "id_country": "225"
    }, {
        "c_id": "44",
        "name": "United Kingdom",
        "id_country": "226"
    }, {
        "c_id": "598",
        "name": "Uruguay",
        "id_country": "227"
    }, {
        "c_id": "1",
        "name": "USA",
        "id_country": "3"
    }, {
        "c_id": "998",
        "name": "Uzbekistan",
        "id_country": "228"
    }, {
        "c_id": "678",
        "name": "Vanuatu",
        "id_country": "229"
    }, {
        "c_id": "39",
        "name": "Vatican City",
        "id_country": "230"
    }, {
        "c_id": "58",
        "name": "Venezuela",
        "id_country": "231"
    }, {
        "c_id": "84",
        "name": "Vietnam",
        "id_country": "232"
    }, {
        "c_id": "681",
        "name": "Wallis & Futuna",
        "id_country": "234"
    }, {
        "c_id": "685",
        "name": "Western Samoa",
        "id_country": "235"
    }, {
        "c_id": "967",
        "name": "Yemen",
        "id_country": "236"
    }, {
        "c_id": "381",
        "name": "Yugoslavia",
        "id_country": "237"
    }, {
        "c_id": "260",
        "name": "Zambia",
        "id_country": "238"
    }, {
        "c_id": "263",
        "name": "Zimbabwe",
        "id_country": "239"
    }];

    OrderForm.validationImgBtn = '';
    OrderForm.validationName = '';

    OrderForm.removePrefWriterImg = '<img class="delete" src="/images/delete16x16.gif" title="Remove preferred writer" alt="x" />';

    OrderForm.BTP_validation = false;
    OrderForm.ForceSubmit = false;
    
    return {
        OrderForm : OrderForm
    }

})
