define(['app/class'],function(classs){

	var config = {
		loginBtnContainerClass : 'login',
		loginBtnClass: 'button',
		loginDropDownSel: '.loginDropDown',
		closeLDDClass: 'closeBtn' // LDD = login dropdown
	}

	var isMobile = ( navigator.userAgent.match(/(iPad|iPhone|iPod|Android)/g) ? true : false );

	function dropDownDelegation (config) {
		document.body.addEventListener('click', function (e) {
			if (isMobile) {
				// @todo: mobile version
			} else {
				if (e.target.className.indexOf(config.loginBtnClass) !== -1 && e.target.parentNode.className.indexOf(config.loginBtnContainerClass) !== -1 ) {
					document.querySelector(config.loginDropDownSel).style.display = 'block';
				}
				if (e.target.className.indexOf(config.closeLDDClass) !== -1) {
					document.querySelector(config.loginDropDownSel).style.display = 'none';
				}
			}
		});
	}

	function init() {
		dropDownDelegation(config);
	}

	return init;
})