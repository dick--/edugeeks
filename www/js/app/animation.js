define(['app/class'],function(classs){
// retardino-motion trigger
	var orderChickCfg = {
		containerSel: '.orderSectionChick',
		wingSel: '.wingie',
		textarea: '.customTextarea',
		animActionClass: 'anim',
		OFwrap : '.orderFormWrap'
	}

	var arrows = {
		containerSel : '.arrowsContainer',
		left1: '.left1',
		left2: '.left2',
		left3: '.left3',
		right1: '.right1',
		right2: '.right2',
		right3: '.right3',
	}

	var configFloating1280 = {
		pageYOffsetMin: 800,
		pageYOffsetMax: 1600,
		floatIngMultipler: 0.4,
	}
	var configFloating1024 = {
		pageYOffsetMin: 800,
		pageYOffsetMax: 1600,
		floatIngMultipler: 0.3,
	}
	var configFloating768 = {
		pageYOffsetMin: 800,
		pageYOffsetMax: 1600,
		floatIngMultipler: 0.25,
	}
	function restart() {
		var wing = document.querySelector(orderChickCfg.containerSel).querySelector(orderChickCfg.wingSel);
		document.querySelector(orderChickCfg.textarea).addEventListener('keypress', function(e) {
			classs.refresh(wing,orderChickCfg.animActionClass);
		}, false);
		document.querySelector(orderChickCfg.OFwrap).addEventListener('click', function(e) {
			classs.refresh(wing,orderChickCfg.animActionClass);
		}, false);
	}

	function floating(obj,offsets,width,arrows) {
		var left1 = document.querySelector(arrows.containerSel).querySelector(arrows.left1);
		var left2 = document.querySelector(arrows.containerSel).querySelector(arrows.left2);
		var left3 = document.querySelector(arrows.containerSel).querySelector(arrows.left3);
		var right1 = document.querySelector(arrows.containerSel).querySelector(arrows.right1);
		var right2 = document.querySelector(arrows.containerSel).querySelector(arrows.right2);
		var right3 = document.querySelector(arrows.containerSel).querySelector(arrows.right3);
		if (window.pageYOffset > obj.pageYOffsetMin && window.pageYOffset < obj.pageYOffsetMax) {
			left1.style.left = window.pageYOffset*obj.floatIngMultipler + offsets.left +'px';
			right1.style.left = width - (window.pageYOffset*obj.floatIngMultipler + offsets.right) + 'px';
			left2.style.left = window.pageYOffset*obj.floatIngMultipler + offsets.left +'px';
			right2.style.left = width - (window.pageYOffset*obj.floatIngMultipler + offsets.right) + 'px';
			left3.style.left = window.pageYOffset*obj.floatIngMultipler + offsets.left+30 +'px';
			right3.style.left = width - (window.pageYOffset*obj.floatIngMultipler + offsets.right) + 'px';
		}
	}

	function arrowsInit() {
		document.addEventListener('scroll',function () {
			if (window.innerWidth > 1280) {
				floating(configFloating1280,{
					left: -320,
					right: -180
				},1280,arrows);
			}

			if (window.innerWidth >= 1024 && window.innerWidth < 1280) {
				floating(configFloating1024,{
					left: -280,
					right: -170
				},1024,arrows);
			}

			if (window.innerWidth >= 768 && window.innerWidth < 1024 ) {
				floating(configFloating768,{
					left: -300,
					right: -180
				},768,arrows);
			}

	    }, false);
	}

	return {
		restart: restart,
		arrows: arrowsInit
	}
})