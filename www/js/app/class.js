define(function(){

	function addClass (el,classNm) {
		if (el.getAttribute('class').indexOf(classNm) == -1) {
			el.setAttribute('class',el.className+' '+classNm);
		} else return false;
	}

	//for refreshing da animations
	function refreshClass (el,classNm) {
		var currClass = el.getAttribute('class');
		if (currClass.indexOf(classNm) != -1) {
			removeClass(el,classNm);
			setTimeout(function(){addClass(el,classNm)},10);
		} else {
			toggleClass(el,classNm);
		}
	}

	function removeClass (el,classNm) {
		el.className = el.className.replace(' '+classNm,'');
		//keep deleting if more than 1 instance of this class
		(el.className.indexOf(classNm) != -1) ? removeClass(el,classNm) : el;
	}

	function toggleClass (el,classNm) {
		var currClass = el.getAttribute('class');
		if (currClass.indexOf(classNm) != -1) {
			(currClass != '') ? el.setAttribute('class',currClass.replace(' '+classNm,'')) : 
								el.setAttribute('class',currClass.replace(classNm,''));
		} else {
			el.setAttribute('class',el.className+' '+classNm);
		}
	}
	
	return {
		add: addClass,
		refresh: refreshClass,
		remove: removeClass,
		toggle: toggleClass
	}
})