OrderForm.languageStyle.setLabel('');
OrderForm.sp_validation = 1; 
OrderForm.SP_NewDesign = {
	init : function()
	{
		me = OrderForm.SP_NewDesign;
        $('#row_additional_142').show();
        if (!OrderForm.isPreview)
        {
            me.pickOutSteps();            
            $('select#doctype, select#urgency, select#numberOfSources, select#style, select#country')
                .selectmenu({maxHeight: 200, style:'dropdown'});
        }
        $('#numpages')
            .selectmenu({maxHeight: 200, style:'dropdown', blur:function(){OrderForm.SP_NewDesign.blurSelectMenuEl()}});
        $('#order_category')
            .selectmenu({maxHeight: 200, style:'dropdown', blur:function(){OrderForm.SP_NewDesign.blurSelectMenuEl2()}});
	},
    blurSelectMenuEl : function()
    {
        checkSomeRequiredFields(8);
    },
    blurSelectMenuEl2 : function()
    {
        checkSomeRequiredFields(9);
    },
    changePages : function(elObj)
    {        
        if (elObj.id == 'o_interval' || elObj.id == 'urgency'  || elObj.id == 'wrlevel')
        {
            if ($('#numpages-menu').length > 0)
            {
                $('#numpages').selectmenu('destroy');
            }
            $('#numpages').selectmenu({maxHeight: 200, style:'dropdown'});
        }
    },
    pickOutSteps : function()
    {
        $('#personal_info input, #personal_info select, #personal_info a, #personal_info span').focus(function() {
            $('.order_title')
                .removeClass('order_title_1_active')
                .removeClass('order_title_2_active')
                .removeClass('order_title_3_active');
            $('.order_title_1').addClass('order_title_1_active');
        });
        $('#order_details input, #order_details select, #order_details textarea, #order_details a, #order_details span').focus(function() {
            $('.order_title')
                .removeClass('order_title_1_active')
                .removeClass('order_title_2_active')
                .removeClass('order_title_3_active');
            if ($('.order_title_2').hasClass('order_title_2_customer'))
            {
                $('.order_title_2').addClass('order_title_1_active');
            }else
            {
                $('.order_title_2').addClass('order_title_2_active');
            }
        });
        $('.feature_tr input, .feature_tr select, .feature_tr textarea, .feature_tr a, .feature_tr span').focus(function() {
            $('.order_title')
                .removeClass('order_title_1_active')
                .removeClass('order_title_2_active')
                .removeClass('order_title_3_active');
            if ($('.order_title_3').hasClass('order_title_3_customer'))
            {
                $('.order_title_3').addClass('order_title_2_active');
            }else
            {
                $('.order_title_3').addClass('order_title_3_active');
            }
            
        });
    }
};

function AjaxCheckEl(el, display_error)
{
    if ($("#email").length > 0)
    {
        if ($('#email').val() == '')
        {
            display_error && $('#email').addClass('error_field');
            $('#email_ok').remove();
            return false;
        }
        
        var data_fields = new Object();

        if($('#'+el))
        {
            eval("data_fields."+el+"='"+$('#'+el).val()+"'");
        }

        $.ajax({
                    type: 'POST',
                    url: '/order.validate/',
                    data: data_fields,
                    success: function(data)
                    {
                        var answer = eval('(' + data + ')');
                        if(answer[el])
                        {
                            switch(el)
                            {
                                case 'email':
                                    display_error && $('#email').addClass('error_field');
                                    $('#email'+'_ok').remove();
                                    break;
                            }
                        }
                        else
                        {
                            switch(el)
                            {
                                case 'email':
                                    $('#email').removeClass('error_field');
                                    if($('#email'+'_ok').attr('class')==undefined)
                                    {
                                        //$('#row_email td:last-child').append('<div id="email_ok" class="ok_field"></div>');
                                    }
                                    checkRetypeEmail(display_error);
                                    break;
                            }
                        }
                    }
        });
    }
}

function checkFirstName(display_error)
{
    if ($('#firstname').length > 0)
    {
        if ($('#firstname').val() == ''||$('#firstname').val() == 'First name')
        {
            display_error && $('#firstname').addClass('error_field');
            $('#firstname_ok').remove();
        }
        else if($('#firstname').val().length > 0)
        {
            $('#firstname').removeClass('error_field');
        }
        if ($('#lastname').val() != ''&&$('#lastname').val() != 'Last name'&&$('#firstname').val() != ''&&$('#firstname').val() != 'First name')
        {
            if($('#firstname_ok').attr('class')==undefined)
            {
                //$('#row_firstname td:last-child').append('<div id="firstname_ok" class="ok_field"></div>');
            }
        }
    }
}

function checkLastName(display_error)
{
    if ($('#lastname').length > 0)
    {
        if ($('#lastname').val() == ''||$('#lastname').val() == 'Last name')
        {
            display_error && $('#lastname').addClass('error_field');
            $('#firstname_ok').remove();
        }
        else if ($('#firstname').val() != '' && $('#firstname').val() != 'First name')
        {
            $('#lastname').removeClass('error_field');
            if($('#firstname_ok').attr('class')==undefined)
            {
                //$('#row_firstname td:last-child').append('<div id="firstname_ok" class="ok_field"></div>');
            }
        }
    }
}

function checkRetypeEmail(display_error)
{
    if ($('#retype_email').length > 0)
    {
        if ($('#retype_email').val() == '' || $('#retype_email').val() != $('#email').val())
        {
            display_error && $('#retype_email').addClass('error_field');
            $('#retype_email'+'_ok').remove();
        }
        else if ($('#retype_email').val() != '')
        {
            $('#retype_email').removeClass('error_field');
            if($('#retype_email'+'_ok').length == 0 && $('#email_ok').length)
            {
                //$('#row_retype_email td:last-child').append('<div id="retype_email_ok" class="ok_field"></div>');
            }
        }
    }
}

function checkCountry(display_error)
{
    if ($('#country').val() == '')
    {
        display_error && $('#country-button').addClass('error_field');
        $('#phone1_number'+'_ok').remove();
    }
    else
    {
        $('#country-button').removeClass('error_field');
    }
}

function checkPhone1(display_error)
    {
        if ($('#phone1_number').length > 0)
        {    
            var intRegex = /^\d+$/;
            var y =$('#phone1_number').val();
            y =y.replace(/\s/g,'');
            var x =$('#phone1_area').val();
            x =x.replace(/\s/g,'');
            if ($('#phone1_number').val()=='' || $('#phone1_number').val()=='number' || !intRegex.test(y))
            {
                display_error && $('#phone1_number').addClass('error_field');
                $('#phone1_number_ok').remove();
            }
            else
            {
                $('#phone1_number').removeClass('error_field');
            }

            if ($('#phone1_area').val()==''||$('#phone1_area').val()=='area'||!intRegex.test(x))
            {
                display_error && $('#phone1_area').addClass('error_field');
                $('#phone1_number'+'_ok').remove();
            }
            else
            {
                $('#phone1_area').removeClass('error_field');
            }
            if ($('#country').val()=='')
            {
                display_error && $('#country-button').addClass('error_field');
                $('#phone1_number'+'_ok').remove();
            }
            else
            {
                $('#country-button').removeClass('error_field');
            }
            if($('#phone1_number').val()!=''&&$('#phone1_number').val()!='number'&&$('#country').val()!=''&&$('#phone1_area').val()!=''&&$('#phone1_area').val()!='area'&&$('#phone1_number'+'_ok').attr('class')==undefined&&intRegex.test(y)&&intRegex.test(x))
            {
                //$('#row_phone1 td:nth-child(4)').append('<div id="phone1_number_ok" class="ok_field"></div>');
            }
        }
    }

function checkPhone1Area(display_error)
    {
        var intRegex = /^\d+$/;
        var y = $('#phone1_area').val();
        y =y.replace(/\s/g,'');
        if ($('#phone1_area').val()==''||$('#phone1_area').val()=='area'||!intRegex.test(y))
        {
            display_error && ('#phone1_area').addClass('error_field');
            $('#phone1_number'+'_ok').remove();
        }
        else
        {
            $('#phone1_area').removeClass('error_field');
        }
    }

function checkPhone2Area()
{
    if ($('#phone2').length > 0)
    {
        if ($('#phone2_number_ok').length != 0)
        {
            return false;
        }
        var stripped = $('#phone2').val().replace(/[\(\)\.\-\ ]/g, '');
        if (stripped.value != "" && parseInt(stripped) && stripped.length > 6) 
        {
            //$('#row_phone2 td:nth-child(3)').append('<div id="phone2_number_ok" class="ok_field"></div>');
        }
        else 
        {
            $('#phone2_number_ok').remove();
        }
    }
}

function checkTopic(display_error)
{
    if ($('#topic').val() == '')
    {
        display_error && $('#topic').addClass('error_field');
        $('#topic'+'_ok').remove();
    }
    else
    {
        $('#topic').removeClass('error_field');
        if($('#topic'+'_ok').attr('class')==undefined)
        {
            //$('#row_topic td:last-child').append('<div id="topic_ok" class="ok_field"></div>');
        }
    }
}

function checkDetails(display_error)
{
    if ($('#details').val() == '')
    {
        display_error && $('#details').addClass('error_field');
        $('#details'+'_ok').remove();
    }
    else
    {
        $('#details').removeClass('error_field');
        if($('#details'+'_ok').attr('class')==undefined)
        {
            num = ($('#row_details td:nth-child(2) > div').length == 1) ? 1 : 2;
            //$('#row_details td:nth-child(2) > div:nth-child(' + num + ')').after('<div id="details_ok" class="ok_field"></div>');
        }
    }
}

function checkNumpages(display_error)
{
            if ($('#numpages').val()==''||$('#numpages').val()=='select'||$('#numpages').val()=='0')
            {
                display_error && $('#numpages'+' ~a').addClass('error_field');
                $('#numpages'+'_ok').remove();
            }
            else if ($('#numpages').val() != '')
            {
                $('#numpages'+' ~a').removeClass('error_field');
                if($('#numpages'+'_ok').attr('class')==undefined)
                {
                    num = ($('#row_numpages td:nth-child(2) > div').length == 1) ? 1 : 2;
                    //$('#row_numpages td:nth-child(2) > div:nth-child(' + num + ')').after('<div id="numpages_ok" class="ok_field"></div>');
                }
            }
}



function checkOrderCategory(n, display_error)
{
        if ($('#order_category').val()==''||$('#order_category').val()=='select'||$('#order_category').val()=='0')
        {
            display_error && $('#order_category'+' ~a').addClass('error_field');
            $('#order_category'+'_ok').remove();
        }
        else if ($('#order_category').val() != '')
        {
            $('#order_category'+' ~a').removeClass('error_field');
            if($('#order_category'+'_ok').attr('class')==undefined)
            {
                //$('#row_order_category td:nth-child(2) div:nth-child('+n+')').after('<div id="order_category_ok" class="ok_field"></div>');
            }
        }
    
}
function checkSelects(id, display_error)
{
    if ($('#'+id).length > 0 && id != 'numpages')
    {
        if (id != 'doctype' && ($('#'+id).val() == '' || $('#'+id).val() == 'select' || $('#'+id).val() == '0'))
        {
            display_error && $('#'+id+' ~a').addClass('error_field');
            $('#'+id+'_ok').remove();
        }
        else
        {
            $('#'+id+' ~a').removeClass('error_field');
            if($('#'+id+'_ok').attr('class')==undefined)
            {
                //$('#row_'+id+' td:nth-child(2)').append('<div id="'+id+'_ok" class="ok_field"></div>');
            }
        }
    }
}

function checkSomeSelects(display_error)
{
    checkSelects('numberOfSources', display_error);
    checkSelects('urgency', display_error);
    checkSelects('academic_level', display_error);
    checkSelects('doctype', display_error);
    checkNumpages(false);
    checkPhone2Area();
}

function checkAllRequiredFields()
{
    checkFirstName();checkLastName();checkPhone1();AjaxCheckEl('email');checkRetypeEmail();checkNumpages();checkOrderCategory(2);
    checkSelects();checkTopic();checkDetails();
    return false;
}

function ckeckOnSwitchDoctype(check_on_load)
{
    checkTopic(check_on_load);
    checkDetails(check_on_load);
    checkOrderCategory(1, check_on_load); checkOrderCategory(2, check_on_load);
    checkNumpages(check_on_load);

    $('#topic').keyup(function(){ checkTopic(false) } );     
    $('#details').keyup(function(){ checkDetails(false) });
    $('#order_category').change(function(){ checkOrderCategory(1, false); checkOrderCategory(2, false) });
    $('#numpages').change(function(){ checkNumpages(false) });
    $('#country').change(function() { checkCountry(false)});
}

function checkSomeRequiredFields(n, display_error)
{
    switch(n)
    {
        case 2:
            checkFirstName(display_error);checkLastName(display_error);
            break;
        case 3:
            checkFirstName(display_error);checkLastName(display_error);
            break;
        case 4:
            checkFirstName(display_error);checkLastName(display_error);checkRetypeEmail(display_error)
            break;
        case 5:
            checkFirstName(display_error);checkLastName(display_error);checkPhone1(display_error);checkRetypeEmail(display_error);
            break;
        case 6:
            checkFirstName(display_error);checkLastName(display_error);checkPhone1(display_error);checkRetypeEmail(display_error);checkTopic(display_error);
            break;
        case 7:
            checkFirstName(display_error);checkLastName(display_error);checkPhone1(display_error);checkRetypeEmail(display_error);checkTopic(display_error);checkDetails(display_error);
            break;
        case 8:
            checkFirstName(display_error);checkLastName(display_error);checkPhone1(display_error);checkRetypeEmail(display_error);checkNumpages(display_error);checkTopic(display_error);checkDetails(display_error);
            break;
        case 9:
            checkFirstName(display_error);checkLastName(display_error);checkPhone1(display_error);checkRetypeEmail(display_error);checkNumpages(display_error);checkTopic(display_error);checkDetails(display_error);checkOrderCategory(2, display_error);
            break;
        case 10:
            checkFirstName(display_error);checkLastName(display_error);checkPhone1(display_error);checkPhone2Area();AjaxCheckEl('email', display_error);checkRetypeEmail(display_error);checkNumpages(display_error);checkTopic(display_error);checkDetails(display_error);checkOrderCategory(1, display_error);
            break;
    }

    return false;
}

$(document).ready(function(){
   
    if ($('.btn_edit_order_prev').length == 0)
    {
        checkSomeRequiredFields(10, false);
    }
    $('#personal_info input').focus(function(){
       // $(this).css('color','#000');
    });
    $('#personal_info input').blur(function(){
        //$(this).css('color','#888');
    });
    
    
    $('#country').change(function() { checkCountry(false)});
    $('#firstname').keyup(function(){ checkFirstName(false) });
    $('#lastname').keyup(function(){ checkLastName(false) });
    $('#phone1_area').keyup(function(){ checkPhone1Area(false) });
    $('#phone1_number').keyup(function(){ checkPhone1(false) } );
    $('#firstname').click(function(){if(this.value=='First name'){this.value='';}});
    $('#lastname').click(function(){if(this.value=='Last name'){this.value='';}});
    $('#phone1_area').click(function(){if(this.value=='area'){this.value='';}});
    $('#phone1_number').click(function(){if(this.value=='number'){this.value='';}});
    $('#email').blur(function(){AjaxCheckEl('email', false)});
    $('#retype_email').blur(function(){ checkRetypeEmail(false) });
    $('#phone2').keyup(function(){ checkPhone2Area() });
    
    
    $('#topic').keyup(function(){ checkTopic(false) } );     
    $('#details').keyup(function(){ checkDetails(false) });
    $('#order_category').change(function(){ checkOrderCategory(1, false); checkOrderCategory(2, false) });
    $('#numpages').change(function(){ checkNumpages(false) });


	OrderForm.afterSwitchForms.push(OrderForm.SP_NewDesign.init);
    OrderForm.afterOnInputChange.push(OrderForm.SP_NewDesign.changePages);
	OrderForm.SP_NewDesign.init();

});
